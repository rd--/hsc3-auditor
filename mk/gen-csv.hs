import qualified Music.Theory.Array.CSV.Midi.MND as T {- hmt -}
import qualified Music.Theory.Pitch as T {- hmt -}
import qualified Music.Theory.Set.List as T {- hmt -}
import qualified Music.Theory.Time.Seq as T {- hmt -}

import qualified Sound.SC3.Auditor.FCD as FCD

prj_dir :: String
prj_dir = "/home/rohan/sw/hsc3-auditor/"

prj_file :: String -> String
prj_file = (++) prj_dir

mnn_from :: Int -> T.Octave_PitchClass Int -> [Int]
mnn_from n p0 = take n [T.octpc_to_midi p0 .. ]

wr_csv :: (Real t, Real n) => String -> T.Tseq t (T.Begin_End (T.Event n)) -> IO ()
wr_csv fn = T.csv_mnd_write_tseq 2 (prj_file fn)

type R = Double

-- | The PF gamut.
--
-- > pf_88 (0.1,2)
pf_88 :: (R,R) -> IO ()
pf_88 (iot,sus) = do
  let sq = zipWith (\t n -> ((t,iot * sus),(n,127,0,[]))) [0, iot ..] (mnn_from 88 (0,9))
  wr_csv "data/csv/pf-88.csv" (T.midi_wseq_to_midi_tseq sq)

-- | The FCD gamut, each registration in sequence.
--
-- > fcd_29 (0.2,5)
fcd_29 :: (R,R) -> IO ()
fcd_29 (iot,sus) =  do
  let mnn_ch = concatMap
               (\stp -> let ch = fromIntegral stp
                        in zip (FCD.fcd_gamut_midi stp) (repeat ch))
               FCD.fcd_stops_ix
      sq = zipWith (\t (n,c) -> ((t,iot * sus),(n,127,c,[]))) [0, iot ..] mnn_ch
  wr_csv "data/csv/fcd-29.csv" (T.midi_wseq_to_midi_tseq sq)

-- | FCD: the six non-vibrato registration combinations.
--
-- > fcd_rg (3,0.75)
fcd_rg :: (R,R) -> IO ()
fcd_rg (iot,sus) = do
  let ch = [60,63,65,67,69,72]
      rg_seq = T.powerset' [0,2,4,6,8,9]
      sq = map (\rg_set -> (concatMap (\n -> (map (\rg -> ((n,127,rg,[]))) rg_set)) ch)) rg_seq
      w_sq = zipWith (\t e -> ((t,iot * sus),e)) [0, iot ..] sq
      t_sq = T.midi_wseq_to_midi_tseq (T.wseq_unjoin w_sq)
  wr_csv "data/csv/fcd-rg.csv" t_sq
