import System.FilePath {- filepath -}

import qualified Music.Theory.List as T {- hmt -}

import           Sound.SC3.Auditor.A37Z
import qualified Sound.SC3.Auditor.Common as Common

-- | The file-name of the voice recording.
a37z_vc_fname :: Common.Voice_Name -> FilePath
a37z_vc_fname vc = a37z_file (vc <.> a37z_format)

-- | The recorded duration for each tone (in seconds), ie. ts=6/4 at q=54.
a37z_sample_dur :: Fractional n => n
a37z_sample_dur = 8

a37z_smp_bnd :: Integral n => [(n,n)]
a37z_smp_bnd = Common.sf_sample_bounds a37z_sample_rate a37z_sample_dur a37z_degree (0,0)

-- | Extract tones from voice recordings
--
-- > mapM_ a37z_sf_extract a37z_vc
a37z_sf_extract :: Common.Voice_Name -> IO ()
a37z_sf_extract vc = do
  let c = Common.sf_extract_tone_set_cmd 1 Nothing (a37z_vc_fname vc) (zip a37z_gamut a37z_smp_bnd)
  mapM_ Common.run_cmd c

-- | Extract ZC tones from voice recordings.
--
-- > mapM_ a37z_zc_sf_extract a37z_vc
a37z_zc_sf_extract :: Common.Voice_Name -> IO ()
a37z_zc_sf_extract vc = do
  let b = T.lookup_err vc a37z_zc_ix
      c = Common.sf_extract_tone_set_cmd 1 (Just "zc") (a37z_vc_fname vc) (zip a37z_gamut b)
  mapM_ Common.run_cmd c

-- * GEN

{-
r <- a37z_gen_zc_data
length r == 6
map length r == [74,74,74,74,74,74]

import Sound.SC3.Auditor.A37Z.Data
zipWith (==) r a37z_zc_ix_raw
-}
a37z_gen_zc_data :: IO [[Int]]
a37z_gen_zc_data = do
  let b = Common.sf_sample_bounds a37z_sample_rate a37z_sample_dur a37z_degree (0.25,1.25)
      ix = concatMap (\(st,du) -> [st,st + du]) b
  mapM (\fn -> Common.sf_zero_crossings fn ix) (map a37z_vc_fname a37z_vc)
