import System.FilePath {- filepath -}
import Text.Printf {- base -}

import qualified Music.Theory.List as T {- hmt -}

import qualified Sound.SC3.Auditor.Common as Common
import qualified Sound.SC3.Auditor.FCD as FCD
import qualified Sound.SC3.Auditor.FCD.Data as FCD.Data
import qualified Sound.SC3.Auditor.FCD.WT as FCD.WT

-- | The file-name of the voice recording
fcd_vc_fname :: Common.Voice_Name -> FilePath
fcd_vc_fname vc = FCD.fcd_dir </> vc <.> FCD.fcd_format

-- | The recorded duration for each tone (in seconds), ie. ts=6/4 at q=54.
--
-- > fcd_sample_dur == 6 + 2/3
fcd_sample_dur :: Fractional n => n
fcd_sample_dur = 20/3

-- | Extract tones from voice recordings
--
-- > mapM_ fcd_sf_extract fcd_vc
fcd_sf_extract :: Common.Voice_Name -> IO ()
fcd_sf_extract vc = do
  let b = Common.sf_sample_bounds FCD.fcd_sample_rate fcd_sample_dur (FCD.fcd_degree vc) (0,0)
      c = Common.sf_extract_tone_set_cmd 1 Nothing (fcd_vc_fname vc) (zip (FCD.fcd_gamut vc) b)
  mapM_ Common.run_cmd c

-- | Analyse voice files for ZC.
--   Cached at 'FCD.Data.fcd_zc_ix_raw'.  See 'fcd_zc_ix' for structured variant.
fcd_gen_zc_data :: IO [[Int]]
fcd_gen_zc_data = do
  let b vc = Common.sf_sample_bounds FCD.fcd_sample_rate fcd_sample_dur (FCD.fcd_degree vc) (0.50,1.25)
      ix vc = concatMap (\(st,du) -> [st,st + du]) (b vc)
  mapM (\vc -> Common.sf_zero_crossings (fcd_vc_fname vc) (ix vc)) FCD.fcd_vc

-- | Extract ZC tones from voice recordings.
--
-- > mapM_ fcd_zc_sf_extract fcd_vc
fcd_zc_sf_extract :: Common.Voice_Name -> IO ()
fcd_zc_sf_extract vc = do
  let b = T.lookup_err vc FCD.fcd_zc_ix
      c = Common.sf_extract_tone_set_cmd 1 (Just "zc") (fcd_vc_fname vc) (zip (FCD.fcd_gamut vc) b)
  mapM_ Common.run_cmd c

-- * WT/RAW DATA

fcd_gen_wt_extract_cmd :: Int -> String -> Int -> Int -> (Int,Int) -> (String,[String])
fcd_gen_wt_extract_cmd sz nm nt wt (i,j) =
  let in_fn = printf "%s/%s.%s" FCD.fcd_dir nm FCD.fcd_format
      out_fn = FCD.WT.fcd_wt_gen_fn nm nt wt
  in ("hsc3-sf-extract",[in_fn,"1",show i,show j,show sz,out_fn])

fcd_gen_wt_extract_cmd_set :: Int -> (String, [[Int]]) -> [[Common.Cmd]]
fcd_gen_wt_extract_cmd_set sz (nm,ix_set) =
  let f nt (wt,(i,j)) = fcd_gen_wt_extract_cmd sz nm nt wt (i,j)
      g (nt,ix) = map (f nt) (zip [0..] (T.adj2 1 ix))
  in map g (zip [0..] ix_set)

-- > run_wt_extract_cmd_set 512
fcd_run_wt_extract_cmd_set :: Int -> IO ()
fcd_run_wt_extract_cmd_set sz = do
  let wt = zip FCD.fcd_vc FCD.Data.fcd_wt_ix_raw
      cmd_seq = concatMap (concat . fcd_gen_wt_extract_cmd_set sz) wt
  mapM_ Common.run_cmd cmd_seq

-- | Cached at 'FCD.Data.fcd_wt_ix_raw'
fcd_gen_wt_data :: IO [[[Int]]]
fcd_gen_wt_data = do
  let f (fn,ix) = Common.sf_zero_crossings_n 10 fn (map fst ix)
  mapM f (zip (map fcd_vc_fname FCD.fcd_vc) (map snd FCD.fcd_zc_ix))
