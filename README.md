hsc3-auditor
------------

A simple
[haskell](http://haskell.org/)
[supercollider](http://audiosynth.com/)
auditioner for music structures.

## cli

[auditor](?t=hsc3-auditor&e=md/auditor.md),
[csv-mnd-au](?t=hsc3-auditor&e=md/csv-mnd-au.md),
[smplr](?t=hsc3-auditor&e=md/smplr.md)
[smplr-sfz](?t=hsc3-auditor&e=md/smplr-sfz.md)

© [rohan drape](http://rohandrape.net/),
  2010-2025,
  [gpl](http://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 31  Tried: 31  Errors: 0  Failures: 0
$
```

