all:
	echo "hsc3-auditor"

mk-cmd:
	(cd cmd ; make all install)

clean:
	(cd cmd ; make clean)
	rm -Rf dist dist-newstyle *~

push-all:
	r.gitlab-push.sh hsc3-auditor

indent:
	fourmolu -i Sound cmd

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
