import Control.Concurrent {- base -}
import Control.Monad {- base -}

import qualified Data.Map as Map {- containers -}
import qualified System.IO.Strict as Strict {- strict -}

import qualified Music.Theory.Opt as Opt {- hmt-base -}

import qualified Music.Theory.Tuning.Midi as Midi {- hmt -}

import qualified Sound.Sc3 as Sc3 {- hsc3 -}

import qualified Sound.Sc3.Ui.Enum.Html as Ui {- hsc3-data -}

import qualified Sound.Sc3.Auditor.Server.Plain as Plain
import qualified Sound.Sc3.Auditor.Sfz as Sfz
import qualified Sound.Sc3.Auditor.Smplr as Smplr

opt_def :: [Opt.OptUsr]
opt_def =
  [ ("b0", "0", "int", "sound file buffer zero index")
  , ("grp", "1", "int", "group-id")
  , ("nid", "1000", "int", "node-id")
  , ("tbl", "", "filename", "tuning table (midi-note-number,fractional-midi-note-number)")
  , ("v0", "0", "int", "velocity-minima")
  , ("v1", "1", "int", "velocity-maxima")
  ]

help :: [String]
help =
  [ "hsc3-smplr-sfz cmd arg..."
  , ""
  , "  load sfz-file [opt]"
  , "  run sfz-file [opt]"
  , "  send synthdef"
  , "  ui [opt] sfz-file-seq"
  ]

load :: FilePath -> [Opt.Opt] -> IO ()
load fn o = Sfz.sfz_load 0 =<< Sfz.sfz_load_instr (Opt.opt_read o "b0") fn

run :: FilePath -> [Opt.Opt] -> IO ()
run fn o = do
  let prm_opt = Smplr.smplr_param_all_opt o
  print ("prm_opt", prm_opt)
  (_, _, nd) <- Sfz.sfz_load_instr (Opt.opt_read o "b0") fn
  tbl_dat <- maybe (return Nothing) (fmap Just . Midi.mnn_fmnn_table_load_csv) (Opt.opt_get_nil o "tbl")
  let st = Plain.st_init_fp ("?", Opt.opt_read o "grp", Opt.opt_read o "nid", ())
      tun_f = maybe Plain.def_tun_f Plain.tun_f_from_tbl tbl_dat
  (act, _) <-
    Plain.rt_midi_osc
      ( st
          { Plain.st_msg = Sfz.sfz_msg_f (const (nd, prm_opt))
          , Plain.st_vel_rng = (Opt.opt_read o "v0", Opt.opt_read o "v1")
          , Plain.st_tun = tun_f
          }
      )
  act

main :: IO ()
main = do
  (o, a) <- Opt.opt_get_arg False help opt_def -- allow syn opt
  case a of
    ["load", fn] -> load fn o
    ["run", fn] -> run fn o
    ["send", "synthdef"] -> Smplr.smplr_load_all
    ["ui", fn] -> ui o fn
    _ -> Opt.opt_usage help opt_def

-- * Ui

-- | {ui-ix -> (sfz-filename,is-loaded),thread-id}
type Ui_St = (Map.Map Int (FilePath, Sfz.Sfz_Instr, Bool), Maybe ThreadId)

ui_st_init :: Sc3.Buffer_Id -> [FilePath] -> IO Ui_St
ui_st_init b0 fn_seq = do
  i_seq <- Sfz.sfz_load_instr_seq b0 fn_seq
  let ls = zip [0 ..] (map (\(fn, i) -> (fn, i, False)) (zip fn_seq i_seq))
  return (Map.fromList ls, Nothing)

ui_st_edit :: Sc3.Param -> (Ui_St, (Int, Int)) -> IO Ui_St
ui_st_edit prm_opt ((m, th), (_, k)) = do
  let tun_f = Plain.def_tun_f
      st = Plain.st_init_fp ("?", 1, 1000, ())
  case Map.lookup k m of
    Nothing -> error "ui_st_edit?"
    Just (fn, i, ld) -> do
      when (not ld) (Sfz.sfz_load 0 i)
      maybe (return ()) killThread th
      let (_, _, nd) = i
      (th', _) <- Plain.rt_midi_osc_th (st {Plain.st_msg = Sfz.sfz_msg_f (const (nd, prm_opt)), Plain.st_tun = tun_f})
      return (Map.insert k (fn, i, True) m, Just th')

ui_fn_seq :: [Opt.Opt] -> [FilePath] -> IO ()
ui_fn_seq o fn_seq = do
  print ("ui:opt", o)
  Smplr.smplr_load_all
  let prm_opt = Smplr.smplr_param_all_opt o
  st <- ui_st_init (Opt.opt_read o "b0") fn_seq
  Ui.ui_html_proc 9160 35 "/tmp/smplr-sfz.html" "smplr-sfz" [(55, fn_seq)] st (ui_st_edit prm_opt) (960, 600) False

-- > ui [("b0","0")] "/home/rohan/sw/hsc3-auditor/etc/sfz.text"
ui :: [Opt.Opt] -> FilePath -> IO ()
ui o fn = fmap lines (Strict.readFile fn) >>= ui_fn_seq o
