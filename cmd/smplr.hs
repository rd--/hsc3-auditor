import qualified Music.Theory.Opt as Opt {- hmt-base -}

import qualified Sound.Sc3.Auditor.Common as Common
import qualified Sound.Sc3.Auditor.Meta as Meta
import qualified Sound.Sc3.Auditor.Server.Plain as Plain
import qualified Sound.Sc3.Auditor.Smplr as Smplr

import qualified O

-- * Cmd

help :: [String]
help =
  [ "hsc3-smplr cmd arg..."
  , ""
  , "  load instr:string [opt]"
  , "  run instr:string [opt]"
  , "  send synthdef"
  ]

run :: String -> [Opt.Opt] -> IO ()
run instr o = do
  let usr_opt = O.au_usr_opt_read o
      (_b0, _dyn, _en, grp, _lp, nid, tbl, (v0, v1), _vc, prm_opt) = usr_opt
      (syn, akt_f, _) = O.au_smplr_opt instr usr_opt
  tbl_dat <- Common.tun_tbl_load tbl
  let st = Plain.st_init_fp (syn, grp, nid, ())
      tun_f = maybe Plain.def_tun_f Plain.tun_f_from_tbl tbl_dat
  (act, _) <-
    Plain.rt_midi_osc
      ( st
          { Plain.st_syn = syn
          , Plain.st_msg = Meta.meta_msg_f (1, akt_f, prm_opt)
          , Plain.st_vel_rng = (v0, v1)
          , Plain.st_tun = tun_f
          }
      )
  act

main :: IO ()
main = do
  (o, a) <- Opt.opt_get_arg False help O.au_smplr_opt_def -- allow syn opt
  case a of
    ["load", instr] -> Meta.au_load (instr, Opt.opt_read o "b0", Opt.opt_get o "vc", Opt.opt_get o "dyn")
    ["run", instr] -> run instr o
    ["send", "synthdef"] -> Smplr.smplr_load_all
    _ -> Opt.opt_usage help O.au_smplr_opt_def
