import Data.Char {- base -}
import Data.Maybe {- base -}
import System.FilePath {- filepath -}

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hsc3 -}

import Sound.Midi.Type {- midi-osc -}

import qualified Music.Theory.List as List {- hmt-base -}
import qualified Music.Theory.Math as Math {- hmt-base -}
import qualified Music.Theory.Opt as Opt {- hmt-base -}

import qualified Music.Theory.Array.Csv.Midi.Mnd as Mnd {- hmt -}
import qualified Music.Theory.Time.Seq as Seq {- hmt -}

import qualified Sound.Sc3.Auditor.Common as Common {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Meta as Meta {- hsc3-auditor -}
{- hsc3-auditor -}

import qualified Sound.Sc3.Auditor.Sfz as Sfz {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Smplr as Smplr

import qualified O

-- * Util

-- | 8-bit unsigned integer
type U8 = Int

-- | 64-bit floating point
type F64 = Double

{- | F64 to U8, it is an error if the input is non-integral or out of range.

>>> map f64_to_u8 [-1,0,0.5,255,256]
[Nothing,Just 0,Nothing,Just 255,Nothing]
-}
f64_to_u8 :: F64 -> Maybe U8
f64_to_u8 n =
  case Math.integral_and_fractional_parts n of
    (r, 0) -> if r >= 0 && r <= 255 then Just r else Nothing
    _ -> Nothing

-- | Erroring variant
f64_to_u8_err :: F64 -> U8
f64_to_u8_err = fromMaybe (error "f64_to_u8") . f64_to_u8

-- * Seq

-- | Mnd Sequence
type Seq = Seq.Wseq F64 (Mnd.Event F64)

-- | Transposition
seq_transpose :: F64 -> Seq -> Seq
seq_transpose n = Seq.wseq_map (\(mnn, vel, ch, param) -> (mnn + n, vel, ch, param))

-- | This only makes sense if the mnn at Seq are whole numbers.
seq_retune :: [(U8, F64)] -> Seq -> Seq
seq_retune tbl =
  let f (mnn, vel, ch, param) = (List.lookup_err (f64_to_u8_err mnn) tbl, vel, ch, param)
  in Seq.wseq_map f

-- | Time scaling, /n/ is a multiplier.
seq_tm_scale :: F64 -> Seq -> Seq
seq_tm_scale n = Seq.seq_tmap (\(st, du) -> (st * n, du * n))

seq_gain :: F64 -> Seq -> Seq
seq_gain n =
  let f (mnn, vel, ch, param) = (mnn, vel * n, ch, param)
  in Seq.wseq_map f

-- * Csv

{- | (tbl,gn,scl,trs)
  tbl = tuning table, gn = gain, scl = time-scalar (multiplier), trs = pitch transposition
-}
type Opt = (Maybe [(U8, F64)], F64, F64, F64)

{- | Load Mnd

>>> let fn = "/home/rohan/sw/hsc3-auditor/data/csv/fcd-29.csv"
>>> sq <- load_mnd (Nothing,1,1,0) fn
>>> length sq
452
-}
load_mnd :: Opt -> FilePath -> IO Seq
load_mnd (tbl, gn, scl, trs) =
  fmap (seq_tm_scale scl . seq_transpose trs . seq_gain gn . maybe id seq_retune tbl)
    . Mnd.csv_midi_read_wseq

-- | Load sequence of csv mnnd files.
load_mnd_seq :: Opt -> [FilePath] -> IO Seq
load_mnd_seq opt = fmap Seq.wseq_concat . mapM (load_mnd opt)

-- * Nq

nq_parse :: RealFrac n => String -> Maybe (n -> n)
nq_parse x =
  let rnd = flip sc3_round_to
      tbl =
        [ ("12et", rnd (1 / 1))
        , ("24et", rnd (1 / 2))
        , ("48et", rnd (1 / 4))
        , ("72et", rnd (1 / 6))
        , ("96et", rnd (1 / 8))
        , ("nil", id)
        ]
  in lookup (map toLower x) tbl

nq_parse_err :: String -> F64 -> F64
nq_parse_err = maybe (error "nq_parse") id . nq_parse

-- * Nrt

type Msg_F = (String, Synth_Id, Group_Id, Param) -> (Channel, F64, F64) -> Maybe Message

instr_nrt :: (String, Msg_F, [Message]) -> [Opt.Opt] -> [FilePath] -> IO [Bundle]
instr_nrt (syn, msg_f, ld_msg) o csv_fn = do
  tbl_dat <- Common.tun_tbl_load_null (Opt.opt_get o "tbl")
  sq <- load_mnd_seq (tbl_dat, Opt.opt_read o "gn", Opt.opt_read o "scl", Opt.opt_read o "trs") csv_fn
  let prm_opt = Common.param_opt_scan (map fst Smplr.smplr_param_all) o
      grp = Opt.opt_read o "grp"
      nq_f = nq_parse_err (Opt.opt_get o "nq")
      beg = bundle 0 (g_new [(1, AddToTail, 0)] : Smplr.smplr_recv_all_msg) -- all?
      eof = bundle (Seq.wseq_end sq + Opt.opt_read o "eT") [nrt_end]
      gen ((st, du), (fmnn, fvel, ch, prm_nt)) =
        let prm = param_merge_r prm_opt prm_nt ++ [("amp", fvel / 127), ("sustain", du)]
        in fmap (Bundle st . return) (msg_f (syn, -1, grp, prm) (ch, nq_f fmnn, fvel))
      msg = beg : mapMaybe gen sq ++ [eof]
  return (if Opt.opt_read o "ld" then bundle 0 ld_msg : msg else msg)

meta_instr_nrt :: String -> [Opt.Opt] -> [FilePath] -> IO [Bundle]
meta_instr_nrt instr o csv_fn = do
  let usr_opt = O.au_usr_opt_read o
      (syn, akt_f, ld_msg) = O.au_smplr_opt instr usr_opt
      msg_f = Meta.meta_sc3_msg_f (Opt.opt_read o "rt_mul", akt_f)
  instr_nrt (syn, msg_f, ld_msg) o csv_fn

sfz_instr_nrt :: FilePath -> [Opt.Opt] -> [FilePath] -> IO [Bundle]
sfz_instr_nrt sfz_fn o csv_fn = do
  let b0 = Opt.opt_read o "b0"
  instr <- Sfz.sfz_load_instr b0 sfz_fn
  let (_, _, nd) = instr
      msg_f = Sfz.sfz_sc3_msg_f (Opt.opt_read o "rt_mul") Common.En_Dur nd
  ld_msg <- if Opt.opt_read o "ld" then Sfz.sfz_load_msg b0 instr else return []
  instr_nrt ("?", msg_f, ld_msg) o csv_fn

typ_instr_nrt :: String -> String -> [Opt.Opt] -> [FilePath] -> IO [Bundle]
typ_instr_nrt typ =
  case typ of
    "meta" -> meta_instr_nrt
    "sfz" -> sfz_instr_nrt
    _ -> error "typ_instr_nrt?"

-- | sr = sample-rate, nc = number-of-channels, sc = nrt-score, sf_fn = sound-file-name
render_nrt :: Int -> Int -> Nrt -> FilePath -> IO ()
render_nrt sr nc sc sf_fn = do
  let osc_fn = replaceExtension sf_fn ".osc"
      opt = (osc_fn, ("_", 0), (sf_fn, nc), sr, PcmFloat, ["-m", "32768"])
  writeNrt osc_fn sc
  nrt_exec_plain opt

-- * Main

{- | Param will be sent with defaults
The loop default is ambiguous, so is removed.
Environment lookup of options only considers keys in the default table.
-}
param_def :: [Opt.OptUsr]
param_def =
  let param = Smplr.smplr_param_all
      ignore = ["bufnum", "loop", "rate"]
      param' = filter (\(k, _) -> k `notElem` ignore) param
  in map (\(k, v) -> (k, show v, "float", "smplr ctl")) param'

opt_def :: [Opt.OptUsr]
opt_def =
  concat
    [ Opt.opt_usr_rw_def [("en", "En_Dur")] O.au_smplr_opt_def
    , param_def
    ,
      [ ("eT", "2", "float", "end time delay (time added to end of sequence)")
      , ("gn", "1", "float", "linear amplitude multiplier")
      , ("ld", "True", "bool", "load/reload samples")
      , ("nc", "1", "int", "Nrt number of channels (output sound file)")
      , ("nq", "nil", "string", "note quantisation (12et|24et|48et|72et|96et|nil)")
      , ("rt_mul", "1", "float", "rate multiplier (sample-playback)")
      , ("scl", "1", "float", "time scalar (start time and duration multiplier)")
      , ("sr", "48000", "int", "Nrt sample-rate")
      , ("trs", "0", "float", "transposition (offset for midi-note-number)")
      ]
    ]

help :: [String]
help =
  [ "csv-mnd-au typ nrt instr csv-fn sf-fn [opt]"
  , "csv-mnd-au typ rt instr csv-fn... [opt]"
  , ""
  , "  typ = meta | sfz"
  ]

main :: IO ()
main = do
  (o, a) <- Opt.opt_get_arg False help opt_def
  let nc = Opt.opt_read o "nc"
      sr = Opt.opt_read o "sr"
  case a of
    [typ, "nrt", ins, csv_fn, sf_fn] -> typ_instr_nrt typ ins o [csv_fn] >>= \sc -> render_nrt sr nc (Nrt sc) sf_fn
    typ : "rt" : ins : csv_fn -> typ_instr_nrt typ ins o csv_fn >>= nrt_audition . Nrt
    _ -> Opt.opt_usage help opt_def

{-

let csv_fn = "/home/rohan/uc/the-center-is-between-us/visitants/csv/midi/air.B.1.csv"
let csv_fn = "/home/rohan/uc/the-center-is-between-us/trees/csv/midi/air.D.1.csv"
let opt = map Opt.opt_plain (Opt.opt_usr_rw_def [("vc","8_llaut"),("trs","-12")] opt_def)
sc <- typ_instr_nrt "meta" "ca-zell" opt [csv_fn]
nrt_audition (Nrt sc)
let sf_fn = "/tmp/t.wav"
render_nrt 48000 2 (Nrt sc) sf_fn

-}
