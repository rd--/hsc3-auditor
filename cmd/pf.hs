{-

see also hsc3-auditor/cmd/smplr.hs

this file is simpler, and doesn't imply the scala database etc.

-}

import Data.Maybe {- base -}
import System.Environment {- base -}

import qualified Music.Theory.Math.Convert as Convert {- hmt-base -}

import Sound.Osc.Fd {- hosc -}
import Sound.Sc3.Fd {- hsc3 -}

import qualified Sound.Sc3.Auditor.Pf as Pf {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Smplr as Smplr {- hsc3-auditor -}

import qualified Sound.Midi.Csv as Csv {- midi-osc -}
import qualified Sound.Midi.Osc as Osc {- midi-osc -}
import qualified Sound.Midi.Osc.Server as Osc.Server {- midi-osc -}
import qualified Sound.Midi.Type as Midi {- midi-osc -}

-- | Environment variables.
type Env = [(String, String)]

env_to_param :: Env -> Param
env_to_param =
  let k_set = ["attack", "bus", "decay", "ramplitude", "rdelay", "rpan"]
      f (k, v) = if k `elem` k_set then Just (k, read v) else Nothing
  in mapMaybe f

to_r :: Int -> Double
to_r = (/ 127.0) . Convert.int_to_double

-- | (t0,volume,modulation,param)
type ST = (Double, Double, Double, Param)

print_csv_mnd :: Time -> Midi.Channel_Voice_Message Int -> IO ()
print_csv_mnd t0 m = do
  t <- time
  maybe (return ()) putStrLn (Csv.cvm_to_csv_mnd (t - t0) m)

note_on_off :: Transport t => t -> ST -> Message -> IO ST
note_on_off s_fd st@(t0, cc_vol, cc_mod, p) d =
  let msg = Osc.osc_to_cvm d
  in print_csv_mnd t0 msg
      >> case msg of
        Midi.Note_On _ n v ->
          let n_ = Convert.int_to_double n
              v_ = to_r v * cc_vol
              p_ = p ++ [("bufnum", n_ - 24), ("amp", v_), ("loop", 0)]
          in sendMessage s_fd (s_new "smplr-gt-ch-2" n AddToTail 1 p_)
              >> return st
        Midi.Note_Off _ n _ ->
          sendBundle s_fd (bundle immediately [n_set1 n "gate" 0, s_noid [n]])
            >> return st
        Midi.Control_Change _ 0x01 m -> print ("cc-mod", m) >> return (t0, cc_vol, to_r m, p)
        Midi.Control_Change _ 0x07 v -> print ("cc-vol", v) >> return (t0, to_r v, cc_mod, p)
        _ -> print ("non-note message", d) >> return st

main :: IO ()
main = do
  a <- getArgs
  p <- fmap env_to_param getEnvironment
  print ("getenv/param=", p)
  s_fd <- openUdp "127.0.0.1" 57110 -- scsynth
  case a of
    ["load=f"] -> return ()
    ["load=t"] -> mapM_ (async s_fd) (Pf.pf_load_msg "008" 0)
    _ -> error "pf load=t|f"
  mapM_ (async s_fd) Smplr.smplr_recv_all_msg
  t0 <- time
  Osc.Server.midi_osc_processor_st (\_ -> note_on_off s_fd) (\_ -> return ()) (t0, 1, 1, p)
