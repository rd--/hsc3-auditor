-- \| historical - Stp variant of Fcd

import qualified Data.List {- base -}

import qualified Data.List.Split as Split {- split -}

import qualified Music.Theory.Opt as Opt {- hmt-base -}

import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Auditor.Common as Common {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Fcd as Fcd {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Smplr as Smplr {- hsc3-auditor -}

import qualified Sound.Sc3.Auditor.Fcd.Stp as Fcd.Stp {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Server.Smplr as Server.Smplr {- hsc3-auditor -}

opt_def :: [Opt.OptUsr]
opt_def =
  [ ("aT", "0", "float", "attack time")
  , ("amp0", "0", "float", "amplitude of 0")
  , ("amp1", "1", "float", "amplitude of 1")
  , ("b0", "0", "int", "sound file buffer zero index")
  , ("curve", "-4", "float", "envelope attack&release curve")
  , ("en", "En_Gate", "string", "en_mode = En_Gate | En_Dur | En_Buf")
  , ("grp0", "10", "int", "start index for auditor group and voice/stop groups")
  , ("grpP", "1", "int", "parent group id")
  , ("mwD", "0.1", "float", "mod-wheel depth")
  , ("nid0", "1000", "int", "node id for initial note")
  , ("pwD", "1.0", "float", "pitch-wheel depth (fractional midi-note interval)")
  , ("rT", "0", "float", "release time")
  , ("stp", "0", "list-of-int", "initial stop/program set")
  , ("syn", "_", "string", "synthdef name")
  ]

help :: [String]
help =
  [ "hsc3-auditor cmd opt"
  , ""
  , "  load-or-send-all instr b0:int stops:list-of-int"
  , "  rt midi-osc instr tun-type:cps|d12|tbl tun-name:string tun-f0|cents-diff:float tun-k:int [opt]"
  , "  send synthdef"
  , ""
  , "    instr:string = fcd|fcd-zc"
  , "    tun-name:string = name of scala tuning or file-name(s) of tuning table"
  , "    tun-f0:float/tun-k:int = f0 is the frequency of midi-note k (cps)"
  , "    tun-cents-diff:float/tun-k:int = fixed detune in cents and midi-note offset (d12 & tbl)"
  , ""
  , "Environment Variables = " ++ Data.List.intercalate "," Server.Smplr.smplr_env_param
  ]

words_comma :: String -> [String]
words_comma = Split.splitOn ","

load_or_send_all :: [String] -> IO ()
load_or_send_all a = do
  case a of
    ["fcd", b0, stp] -> Fcd.Stp.fcd_load_or_send_all (read b0) (read_int_list stp)
    ["fcd-zc", b0, stp] -> Fcd.Stp.fcd_load_or_send_all_zc (read b0) (read_int_list stp)
    _ -> putStrLn (unlines help)

rt_midi_osc :: String -> Server.Smplr.Server_Opt_A -> IO ()
rt_midi_osc ins opt_a = do
  let opt_b =
        case ins of
          "fcd" ->
            ( Fcd.Stp.fcd_stops_ix
            , Fcd.Stp.fcd_range_stp
            , \stp -> Fcd.fcd_akt_f (Fcd.Stp.fcd_stop_to_vc stp)
            , Common.Ch_Pan_1
            , Common.Lp_Buf
            , Fcd.Stp.fcd_smplr
            )
          _ -> error "rt_midi_osc?"
      g_init z = Fcd.Stp.fcd_grp_init z 14 -- ???
  Server.Smplr.rt_midi_osc_au (opt_a, opt_b, g_init)

read_int_list :: String -> [Int]
read_int_list = map read . words_comma

auditor :: ([Opt.Opt], [String]) -> Param -> IO ()
auditor (o, a) p =
  case a of
    "load-or-send-all" : a' -> load_or_send_all a'
    ["rt", "midi-osc", ins, ty, tn_nm, ty_f, k] ->
      let syn = Opt.opt_get o "syn"
          syn' = if syn == "_" then Nothing else Just syn
          grp = (Opt.opt_read o "grpP", Opt.opt_read o "grp0")
          curve = Opt.opt_read o "curve"
          ampl = (Opt.opt_read o "amp0", Opt.opt_read o "amp1")
          env = (Opt.opt_read o "aT", Opt.opt_read o "rT", curve, curve)
          tn = words_comma tn_nm
          opt =
            ( syn'
            , ty
            , Opt.opt_read o "b0"
            , grp
            , Opt.opt_read o "nid0"
            , tn
            , read ty_f
            , read k
            , ampl
            , env
            , Opt.opt_read o "pwD"
            , Opt.opt_read o "mwD"
            , p
            , read_int_list (Opt.opt_get o "stp")
            , Opt.opt_read o "en"
            )
      in rt_midi_osc ins opt
    ["send", "synthdef"] -> Smplr.smplr_load_all
    _ -> Opt.opt_usage help opt_def

main :: IO ()
main = do
  (o, a) <- Opt.opt_get_arg True help opt_def -- disallow syn opt (?)
  p <- Server.Smplr.smplr_read_env
  print ("getenv/param=", p)
  auditor (o, a) p
