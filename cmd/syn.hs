import System.Environment {- base -}

import qualified Music.Theory.Pitch as T {- hmt -}
import qualified Music.Theory.Tuning as T {- hmt -}
import qualified Music.Theory.Tuning.Load as T {- hmt -}
import qualified Music.Theory.Tuning.Midi as T {- hmt -}

import Sound.Osc.Fd {- hosc -}
import Sound.Sc3.Fd {- hsc3 -}

import qualified Sound.Midi.Csv as Csv {- midi-osc -}

import qualified Sound.Midi.Ky as Ky {- midi-osc -}
import qualified Sound.Midi.Osc.Server as Server {- midi-osc -}
import qualified Sound.Midi.Type as Midi {- midi-osc -}

import qualified Sound.Sc3.Auditor.Syn as Syn {- hsc3-auditor -}

type R = Double
type St = (Ky.Ky (Int, T.Midi), R) -- (ky,dt)
type Recv_Opt = (String, R, Int, (R, R), R, R, R)

init_f :: Int -> Server.Midi_Init_f St
init_f n0 _ = return (Ky.ky_init n0, 0)

print_csv_mnd :: Time -> Midi.Channel_Voice_Message T.Midi -> IO ()
print_csv_mnd t0 m = do
  t <- time
  maybe (return ()) putStrLn (Csv.cvm_to_csv_mnd (t - t0) m)

recv_f :: Recv_Opt -> T.Sparse_Midi_Tuning_f -> Server.Midi_Recv_f St T.Midi
recv_f (osc_ty, t0, grp, ampl, aT, rT, pb) tn_f fd (ky, dt) msg =
  let mk_a = linlin_hs (0, 1) ampl . (/ 127) . fromIntegral
      rmv ch mnn = do
        let (ky', z) = Ky.ky_free1 ky (ch, mnn)
        case z of
          Nothing -> return ()
          Just z' -> sendMessage fd (n_set z' [("gate", 0)])
        return (ky', dt)
      gen ch mnn vel =
        case tn_f mnn of
          Nothing -> return (ky, dt)
          Just mdt -> do
            let (ky', z) = Ky.ky_alloc1 ky (ch, mnn)
                mnn' = T.midi_detune_to_fmidi mdt
                osc_msg = Syn.syn_message (0, Syn.syn_nm (osc_ty, True), z, grp, aT, rT, []) dt mnn' (mk_a vel) Nothing
            sendMessage fd osc_msg
            return (ky', dt)
      detun _ c = do
        sendMessage fd (n_set grp [("pw", c)])
        return (ky, c)
  in print_csv_mnd t0 msg
      >> case msg of
        Midi.Note_Off ch mnn _ -> rmv ch mnn
        Midi.Note_On ch mnn vel -> gen ch mnn vel
        Midi.Pitch_Bend ch d1 d2 -> detun ch (urange (negate pb) pb (Midi.pitch_bend (0, 1) (d1, d2)))
        _ -> return (ky, dt)

{-
import Data.Maybe {- base -}
import qualified Music.Theory.Tuning.Scala as T {- hmt -}

       else do t0 <- T.scl_load_tuning 0.01 nm0
               t1 <- T.scl_load_tuning 0.01 nm1
               let tbl0 = T.gen_cps_tuning_tbl (tuning_f (t0,0,0)) -- constants...
                   tbl1 = T.gen_cps_tuning_tbl (tuning_f (t1,-74.7,3))
               return (T.gen_dtt_lookup_f tbl0 tbl1)
-}

gen_tuning_f :: String -> String -> String -> T.Cents -> T.Midi -> IO T.Sparse_Midi_Tuning_f
gen_tuning_f nm0 nm1 ty ty_f k =
  if nm1 == "nil"
    then T.load_tuning_ty ty (nm0, ty_f, k)
    else error "dtt not working..."

syn :: (String, String, Int, Int, String, String, R, T.Midi, R, R, R, R) -> IO ()
syn (osc_ty, tun_ty, grp, n0, tn_nm_0, tn_nm_1, ty_f, k, aT, rT, gn, pb) = do
  let ampl = (0, gn)
  t0 <- time
  tn_f <- gen_tuning_f tn_nm_0 tn_nm_1 tun_ty ty_f k
  let recv_opt = (osc_ty, t0, grp, ampl, aT, rT, pb)
  Server.run_midi (init_f n0) (recv_f recv_opt tn_f)

help :: [String]
help =
  ["syn sine|triangle-tone cps|d12 group:int n0:int tuning:string nil|tuning:string cents-diff|f0:float k:int aT:float rT:float gain:float pitch-bend/semi-tones:float"]

main :: IO ()
main = do
  a <- getArgs
  case a of
    [osc_ty, tun_ty, grp, n0, tn_nm_0, tn_nm_1, ty_f, k, aT, rT, gn, pb] ->
      withSc3 (\fd -> mapM_ (async fd . d_recv) [Syn.sine_tone True, Syn.triangle_tone True])
        >> syn (osc_ty, tun_ty, read grp, read n0, tn_nm_0, tn_nm_1, read ty_f, read k, read aT, read rT, read gn, read pb)
    _ -> putStrLn (unlines help)
