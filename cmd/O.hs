-- | Opt
module O where

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hosc -}

import qualified Music.Theory.Opt as Opt {- hmt-base -}

import qualified Sound.Sc3.Auditor.Common as Common
import qualified Sound.Sc3.Auditor.Meta as Meta
import qualified Sound.Sc3.Auditor.Smplr as Smplr

au_smplr_opt_def :: [Opt.OptUsr]
au_smplr_opt_def =
  -- Meta
  [ ("b0", "0", "int", "sound file buffer zero index")
  , ("dyn", "?", "string", "instrument dynamic")
  , ("instr", "?", "string", "instrument name")
  , ("vc", "?", "string", "instrument voice")
  , -- Smplr
    ("en", "En_Gate", "string", "end-mode")
  , ("lp", "Lp_None", "string", "loop-mode")
  , -- Plain
    ("grp", "1", "int", "group-id")
  , ("nid", "1000", "int", "node-id")
  , --
    ("tbl", "", "filename", "tuning table (midi-note-number,fractional-midi-note-number)")
  , ("v0", "0", "int", "velocity-minima")
  , ("v1", "1", "int", "velocity-maxima")
  ]

-- | (BufferZero, Dynamic, EndMode, GroupId, LoopMode, SynthId, TuningTable, KeyVelocityRange, VoiceName, OptionalParameters)
type Au_Usr_Opt = (Buffer_Id, Common.Dynamic_Name, Common.En_Mode, Group_Id, Common.Lp_Mode, Synth_Id, Maybe FilePath, (Double, Double), Common.Voice_Name, Param)

au_usr_opt_read :: [Opt.Opt] -> Au_Usr_Opt
au_usr_opt_read o =
  ( Opt.opt_read o "b0"
  , Opt.opt_get o "dyn"
  , Opt.opt_read o "en"
  , Opt.opt_read o "grp"
  , Opt.opt_read o "lp"
  , Opt.opt_read o "nid"
  , Opt.opt_get_nil o "tbl"
  , (Opt.opt_read o "v0", Opt.opt_read o "v1")
  , Opt.opt_get o "vc"
  , Smplr.smplr_param_all_opt o
  )

type Au_Opt = (String, Common.Akt_f, [Message])

au_smplr_opt :: String -> Au_Usr_Opt -> Au_Opt
au_smplr_opt instr (b0, dyn, en, _grp, lp, _nid, _tbl, _vel, vc, _prm_opt) =
  let (akt_f, rng, nc) = Meta.au_instr_dat (instr, vc)
      syn_opt = (Common.nc_to_ch_mode nc, en, lp)
      syn = Smplr.smplr_nm syn_opt
      akt_f_rw = Common.akt_f_rw_b0 b0 (Common.akt_f_rw_fold rng akt_f)
  in (syn, akt_f_rw, Meta.au_load_msg (instr, b0, vc, dyn))
