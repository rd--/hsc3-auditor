# smplr-sfz

`UI`

Very simple UI that reads a list of SFZ files from a text file.  The
SFZ file are indicated in a list view. Selecting an entry loads the
sound file data (if not already loaded) and runs an auditor at the
sample set.

~~~~
hsc3-smplr-sfz ui ~/sw/hsc3-auditor/etc/sfz.text
~~~~

`SYNTHDEF`

~~~~
hsc3-scsynth dump-osc 1
hsc3-smplr-sfz send synthdef
~~~~

`PF`

~~~~
dir=~/data/audio/instr/bosendorfer

hsc3-smplr-sfz load $dir/pf.sfz --b0=000
hsc3-smplr-sfz run $dir/pf.sfz --b0=000 --rT=1

hsc3-smplr-sfz load $dir/d012.sfz --b0=000
hsc3-smplr-sfz run $dir/d012.sfz --b0=000 --rT=1

hsc3-smplr-sfz load $dir/d060.sfz --b0=100
hsc3-smplr-sfz run $dir/d060.sfz --b0=100 --rT=1

hsc3-smplr-sfz load $dir/008.sfz --b0=000
hsc3-smplr-sfz run $dir/008.sfz --b0=000 --rT=1

hsc3-smplr-sfz load $dir/032.sfz --b0=100
hsc3-smplr-sfz run $dir/032.sfz --b0=100 --rT=1

hsc3-smplr-sfz load $dir/040.sfz --b0=200
hsc3-smplr-sfz run $dir/040.sfz --b0=200 --rT=1
~~~~

`FCD`

~~~~
dir=~/data/audio/instr/farfisa/aad/

hsc3-smplr-sfz load $dir/flute8.sfz --b0=000
hsc3-smplr-sfz run $dir/flute8.sfz --b0=000

hsc3-smplr-sfz load $dir/oboe8.sfz --b0=050
hsc3-smplr-sfz run $dir/oboe8.sfz --b0=050

hsc3-smplr-sfz load $dir/trumpet8.sfz --b0=100
hsc3-smplr-sfz run $dir/trumpet8.sfz --b0=100

hsc3-smplr-sfz load $dir/strings8.sfz --b0=150
hsc3-smplr-sfz run $dir/strings8.sfz --b0=150

hsc3-smplr-sfz load $dir/green-16.sfz --b0=200
hsc3-smplr-sfz run $dir/green-16.sfz --b0=200

hsc3-smplr-sfz load $dir/green-2+2_3.sfz --b0=250
hsc3-smplr-sfz run $dir/green-2+2_3.sfz --b0=250

hsc3-smplr-sfz load $dir/dolce-8.sfz --b0=300
hsc3-smplr-sfz run $dir/dolce-8.sfz --b0=300

hsc3-smplr-sfz load $dir/principale-8.sfz --b0=350
hsc3-smplr-sfz run $dir/principale-8.sfz --b0=350

hsc3-smplr-sfz load $dir/bass-sharp.sfz --b0=400
hsc3-smplr-sfz run $dir/bass-sharp.sfz --b0=400

hsc3-smplr-sfz load $dir/bass-soft.sfz --b0=450
hsc3-smplr-sfz run $dir/bass-soft.sfz --b0=450
~~~~

`FCD-ZC`

~~~~
dir=~/data/audio/instr/farfisa/aad/

hsc3-smplr-sfz load $dir/flute8-zc.sfz --b0=000
hsc3-smplr-sfz run $dir/flute8-zc.sfz --b0=000

hsc3-smplr-sfz load $dir/oboe8-zc.sfz --b0=050
hsc3-smplr-sfz run $dir/oboe8-zc.sfz --b0=050

hsc3-smplr-sfz load $dir/trumpet8-zc.sfz --b0=100
hsc3-smplr-sfz run $dir/trumpet8-zc.sfz --b0=100

hsc3-smplr-sfz load $dir/strings8-zc.sfz --b0=150
hsc3-smplr-sfz run $dir/strings8-zc.sfz --b0=150

hsc3-smplr-sfz load $dir/green-16-zc.sfz --b0=200
hsc3-smplr-sfz run $dir/green-16-zc.sfz --b0=200

hsc3-smplr-sfz load $dir/green-2+2_3-zc.sfz --b0=250
hsc3-smplr-sfz run $dir/green-2+2_3-zc.sfz --b0=250

hsc3-smplr-sfz load $dir/dolce-8-zc.sfz --b0=300
hsc3-smplr-sfz run $dir/dolce-8-zc.sfz --b0=300

hsc3-smplr-sfz load $dir/principale-8-zc.sfz --b0=350
hsc3-smplr-sfz run $dir/principale-8-zc.sfz --b0=350

hsc3-smplr-sfz load $dir/bass-sharp-zc.sfz --b0=400
hsc3-smplr-sfz run $dir/bass-sharp-zc.sfz --b0=400

hsc3-smplr-sfz load $dir/bass-soft-zc.sfz --b0=450
hsc3-smplr-sfz run $dir/bass-soft-zc.sfz --b0=450
~~~~

`ML`

~~~~
dir=~/data/audio/instr/mellotron

hsc3-smplr-sfz load $dir/MkIIBrass.sfz --b0=000
hsc3-smplr-sfz run $dir/MkIIBrass.sfz --b0=000

hsc3-smplr-sfz load $dir/MkIIFlute.sfz --b0=100
hsc3-smplr-sfz run $dir/MkIIFlute.sfz --b0=100 --r_pan=0.75 --rT=0.1

hsc3-smplr-sfz load $dir/MkIIViolins.sfz --b0=200
hsc3-smplr-sfz run $dir/MkIIViolins.sfz --b0=200 --aT=0.65

hsc3-smplr-sfz load $dir/M300A.sfz --b0=300
hsc3-smplr-sfz run $dir/M300A.sfz --b0=300

hsc3-smplr-sfz load $dir/M300B.sfz --b0=400
hsc3-smplr-sfz run $dir/M300B.sfz --b0=400 --aT=0.35

hsc3-smplr-sfz load $dir/CombinedChoir.sfz --b0=500
hsc3-smplr-sfz run $dir/CombinedChoir.sfz --b0=500 --aT=0.35

hsc3-smplr-sfz load $dir/Cello.sfz --b0=600
hsc3-smplr-sfz run $dir/Cello.sfz --b0=600

hsc3-smplr-sfz load $dir/StringSection.sfz --b0=700
hsc3-smplr-sfz run $dir/StringSection.sfz --b0=700

hsc3-smplr-sfz load $dir/Woodwind2.sfz --b0=800
hsc3-smplr-sfz run $dir/Woodwind2.sfz --b0=800

hsc3-smplr-sfz load $dir/GC3Brass.sfz --b0=900
hsc3-smplr-sfz run $dir/GC3Brass.sfz --b0=900
~~~~

`CDM`

~~~~

hsc3-smplr-sfz load ~/data/audio/instr/CDM-GAMELAN/gssl.sfz --b0=000
hsc3-smplr-sfz run ~/data/audio/instr/CDM-GAMELAN/gssl.sfz --b0=000

hsc3-smplr load cdm-gspl --b0=100
hsc3-smplr run cdm-gspl --b0=100 --en=En_Buf

hsc3-smplr load cdm-gpsl --b0=200
hsc3-smplr run cdm-gpsl --b0=200 --en=En_Buf
~~~~

`CL`

~~~~
hsc3-smplr-sfz load ~/data/audio/instr/clavichord/cl.sfz --b0=0
hsc3-smplr-sfz run ~/data/audio/instr/clavichord/cl.sfz --b0=0
~~~~

`SM`

~~~~
dir=~/data/audio/instr/soni-musicae

hsc3-smplr-sfz load $dir/Blanchet-1720/I.sfz --b0=000
hsc3-smplr-sfz run $dir/Blanchet-1720/I.sfz --b0=000 --v0=1 --en=En_Buf

hsc3-smplr-sfz load $dir/Blanchet-1720/II.sfz --b0=100
hsc3-smplr-sfz run $dir/Blanchet-1720/II.sfz --b0=100 --v0=1 --en=En_Buf

hsc3-smplr-sfz load $dir/Blanchet-1720/III.sfz --b0=200
hsc3-smplr-sfz run $dir/Blanchet-1720/III.sfz --b0=200 --v0=1 --en=En_Buf

dir=~/data/audio/instr/soni-musicae

hsc3-smplr-sfz load $dir/Diato/I.sfz --b0=000
hsc3-smplr-sfz run $dir/Diato/I.sfz --b0=000

hsc3-smplr-sfz load $dir/Diato/II.sfz --b0=100
hsc3-smplr-sfz run $dir/Diato/II.sfz --b0=100

dir=~/data/audio/instr/soni-musicae

hsc3-smplr-sfz load $dir/PF-Yamaha-CF3/p.sfz --b0=000
hsc3-smplr-sfz run $dir/PF-Yamaha-CF3/p.sfz --b0=000 --rT=0.5

hsc3-smplr-sfz load $dir/PF-Yamaha-CF3/mp.sfz --b0=100
hsc3-smplr-sfz run $dir/PF-Yamaha-CF3/mp.sfz --b0=100 --rT=0.5

hsc3-smplr-sfz load $dir/PF-Yamaha-CF3/mf.sfz --b0=200
hsc3-smplr-sfz run $dir/PF-Yamaha-CF3/mf.sfz --b0=200 --rT=0.5

dir=~/data/audio/instr/soni-musicae

hsc3-smplr-sfz load $dir/Petit-Italien/I.sfz --b0=000
hsc3-smplr-sfz run $dir/Petit-Italien/I.sfz --b0=000 --en=En_Buf

hsc3-smplr-sfz load $dir/Petit-Italien/II.sfz --b0=100
hsc3-smplr-sfz run $dir/Petit-Italien/II.sfz --b0=100 --en=En_Buf

hsc3-smplr-sfz load $dir/Petit-Italien/III.sfz --b0=200
hsc3-smplr-sfz run $dir/Petit-Italien/III.sfz --b0=200 --en=En_Buf

dir=~/data/audio/instr/soni-musicae

hsc3-smplr-sfz load $dir/Orgue-de-Salon/B.sfz --b0=000
hsc3-smplr-sfz run $dir/Orgue-de-Salon/B.sfz --b0=000

hsc3-smplr-sfz load $dir/Orgue-de-Salon/C.sfz --b0=100
hsc3-smplr-sfz run $dir/Orgue-de-Salon/C.sfz --b0=100

hsc3-smplr-sfz load $dir/Orgue-de-Salon/F.sfz --b0=200
hsc3-smplr-sfz run $dir/Orgue-de-Salon/F.sfz --b0=200

hsc3-smplr-sfz load $dir/Orgue-de-Salon/F2.sfz --b0=300
hsc3-smplr-sfz run $dir/Orgue-de-Salon/F2.sfz --b0=300

hsc3-smplr-sfz load $dir/Orgue-de-Salon/O.sfz --b0=400
hsc3-smplr-sfz run $dir/Orgue-de-Salon/O.sfz --b0=400

dir=~/data/audio/instr/soni-musicae

hsc3-smplr-sfz load $dir/Carillon-de-Gand/gc.sfz --b0=000
hsc3-smplr-sfz run $dir/Carillon-de-Gand/gc.sfz --b0=000

dir=~/data/audio/instr/soni-musicae

hsc3-smplr-sfz load $dir/Italian-Spinet/I.sfz --b0=000
hsc3-smplr-sfz run $dir/Italian-Spinet/I.sfz --b0=000

hsc3-smplr-sfz load $dir/Italian-Spinet/II.sfz --b0=100
hsc3-smplr-sfz run $dir/Italian-Spinet/II.sfz --b0=100
~~~~

`CROTALES`

~~~~
hsc3-smplr-sfz load ~/data/audio/instr/crotales/crotales.sfz --b0=000
hsc3-smplr-sfz run ~/data/audio/instr/crotales/crotales.sfz --b0=000 --en=En_Buf
~~~~

`A37Z` & `A37Z-ZC`

~~~~
dir=~/data/audio/instr/audiosonic

hsc3-smplr-sfz load $dir/flute.sfz --b0=000
hsc3-smplr-sfz run $dir/flute.sfz --b0=000

hsc3-smplr-sfz load $dir/oboe.sfz --b0=050
hsc3-smplr-sfz run $dir/oboe.sfz --b0=050

hsc3-smplr-sfz load $dir/strings.sfz --b0=100
hsc3-smplr-sfz run $dir/strings.sfz --b0=100

hsc3-smplr-sfz load $dir/flute-zc.sfz --b0=000
hsc3-smplr-sfz run $dir/flute-zc.sfz --b0=000

hsc3-smplr-sfz load $dir/oboe-zc.sfz --b0=050
hsc3-smplr-sfz run $dir/oboe-zc.sfz --b0=050

hsc3-smplr-sfz load $dir/strings-zc.sfz --b0=100
hsc3-smplr-sfz run $dir/strings-zc.sfz --b0=100
~~~~

`CASACOTA` - `ESMUC`

~~~~
dir=~/data/audio/instr/casacota/orgue_esmuc_415_Werckmeister3

hsc3-smplr-sfz load $dir/om_flautat.sfz --b0=000
hsc3-smplr-sfz run $dir/om_flautat.sfz --b0=000 --rT=0.35

hsc3-smplr-sfz load $dir/om_octava.sfz --b0=100
hsc3-smplr-sfz run $dir/om_octava.sfz --b0=100 --rT=0.35

hsc3-smplr-sfz load $dir/om_dotzena.sfz --b0=200
hsc3-smplr-sfz run $dir/om_dotzena.sfz --b0=200 --rT=0.35

hsc3-smplr-sfz load $dir/om_nasard_17.sfz --b0=300
hsc3-smplr-sfz run $dir/om_nasard_17.sfz --b0=300 --rT=0.35

hsc3-smplr-sfz load $dir/om_plens.sfz --b0=400
hsc3-smplr-sfz run $dir/om_plens.sfz --b0=400 --rT=0.35

hsc3-smplr-sfz load $dir/po_bordo.sfz --b0=500
hsc3-smplr-sfz run $dir/po_bordo.sfz --b0=500 --rT=0.35

hsc3-smplr-sfz load $dir/po_xemeneia.sfz --b0=600
hsc3-smplr-sfz run $dir/po_xemeneia.sfz --b0=600 --rT=0.35

hsc3-smplr-sfz load $dir/po_quinzena.sfz --b0=700
hsc3-smplr-sfz run $dir/po_quinzena.sfz --b0=700 --rT=0.35

hsc3-smplr-sfz load $dir/po_oboe.sfz --b0=800
hsc3-smplr-sfz run $dir/po_oboe.sfz --b0=800 --rT=0.35

hsc3-smplr-sfz load $dir/pedal_16.sfz --b0=900
hsc3-smplr-sfz run $dir/pedal_16.sfz --b0=900 --rT=0.35

hsc3-smplr-sfz load $dir/pedal_8.sfz --b0=1000
hsc3-smplr-sfz run $dir/pedal_8.sfz --b0=1000 --rT=0.35
~~~~

`CASACOTA` - `Zell 1737`

~~~~
dir=~/data/audio/instr/casacota/zell_1737_415_MeanTone5

hsc3-smplr-sfz load $dir/8_i.sfz --b0=000
hsc3-smplr-sfz run $dir/8_i.sfz --b0=000 --rT=1

hsc3-smplr-sfz load $dir/8_ii.sfz --b0=100
hsc3-smplr-sfz run $dir/8_ii.sfz --b0=100 --rT=0.35

hsc3-smplr-sfz load $dir/4.sfz --b0=200
hsc3-smplr-sfz run $dir/4.sfz --b0=200 --rT=0.35

hsc3-smplr-sfz load $dir/8_llaut.sfz --b0=300
hsc3-smplr-sfz run $dir/8_llaut.sfz --b0=300 --rT=0.35

hsc3-smplr-sfz load $dir/8_i-et12.sfz --b0=000
hsc3-smplr-sfz run $dir/8_i-et12.sfz --b0=000 --rT=1
~~~~

`CASACOTA` - `Hauslaib 1590`

~~~~
dir=~/data/audio/instr/casacota/Hauslaib_1590_415_MeanTone4/

hsc3-smplr-sfz load $dir/flauteadillos.sfz --b0=000
hsc3-smplr-sfz run $dir/flauteadillos.sfz --b0=000

hsc3-smplr-sfz load $dir/realejos_de_batalla.sfz --b0=050
hsc3-smplr-sfz run $dir/realejos_de_batalla.sfz --b0=050 --rT=0.35

hsc3-smplr-sfz load $dir/realejos.sfz --b0=100
hsc3-smplr-sfz run $dir/realejos.sfz --b0=100 --rT=0.35

hsc3-smplr-sfz load $dir/monacordio.sfz --b0=150
hsc3-smplr-sfz run $dir/monacordio.sfz --b0=150 --rT=0.35

hsc3-smplr-sfz load $dir/beintidosenas.sfz --b0=200
hsc3-smplr-sfz run $dir/beintidosenas.sfz --b0=200 --rT=0.35

hsc3-smplr-sfz load $dir/quinzenas.sfz --b0=250
hsc3-smplr-sfz run $dir/quinzenas.sfz --b0=250 --rT=0.35
~~~~

`Vienna`

~~~~
dir=~/data/audio/instr/vienna

hsc3-smplr-sfz load $dir/Woodwind/FL2/FL2_pV_nA_sus_pp.sfz --b0=000
hsc3-smplr-sfz run $dir/Woodwind/FL2/FL2_pV_nA_sus_pp.sfz --b0=000

hsc3-smplr-sfz load $dir/Harp/HARP_1/HA_ES_mf.sfz --b0=000
hsc3-smplr-sfz run $dir/Harp/HARP_1/HA_ES_mf.sfz --b0=000 --en=En_Buf

hsc3-smplr-sfz load $dir/Harp/HARP_1/HA_ES_pdlt_mp.sfz --b0=100
hsc3-smplr-sfz run $dir/Harp/HARP_1/HA_ES_pdlt_mp.sfz --b0=100 --en=En_Buf

hsc3-smplr-sfz load $dir/Harp/HARP_1/HA_RS_ES_mu_p.sfz --b0=200
hsc3-smplr-sfz run $dir/Harp/HARP_1/HA_RS_ES_mu_p.sfz --b0=200 --en=En_Buf

hsc3-smplr-sfz load $dir/Harp/HARP_1/HA_RS_bisb_mp.sfz --b0=300
hsc3-smplr-sfz run $dir/Harp/HARP_1/HA_RS_bisb_mp.sfz --b0=300 --en=En_Buf

hsc3-smplr load vn-ho_LV_nA_sus_mp --b0=000
hsc3-smplr run vn-ho_LV_nA_sus_mp --b0=000

hsc3-smplr load vn-ho_oV_nA_sus_mp --b0=100
hsc3-smplr run vn-ho_oV_nA_sus_mp --b0=100

hsc3-smplr-sfz load $dir/Woodwind/KLB/KLB_pA_sus_p.sfz --b0=000
hsc3-smplr-sfz run $dir/Woodwind/KLB/KLB_pA_sus_p.sfz --b0=000

hsc3-smplr load vn-klb_stac_p1 --b0=100
hsc3-smplr run vn-klb_stac_p1 --b0=100 --en=En_Buf

hsc3-smplr load vn-po_nA_sus_p --b0=000
hsc3-smplr run vn-po_nA_sus_p --b0=000

hsc3-smplr load vn-trc_oV_nA_sus_mp --b0=000
hsc3-smplr run vn-trc_oV_nA_sus_mp --b0=000

hsc3-smplr load vn-tu_oV_nA_sus_mp --b0=000
hsc3-smplr run vn-tu_oV_nA_sus_mp --b0=000

hsc3-smplr load vn-vi_mV_sus_mf --b0=0
hsc3-smplr run vn-vi_mV_sus_mf --rT=0.35

hsc3-smplr load vn-va_mV_sus_mf --b0=0
hsc3-smplr run vn-va_mV_sus_mf --rT=0.35

hsc3-smplr load vn-vc_mV_sus_mf --b0=0
hsc3-smplr run vn-vc_mV_sus_mf --rT=0.35
~~~~

`Philharmonia`

~~~~
dir=~/data/audio/instr/philharmonia

hsc3-smplr-sfz load $dir/bass-clarinet/bcl.sfz --b0=000
hsc3-smplr-sfz run $dir/bass-clarinet/bcl.sfz --b0=000 --rT=0.35
~~~~

`FAIRLIGHT-CMI`

~~~~
dir=~/rd/j/2019-04-21/FAIRLIGHT/IIX

hsc3-smplr-sfz load $dir/VOICES/sararr1.sfz --b0=0
hsc3-smplr-sfz run $dir/VOICES/sararr1.sfz --b0=0
~~~~
