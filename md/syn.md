# syn

Send `midi-osc` note data to _synthesiser_ definitions at `SC3`.

~~~~
midi-osc-syn sine-tone d12 1 1000 young-lm_piano nil -74.7 -3 0.05 4 0.1 1
midi-osc-syn triangle-tone d12 1 1000 et12 nil 0 0 0.05 0.1 0.1 1
~~~~
