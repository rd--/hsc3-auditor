# smplr

`SYNTHDEF`

~~~~
hsc3-scsynth dump-osc 1
hsc3-smplr send synthdef
~~~~

`PF`

~~~~
hsc3-smplr load pf --b0=000 --dyn=d012
hsc3-smplr run pf --b0=000 --rT=1

hsc3-smplr load pf --b0=100 --dyn=d060
hsc3-smplr run pf --b0=100

hsc3-smplr load pf --b0=200 --dyn=d084
hsc3-smplr run pf --b0=200

hsc3-smplr load pf --b0=300 --dyn=d108
hsc3-smplr run pf --b0=300

hsc3-smplr load pf --b0=400 --dyn=d127
hsc3-smplr run pf --b0=400

hsc3-smplr load pf --b0=000 --dyn=008
hsc3-smplr run pf --b0=000

hsc3-smplr load pf --b0=100 --dyn=032
hsc3-smplr run pf --b0=100

hsc3-smplr load pf --b0=200 --dyn=040
hsc3-smplr run pf --b0=200

hsc3-smplr load pf --b0=300 --dyn=056
hsc3-smplr run pf --b0=300

hsc3-smplr load pf --b0=400 --dyn=064
hsc3-smplr run pf --b0=400

hsc3-smplr load pf --b0=500 --dyn=072
hsc3-smplr run pf --b0=500

hsc3-smplr load pf --b0=600 --dyn=080
hsc3-smplr run pf --b0=600

hsc3-smplr load pf --b0=700 --dyn=096
hsc3-smplr run pf --b0=700

hsc3-smplr load pf --b0=800 --dyn=104
hsc3-smplr run pf --b0=800

hsc3-smplr load pf --b0=900 --dyn=112
hsc3-smplr run pf --b0=900

hsc3-smplr load pf --b0=1000 --dyn=128
hsc3-smplr run pf --b0=1000
~~~~

`FCD` - ZC

~~~~
hsc3-smplr load fcd-zc --b0=000 --vc=flute8
hsc3-smplr run fcd --b0=000 --vc=flute8 --aT=0 --rT=0 --lp=LP_BUF

hsc3-smplr load fcd-zc --b0=050 --vc=oboe8
hsc3-smplr run fcd --b0=050 --vc=oboe8 --aT=0 --rT=0 --lp=LP_BUF

hsc3-smplr load fcd-zc --b0=100 --vc=trumpet8
hsc3-smplr run fcd --b0=100 --vc=trumpet8 --aT=0 --rT=0 --lp=LP_BUF

hsc3-smplr load fcd-zc --b0=150 --vc=strings8
hsc3-smplr run fcd --b0=150 --vc=strings8 --aT=0 --rT=0 --lp=LP_BUF

hsc3-smplr load fcd-zc --b0=200 --vc=green-16
hsc3-smplr run fcd --b0=200 --vc=green-16 --aT=0 --rT=0 --lp=LP_BUF

hsc3-smplr load fcd-zc --b0=250 --vc=green-2+2_3
hsc3-smplr run fcd --b0=250 --vc=green-2+2_3 --aT=0 --rT=0 --lp=LP_BUF

hsc3-smplr load fcd-zc --b0=300 --vc=dolce-8
hsc3-smplr run fcd --b0=300 --vc=dolce-8 --aT=0 --rT=0 --lp=LP_BUF

hsc3-smplr load fcd-zc --b0=350 --vc=principale-8
hsc3-smplr run fcd --b0=350 --vc=principale-8 --aT=0 --rT=0 --lp=LP_BUF

hsc3-smplr load fcd-zc --b0=400 --vc=bass-sharp
hsc3-smplr run fcd --b0=400 --vc=bass-sharp --aT=0 --rT=0 --lp=LP_BUF

hsc3-smplr load fcd-zc --b0=450 --vc=bass-soft
hsc3-smplr run fcd --b0=450 --vc=bass-soft --aT=0 --rT=0 --lp=LP_BUF
~~~~

`ML`

~~~~
hsc3-smplr load ml --b0=0 --vc=MkIIBrass
hsc3-smplr run ml --b0=0

hsc3-smplr load ml --b0=100 --vc=MkIIFlute
hsc3-smplr run ml --b0=100 --r_pan=0.75 --rT=0.1

hsc3-smplr load ml --b0=200 --vc=MkIIViolins
hsc3-smplr run ml --b0=200 --aT=0.65

hsc3-smplr load ml --b0=300 --vc=M300A
hsc3-smplr run ml --b0=300

hsc3-smplr load ml --b0=400 --vc=M300B
hsc3-smplr run ml --b0=400 --aT=0.35

hsc3-smplr load ml --b0=500 --vc=CombinedChoir
hsc3-smplr run ml --b0=500 --aT=0.35

hsc3-smplr load ml --b0=600 --vc=Cello
hsc3-smplr run ml --b0=600

hsc3-smplr load ml --b0=700 --vc=StringSection
hsc3-smplr run ml --b0=700

hsc3-smplr load ml --b0=800 --vc=Woodwind2
hsc3-smplr run ml --b0=800

hsc3-smplr load ml --b0=900 --vc=GC3Brass
hsc3-smplr run ml --b0=900
~~~~

`CDM`

~~~~
hsc3-smplr load cdm-gssl --b0=000
hsc3-smplr run cdm-gssl --b0=000 --en=En_Buf

hsc3-smplr load cdm-gspl --b0=100
hsc3-smplr run cdm-gspl --b0=100 --en=En_Buf

hsc3-smplr load cdm-gpsl --b0=200
hsc3-smplr run cdm-gpsl --b0=200 --en=En_Buf
~~~~

`CL`

~~~~
hsc3-smplr load cl --b0=0
hsc3-smplr run cl --b0=0
~~~~

`SM`

~~~~
hsc3-smplr load sm-bl --b0=000 --vc=I
hsc3-smplr run sm-bl --b0=000 --v0=1 --en=En_Buf

hsc3-smplr load sm-bl --b0=100 --vc=II
hsc3-smplr run sm-bl --b0=100 --v0=1 --en=En_Buf

hsc3-smplr load sm-bl --b0=200 --vc=III
hsc3-smplr run sm-bl --b0=200 --v0=1 --en=En_Buf

hsc3-smplr load sm-di --b0=000 --vc=I
hsc3-smplr run sm-di --b0=000 --rT=0.15

hsc3-smplr load sm-di --b0=100 --vc=II
hsc3-smplr run sm-di --b0=100 --rT=0.15

hsc3-smplr load sm-pf --b0=000 --dyn=p
hsc3-smplr run sm-pf --b0=000 --rT=0.5

hsc3-smplr load sm-pf --b0=100 --dyn=mp
hsc3-smplr run sm-pf --b0=100 --rT=0.5

hsc3-smplr load sm-pi --b0=000 --vc=I
hsc3-smplr run sm-pi --b0=000 --en=En_Buf

hsc3-smplr load sm-pi --b0=100 --vc=II
hsc3-smplr run sm-pi --b0=100 --en=En_Buf

hsc3-smplr load sm-os --b0=000 --vc=B
hsc3-smplr run sm-os --b0=000 --rT=0.2

hsc3-smplr load sm-os --b0=100 --vc=C
hsc3-smplr run sm-os --b0=100 --rT=0.2

hsc3-smplr load sm-os --b0=200 --vc=F
hsc3-smplr run sm-os --b0=200 --rT=0.2

hsc3-smplr load sm-os --b0=300 --vc=F2
hsc3-smplr run sm-os --b0=300 --rT=0.2

hsc3-smplr load sm-os --b0=400 --vc=O
hsc3-smplr run sm-os --b0=400 --rT=0.2

hsc3-smplr load sm-si --b0=000 --vc=I
hsc3-smplr run sm-si --b0=000 --rT=0.2

hsc3-smplr load sm-si --b0=100 --vc=II
hsc3-smplr run sm-si --b0=100 --rT=0.2
~~~~

`CROTALES`

~~~~
hsc3-smplr load crotales --b0=000
hsc3-smplr run crotales --b0=000 --en=En_Buf
~~~~

`A37Z`

~~~~
hsc3-smplr load a37z --b0=000 --vc=flute
hsc3-smplr load a37z-zc --b0=000 --vc=flute
hsc3-smplr run a37z --b0=000 --rT=0.05 --lfo_depth=0.02

hsc3-smplr load a37z --b0=050 --vc=oboe
hsc3-smplr load a37z-zc --b0=050 --vc=oboe
hsc3-smplr run a37z --b0=050 --rT=0.05 --lfo_depth=0.02

hsc3-smplr load a37z-zc --b0=100 --vc=strings
hsc3-smplr run a37z --b0=100 --rT=0.05 --lfo_depth=0.02

hsc3-smplr load a37z-zc --b0=150 --vc=flute-vib
hsc3-smplr run a37z --b0=150 --rT=0.05 --lfo_depth=0.02
~~~~

Casacota - `ESMUC`

~~~~
hsc3-smplr load ca-esmuc --b0=000 --vc=om_flautat
hsc3-smplr run ca-esmuc --b0=000 --rT=0.35

hsc3-smplr load ca-esmuc --b0=100 --vc=om_octava
hsc3-smplr run ca-esmuc --b0=100 --rT=0.35

hsc3-smplr load ca-esmuc --b0=200 --vc=om_dotzena
hsc3-smplr run ca-esmuc --b0=200 --rT=0.35

hsc3-smplr load ca-esmuc --b0=300 --vc=om_nasard_17
hsc3-smplr run ca-esmuc --b0=300 --rT=0.35

hsc3-smplr load ca-esmuc --b0=400 --vc=om_plens
hsc3-smplr run ca-esmuc --b0=400 --rT=0.35

hsc3-smplr load ca-esmuc --b0=500 --vc=po_bordo
hsc3-smplr run ca-esmuc --b0=500 --rT=0.35

hsc3-smplr load ca-esmuc --b0=600 --vc=po_xemeneia
hsc3-smplr run ca-esmuc --b0=600 --rT=0.35

hsc3-smplr load ca-esmuc --b0=700 --vc=po_quinzena
hsc3-smplr run ca-esmuc --b0=700 --rT=0.35

hsc3-smplr load ca-esmuc --b0=800 --vc=po_oboe
hsc3-smplr run ca-esmuc --b0=800 --rT=0.35

hsc3-smplr load ca-esmuc-pedal --b0=900 --vc=pedal_16
hsc3-smplr run ca-esmuc-pedal --b0=900 --rT=0.35

hsc3-smplr load ca-esmuc-pedal --b0=1000 --vc=pedal_8
hsc3-smplr run ca-esmuc-pedal --b0=1000 --rT=0.35
~~~~

Casacota - `ESMUC` - LP

~~~~
hsc3-smplr load ca-esmuc-lp --b0=000 --vc=om_flautat
hsc3-smplr run ca-esmuc --b0=000 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-esmuc-lp --b0=100 --vc=om_octava
hsc3-smplr run ca-esmuc --b0=100 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-esmuc-lp --b0=200 --vc=om_dotzena
hsc3-smplr run ca-esmuc --b0=200 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-esmuc-lp --b0=300 --vc=om_nasard_17
hsc3-smplr run ca-esmuc --b0=300 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-esmuc-lp --b0=400 --vc=om_plens
hsc3-smplr run ca-esmuc --b0=400 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-esmuc-lp --b0=500 --vc=po_bordo
hsc3-smplr run ca-esmuc --b0=500 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-esmuc-lp --b0=600 --vc=po_xemeneia
hsc3-smplr run ca-esmuc --b0=600 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-esmuc-lp --b0=700 --vc=po_quinzena
hsc3-smplr run ca-esmuc --b0=700 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-esmuc-lp --b0=800 --vc=po_oboe
hsc3-smplr run ca-esmuc --b0=800 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-esmuc-pedal-lp --b0=900 --vc=pedal_16
hsc3-smplr run ca-esmuc-pedal --b0=900 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-esmuc-pedal-lp --b0=1000 --vc=pedal_8
hsc3-smplr run ca-esmuc-pedal --b0=1000 --aT=0.1 --rT=0.35 --lp=LP_BUF
~~~~

Casacota - `Zell 1737`

~~~~
hsc3-smplr load ca-zell --b0=000 --vc=8_i
hsc3-smplr run ca-zell --b0=000 --rT=0.35 --en=En_Buf

hsc3-smplr load ca-zell --b0=100 --vc=8_ii
hsc3-smplr run ca-zell --b0=100 --rT=0.35 --en=En_Buf

hsc3-smplr load ca-zell --b0=200 --vc=4
hsc3-smplr run ca-zell --b0=200 --rT=0.35 --en=En_Buf

hsc3-smplr load ca-zell --b0=300 --vc=8_llaut
hsc3-smplr run ca-zell --b0=300 --rT=0.35 --en=En_Buf
~~~~

Casacota - `Hauslaib 1590`

~~~~
hsc3-smplr load ca-hauslaib --b0=000 --vc=flauteadillos
hsc3-smplr run ca-hauslaib --b0=000 --rT=0.35

hsc3-smplr load ca-hauslaib --b0=050 --vc=realejos_de_batalla
hsc3-smplr run ca-hauslaib --b0=050 --rT=0.35

hsc3-smplr load ca-hauslaib --b0=100 --vc=realejos
hsc3-smplr run ca-hauslaib --b0=100 --rT=0.35

hsc3-smplr load ca-hauslaib --b0=150 --vc=monacordio
hsc3-smplr run ca-hauslaib --b0=150 --rT=0.35

hsc3-smplr load ca-hauslaib --b0=200 --vc=beintidosenas
hsc3-smplr run ca-hauslaib --b0=200 --rT=0.35

hsc3-smplr load ca-hauslaib --b0=250 --vc=quinzenas
hsc3-smplr run ca-hauslaib --b0=250 --rT=0.35
~~~~

Casacota - `Hauslaib 1590` - LP

~~~~
hsc3-smplr load ca-hauslaib-lp --b0=000 --vc=flauteadillos
hsc3-smplr run ca-hauslaib --b0=000 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-hauslaib-lp --b0=050 --vc=realejos_de_batalla
hsc3-smplr run ca-hauslaib --b0=050 --aT=0.1 --rT=0.35 --lp=LP_BUF

hsc3-smplr load ca-hauslaib-lp --b0=100 --vc=realejos
hsc3-smplr run ca-hauslaib --b0=100 --aT=0.1 --rT=0.35 --lp=LP_BUF
~~~~

`Vienna`

~~~~
hsc3-smplr load vn-fl2_pV_nA_sus_pp --b0=000
hsc3-smplr run vn-fl2_pV_nA_sus_pp --b0=000 --rT=0.35

hsc3-smplr load vn-ha_es_mf --b0=000
hsc3-smplr run vn-ha_es_mf --b0=000 --en=En_Buf

hsc3-smplr load vn-ha_es_pdlt_mp --b0=100
hsc3-smplr run vn-ha_es_pdlt_mp --b0=100 --en=En_Buf

hsc3-smplr load vn-ha_rs_es_mu_p --b0=200
hsc3-smplr run vn-ha_rs_es_mu_p --b0=200 --en=En_Buf

hsc3-smplr load vn-ha_rs_bisb_mp --b0=300
hsc3-smplr run vn-ha_rs_bisb_mp --b0=300 --en=En_Buf

hsc3-smplr load vn-ho_LV_nA_sus_mp --b0=000
hsc3-smplr run vn-ho_LV_nA_sus_mp --b0=000

hsc3-smplr load vn-ho_oV_nA_sus_mp --b0=100
hsc3-smplr run vn-ho_oV_nA_sus_mp --b0=100

hsc3-smplr load vn-klb_pA_sus_p --b0=000
hsc3-smplr run vn-klb_pA_sus_p --b0=000 --rT=0.35

hsc3-smplr load vn-klb_stac_p1 --b0=100
hsc3-smplr run vn-klb_stac_p1 --b0=100 --en=En_Buf

hsc3-smplr load vn-po_nA_sus_p --b0=000
hsc3-smplr run vn-po_nA_sus_p --b0=000

hsc3-smplr load vn-trc_oV_nA_sus_mp --b0=000
hsc3-smplr run vn-trc_oV_nA_sus_mp --b0=000

hsc3-smplr load vn-tu_oV_nA_sus_mp --b0=000
hsc3-smplr run vn-tu_oV_nA_sus_mp --b0=000

hsc3-smplr load vn-vi_mV_sus_mf --b0=0
hsc3-smplr run vn-vi_mV_sus_mf --rT=0.35

hsc3-smplr load vn-va_mV_sus_mf --b0=0
hsc3-smplr run vn-va_mV_sus_mf --rT=0.35

hsc3-smplr load vn-vc_mV_sus_mf --b0=0
hsc3-smplr run vn-vc_mV_sus_mf --rT=0.35
~~~~

`Philharmonia`

~~~~
hsc3-smplr load ph-bcl --b0=000
hsc3-smplr run ph-bcl --b0=000 --rT=0.35
~~~~
