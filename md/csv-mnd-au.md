# csv-mnd-au

~~~~
hsc3-csv-mnd-au -h
~~~~

~~~~
fn=/home/rohan/sw/hsc3-auditor/data/csv/pf-88.csv

fn=/home/rohan/sw/hmt/data/csv/mnd/1080-C01.csv
hsc3-csv-mnd-au meta rt fcd --vc=dolce-8 $fn --ld=True

sfz_fn=/home/rohan/data/audio/instr/casacota/zell_1737_415_MeanTone5/8_i.sfz
sfz_fn=/home/rohan/data/audio/instr/celeste/long.sfz
sfz_fn=/home/rohan/rd/j/2019-04-21/FAIRLIGHT/IIX/VOICES/sararr1.sfz
hsc3-csv-mnd-au sfz rt $sfz_fn $fn --ld=True --gn=0.25

hsc3-csv-mnd-au meta rt pf --dyn=d008 $fn --ld=True
hsc3-csv-mnd-au meta rt ca-zell --vc=8_i $fn --ld=True --gn=0.5
hsc3-csv-mnd-au meta rt cel --vc=long $fn --ld=True --gn=0.25
hsc3-csv-mnd-au meta rt sm-si --vc=II $fn --ld=True
hsc3-csv-mnd-au meta rt sm-os --vc=B $fn --ld=True
hsc3-csv-mnd-au meta rt ca-esmuc --vc=om_flautat $fn --ld=True --gn=0.5
hsc3-csv-mnd-au meta rt ca-hauslaib --vc=flauteadillos $fn --ld=True
hsc3-csv-mnd-au meta rt vn-fl2_pV_nA_sus_pp $fn --ld=True
hsc3-csv-mnd-au meta rt vn-ha_es_mf $fn --ld=True --gn=0.25

tbl=/home/rohan/uc/invisible/places/csv/lmy.csv
fn=/home/rohan/uc/invisible/places/csv/A.2.csv
hsc3-csv-mnd-au meta rt ml --vc=MkIIFlute --ld=True --tbl=$tbl $fn
~~~~

~~~~
$ hsc3-csv-mnd-au fcd rt --ld=True --gn=0.5 --nq=nil --stp=-1 ~/sw/hsc3-auditor/data/csv/fcd-29.csv
$ hsc3-csv-mnd-au fcd rt --ld=False --gn=0.5 --nq=nil --stp=-1 ~/sw/hsc3-auditor/data/csv/fcd-rg.csv
$ hsc3-csv-mnd-au fcd nrt --nc=2 --gn=0.5 --stp=-1 ~/sw/hsc3-auditor/data/csv/fcd-29.csv /tmp/t.wav
~~~~
