# auditor

There are three tuning rules:

- `d12` loads scala files for tunings of degree 12, with a detune
  parameter in cents and a note offset parameter

- `cps` loads arbitrary scala files, with parameters to indicate the
  midi note to begin the scale at at the frequency to assign to that
  note, notes below the indicated midi note do not sound

- `tbl` loads a CSV file of (midi-note,frequency) pairs, the table may
  be sparse, a comma-separated list of csv files may be given

A `syn` argument of `_` indicates the standard synthdef for `ins`.

Listens for incoming OSC messages at port 57149:

~~~~
echo '["/p_set","rdetune",0]' | hosc-json-cat -h 127.0.0.1 -p 57149
echo '["/amp_stop",0,0.25]' | hosc-json-cat -h 127.0.0.1 -p 57149
~~~~

`FCD`

~~~~
hsc3-auditor load-all fcd-zc 0 0,1,2,3,4,5,6,7,8,9,10,11
hsc3-auditor rt midi-osc fcd d12 young-lm_piano -74.7 -3 --aT=0.05 --rT=0.05 --pwD=0.15
hsc3-auditor load-all fcd 0 0
hsc3-auditor rt midi-osc fcd d12 meanquar 0 0 --aT=0.05 --rT=0.05 --pwD=0.15
~~~~
