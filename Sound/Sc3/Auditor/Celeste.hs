{- | Celeste

Recordings of ASTRA celeste made by Anthony Pateras

There are two sets, short and long.
-}
module Sound.Sc3.Auditor.Celeste where

import System.FilePath {- filepath -}
import Text.Printf {- base -}

import qualified Music.Theory.Pitch as Pitch {- hmt -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Auditor.Common as Common

-- | dir = directory
cel_dir :: FilePath
cel_dir = "/home/rohan/data/audio/instr/celeste/"

cel_file :: FilePath -> FilePath
cel_file = (++) cel_dir

-- | nc = number-of-channels
cel_nc :: Int
cel_nc = 1

cel_ch_mode :: Common.Ch_Mode
cel_ch_mode = Common.nc_to_ch_mode cel_nc

cel_vc :: [Common.Voice_Name]
cel_vc = ["short", "long"]

-- | Stored as @wav@.
cel_format :: String
cel_format = "wav"

-- | C3-C8
cel_range :: (Key, Key)
cel_range = Common.range_octpc_to_midi ((3, 0), (8, 0))

cel_akt_f :: Common.Akt_f
cel_akt_f = Common.akt_f_rng cel_range

cel_degree :: Int
cel_degree = Common.range_to_degree cel_range

cel_gamut :: [Key]
cel_gamut = Common.range_to_gamut cel_range

cel_sample_rate :: Num n => n
cel_sample_rate = 96000

midi_note_name :: Key -> String
midi_note_name = Pitch.pitch_pp_iso . Pitch.midi_to_pitch (Pitch.pc_spell_sharp :: Pitch.Spelling Key)

-- | sf = sound-file
cel_sf_name_rel :: Maybe FilePath -> Common.Voice_Name -> (Key, Key) -> FilePath
cel_sf_name_rel sub_dir vc (n, m) =
  case sub_dir of
    Nothing -> printf "%s/%02d-%s-%s.wav" vc n (midi_note_name m) vc
    Just dir -> printf "%s/%s/%02d-%s-%s.wav" vc dir n (midi_note_name m) vc

cel_sf_names_rel :: Maybe FilePath -> Common.Voice_Name -> [FilePath]
cel_sf_names_rel dir vc = map (cel_sf_name_rel dir vc) (zip [1 .. cel_degree] cel_gamut)

{- | Absolute file names, in sequence, of voice.

> putStrLn $ unlines $ concat $ map (cel_sf_names Nothing) cel_vc
-}
cel_sf_names :: Maybe FilePath -> Common.Voice_Name -> [FilePath]
cel_sf_names dir = map cel_file . cel_sf_names_rel dir

cel_load_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
cel_load_msg b0 vc = Common.au_loader [0] (cel_sf_names Nothing vc) b0

cel_load :: Buffer_Id -> Common.Voice_Name -> IO ()
cel_load b0 = withSc3 . mapM_ async . cel_load_msg b0

cel_sfz :: Common.Voice_Name -> [Sfz_Section]
cel_sfz vc =
  Common.sfz_grp_rT 0.05
    : Common.sfz_region_1_seq (cel_sf_names_rel Nothing vc) cel_gamut Nothing

-- > mapM_ cel_sfz_wr cel_vc
cel_sfz_wr :: Common.Voice_Name -> IO ()
cel_sfz_wr vc = sfz_write_sections False (cel_dir </> vc <.> "sfz") (cel_sfz vc)
