-- | gliss = glissandi
module Sound.Sc3.Auditor.Smplr.Gliss where

import Data.Maybe {- base -}

import Music.Theory.Geometry.Vector {- hmt-base -}

import qualified Music.Theory.Tuning as Tuning {- hmt -}

import qualified Sound.Osc.Core as Osc {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Midi.Type as Midi {- midi-osc -}

import qualified Sound.Sc3.Auditor.Common as Common {- hsc3-auditor -}

{- | Trivial file playback instrument, with glissandi.
     Amplitude glissandi (from amp0 to amp1) is a multiplier for the LINEN envelope.
     Post-processor is before panning.

> import Sound.Sc3.Ugen.Dot
> draw smplr_gliss_ugen

duration is of gliss,
sustain is of overall (ie. including post-gliss stasis)
-}
smplr_gliss_ugen :: Int -> (Ugen -> Ugen) -> Ugen
smplr_gliss_ugen nc post_proc =
  let b = control kr "bufnum" 0
      dur = control kr "duration" 1
      sus = control kr "sustain" 1
      amp = Common.k_pair_lin dur "amp" 0.1 -- amp0,amp1
      rt = Common.k_pair_exp dur "rate" 1 -- rate0,rate1
      loc = Common.k_pair_lin dur "pan" 0 -- pan0,pan1
      atk = control kr "attack" 0
      dcy = control kr "decay" 0.5
      sp = control kr "startpos" 0 -- frames
      loop = control kr "loop" (from_loop NoLoop)
      r' = bufRateScale kr b * rt
      pb = playBuf nc ar b r' 1 sp (WithLoop loop) DoNothing
      e = envGen kr 1 1 0 1 RemoveSynth (envLinen atk (sus - atk - dcy) dcy 1)
      s = post_proc (pb * e)
  in case nc of
      1 -> pan2 s loc amp
      2 -> s * amp
      _ -> error "smplr_gliss_ugen"

{- | Add bus control and out Ugen, name is @smplr-gliss@.

> withSc3 (async (d_recv (smplr_gliss id)))
-}
smplr_gliss :: Int -> (Ugen -> Ugen) -> Synthdef
smplr_gliss nc post_proc =
  let bus = control kr "bus" 0
  in synthdef "smplr-gliss" (out bus (smplr_gliss_ugen nc post_proc))

{- | Sampler options (Akt_f,nid,grp,prm)

nid = node id,
grp = group to allocate node at,
prm = synthesis parameters

prm: aT = attack time, rT = release time, bus = output bus, loop : bool
-}
type Smplr_Gliss_Opt = (Common.Akt_f, Node_Id, Group_Id, Param)

-- | (Channel,V2 (Key,Detune),Velocity)
type Smplr_Gliss_Nt = (Midi.Channel, V2 (Midi.Key, Double), Midi.Velocity)

{- | Make @smplr-gliss@ type control 'Message'.

du = duration,
su = sustain
g = gain
-}
smplr_gliss_msg_typ :: String -> Smplr_Gliss_Opt -> Smplr_Gliss_Nt -> V2 Double -> V2 Double -> Osc.Message
smplr_gliss_msg_typ nm (akt_f, nid, grp, usr_prm) nt (du, su) (g0, g1) =
  let (b, r0, r1) = smplr_gliss_calc akt_f nt
      prm =
        [ ("duration", realToFrac du)
        , ("sustain", realToFrac su)
        , ("bufnum", fromIntegral b)
        , ("rate0", r0)
        , ("rate1", r1)
        , ("amp0", g0)
        , ("amp1", g1)
        , ("startpos", 0)
        ]
  in s_new nm nid AddToHead grp (param_merge_r usr_prm prm)

smplr_gliss_msg :: Smplr_Gliss_Opt -> Smplr_Gliss_Nt -> V2 Double -> V2 Double -> Osc.Message
smplr_gliss_msg = smplr_gliss_msg_typ "smplr-gliss"

{- | Calculate buffer number, and start and end rates.

mnn = midi note number,
dt = detune (cents),
rt = rate

> smplr_gliss_calc (Common.akt_f_simple (zip [0..127] [0..127])) (0,((60,0),(71,100)),127) == (60,1,2)
-}
smplr_gliss_calc :: Common.Akt_f -> Smplr_Gliss_Nt -> (Buffer_Id, Double, Double)
smplr_gliss_calc akt_f (ch, ((mnn0, dt0), (mnn1, dt1)), vel) =
  let (b, dt) = fromJust (akt_f (ch, mnn0, vel))
      rt0 = Tuning.cents_to_fratio (dt + dt0)
      rt1 = Tuning.cents_to_fratio (dt + (fromIntegral mnn1 - fromIntegral mnn0) * 100 + dt1)
  in (b, rt0, rt1)
