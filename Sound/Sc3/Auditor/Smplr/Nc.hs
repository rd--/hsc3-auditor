-- | Smplr-Nc
module Sound.Sc3.Auditor.Smplr.Nc where

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

smplr_nc_msg :: Int -> Message
smplr_nc_msg nc = d_recv (synthdef "smplr" (smplr_nc nc))

-- > smplr_nc_load 2
smplr_nc_load :: Int -> IO ()
smplr_nc_load = withSc3 . async_ . smplr_nc_msg

{- | Sample playback instrument.  The synth frees itself at the end of
the buffer or after the indicated duration (dur), whichever is
sooner.  The default duration is very long. The release time
(fadeTime) can be set.
-}
smplr_nc :: Int -> Ugen
smplr_nc nc =
  let b = control kr "bufnum" 0 -- buffer
      r = control kr "rate" 1 -- rate
      a = control kr "amp" 1 -- amplitude
      o = control kr "bus" 0 -- output channel
      d = control kr "dur" 1e4 -- duration (very long by default)
      ft = control kr "fadeTime" 1
      b_r = bufRateScale kr b * r
      c = envCoord [(0, 1), (d, 1), (d + ft, 0)] 1 1 EnvLin
      e = envGen kr 1 a 0 1 RemoveSynth c
  in out o (playBuf nc ar b b_r 1 0 NoLoop RemoveSynth * e)
