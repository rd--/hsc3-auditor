{- |  Casa da Música - Javanese Gamelan
   <https://github.com/Digitopia/CDM-GAMELAN-SAMPLE-LIBRARY>
-}
module Sound.Sc3.Auditor.Cdm where

import Data.Char {- base -}
import Data.List {- base -}

import System.FilePath {- filepath -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import qualified Music.Theory.List as T {- hmt -}
import qualified Music.Theory.Pitch as T {- hmt -}
import qualified Music.Theory.Tuple as T {- hmt -}

import qualified Music.Theory.Gamelan as G {- hmt -}

import qualified Sound.Sc3.Auditor.Common as Common

int_to_integer :: Int -> Integer
int_to_integer = fromIntegral

-- | dir = directory
cdm_dir :: FilePath
cdm_dir = "/home/rohan/data/audio/instr/CDM-GAMELAN/"

cdm_file :: FilePath -> FilePath
cdm_file = (</>) cdm_dir

-- | <base-name><key>.wav
cdm_resolve :: String -> String -> FilePath
cdm_resolve x k = (x ++ k) <.> "wav"

-- | nc = number-of-channels
cdm_nc :: Int
cdm_nc = 2

cdm_scale_tbl :: [(G.Scale, String)]
cdm_scale_tbl =
  [ (G.Pelog, "PL")
  , (G.Slendro, "SL")
  ]

cdm_scale_str :: G.Scale -> String
cdm_scale_str = flip T.lookup_err cdm_scale_tbl

type CDM_PITCH = (G.Octave, G.Degree)

cdm_pitch :: CDM_PITCH -> G.Pitch
cdm_pitch (o, d) = G.Pitch o d

type CDM_RNG = (G.Scale, CDM_PITCH, CDM_PITCH)

cdm_rng_scl :: CDM_RNG -> G.Scale
cdm_rng_scl (scl, _, _) = scl

cdm_rng_elem :: CDM_RNG -> [G.Note]
cdm_rng_elem (scl, p1, p2) = G.note_range_elem scl (cdm_pitch p1) (cdm_pitch p2)

data CDM_ANNOT = CDM_PLAIN | CDM_SUSTAIN | CDM_LOUD | CDM_SOFT

cdm_annot_char :: CDM_ANNOT -> Maybe Char
cdm_annot_char a =
  case a of
    CDM_PLAIN -> Nothing
    CDM_SUSTAIN -> Just 's'
    CDM_LOUD -> Just 'f'
    CDM_SOFT -> Just 'p'

cdm_annot_code :: CDM_ANNOT -> String
cdm_annot_code = maybe "" return . cdm_annot_char

type CDM_INSTR = (G.Instrument_Name, String, [CDM_RNG], [CDM_ANNOT])

cdm_instr_tone_seq :: G.Scale -> CDM_INSTR -> [G.Tone CDM_ANNOT]
cdm_instr_tone_seq scl (nm, _z, rng, an_set) =
  let nt_seq = concatMap cdm_rng_elem (filter ((==) scl . cdm_rng_scl) rng)
  in [G.Tone nm (Just nt) Nothing (Just an) | nt <- nt_seq, an <- an_set]

tone_to_code :: G.Tone CDM_ANNOT -> String
tone_to_code tn =
  case tn of
    G.Tone nm (Just nt) _ (Just an) -> cdm_instr_str nm ++ cdm_note_code nt ++ cdm_annot_code an
    _ -> error "tone_to_code?"

cdm_instr_nm :: G.Scale -> CDM_INSTR -> [String]
cdm_instr_nm scl = map ((<.> "wav") . tone_to_code) . cdm_instr_tone_seq scl

cdm_instr_fn :: G.Scale -> CDM_INSTR -> [FilePath]
cdm_instr_fn scl = map cdm_file . cdm_instr_nm scl

cdm_instr_codes :: [String]
cdm_instr_codes = words "BONANG DRUMS GAMBANG GENDER GONG KEMPYANG KENONG KETHUK SARON"

-- > cdm_instr_nm G.Pelog (cdm_instr_tbl !! 5)
cdm_instr_tbl :: [CDM_INSTR]
cdm_instr_tbl =
  [ (G.Bonang_Barung, "BONANG/BB", [(G.Pelog, (0, 1), (1, 7)), (G.Slendro, (0, 1), (2, 2))], [CDM_PLAIN])
  , (G.Bonang_Panerus, "BONANG/BP", [(G.Pelog, (0, 1), (1, 7)), (G.Slendro, (0, 1), (2, 2))], [CDM_PLAIN])
  , (G.Gambang_Kayu, "GAMBANG/G", [(G.Pelog, (-2, 6), (2, 5)), (G.Slendro, (-1, 1), (2, 5))], [CDM_PLAIN])
  , (G.Gender_Barung, "GENDER/GB", [(G.Pelog, (-1, 6), (2, 3)), (G.Slendro, (-1, 6), (2, 3))], [CDM_PLAIN])
  , (G.Gender_Panerus, "GENDER/GP", [(G.Pelog, (-1, 6), (2, 3)), (G.Slendro, (-1, 6), (2, 3))], [CDM_PLAIN])
  , (G.Gender_Panembung, "GENDER/GS", [(G.Pelog, (0, 1), (0, 7)), (G.Slendro, (-1, 6), (1, 1))], [CDM_PLAIN])
  , (G.Gong_Ageng, "GONG/GA", [], [CDM_SOFT, CDM_LOUD])
  , (G.Gong_Suwukan, "GONG/GS", [], [CDM_SOFT, CDM_LOUD])
  , (G.Kempul, "GONG/GK", [(G.Pelog, (0, 1), (0, 7)), (G.Slendro, (-1, 1), (0, 6))], [CDM_SOFT, CDM_LOUD])
  , (G.Kempyang, "KEMPYANG/Kp", [(G.Pelog, (0, 6), (0, 6)), (G.Slendro, (0, 1), (0, 1))], [CDM_PLAIN, CDM_SUSTAIN])
  , (G.Kenong, "KENONG/K", [(G.Pelog, (0, 2), (1, 1)), (G.Slendro, (0, 2), (1, 2))], [CDM_PLAIN, CDM_SUSTAIN])
  , (G.Ketuk, "KETHUK/Kt", [(G.Pelog, (0, 6), (0, 6)), (G.Slendro, (0, 2), (0, 2))], [CDM_PLAIN, CDM_SUSTAIN])
  , (G.Saron_Barung, "SARON/SB", [(G.Pelog, (0, 1), (0, 7)), (G.Slendro, (-1, 6), (1, 3))], [CDM_PLAIN])
  , (G.Saron_Demung, "SARON/SD", [(G.Pelog, (0, 1), (0, 7)), (G.Slendro, (-1, 6), (1, 1))], [CDM_PLAIN])
  , (G.Saron_Panerus, "SARON/SP", [(G.Pelog, (0, 1), (0, 7)), (G.Slendro, (-1, 6), (1, 1))], [CDM_PLAIN])
  ]

cdm_drums :: [(String, [String])]
cdm_drums =
  [ ("CIBLON_TAMBOR_MEDIO", words "DLANG LUNG TAK THUNG TONG")
  , ("KENDHANG AGENG", words "DHA")
  , ("KETIPUNG_TAMBOR PEQUENO", words "DHUNG TAK")
  ]

cdm_instr_str :: G.Instrument_Name -> String
cdm_instr_str nm = T.p4_snd (T.find_err ((== nm) . T.p4_fst) cdm_instr_tbl)

cdm_instr_rng :: G.Instrument_Name -> [CDM_RNG]
cdm_instr_rng nm = T.p4_third (T.find_err ((== nm) . T.p4_fst) cdm_instr_tbl)

cdm_parse_oct :: String -> G.Octave
cdm_parse_oct str =
  if all (== 'h') str
    then genericLength str
    else
      if all (== 'l') str
        then negate (genericLength str)
        else error "cdm_parse_oct?"

cdm_parse_pitch :: String -> G.Pitch
cdm_parse_pitch str =
  case str of
    dgr : oct -> G.Pitch (cdm_parse_oct oct) (int_to_integer (digitToInt dgr))
    _ -> error "cdm_parse_pitch?"

-- > map cdm_parse_note (words "PL1ll SL2l PL3 SL5h PL6hh")
cdm_parse_note :: String -> G.Note
cdm_parse_note str =
  case str of
    'P' : 'L' : p -> G.Note G.Pelog (cdm_parse_pitch p)
    'S' : 'L' : p -> G.Note G.Slendro (cdm_parse_pitch p)
    _ -> error "cdm_parse_note?"

-- > map (cdm_note_code . cdm_parse_note) (words "PL1ll SL2l PL3 SL5h PL6hh")
cdm_note_code :: G.Note -> String
cdm_note_code (G.Note scl (G.Pitch oct dgr)) =
  let d = show dgr
      o = if oct < 0 then genericReplicate (abs oct) 'l' else genericReplicate oct 'h'
  in case scl of
      G.Pelog -> "PL" ++ d ++ o
      G.Slendro -> "SL" ++ d ++ o

-- | Frequencies, measured.
cdm_freq :: [(String, [(String, Double)])]
cdm_freq =
  [
    ( "GENDER/GSSL"
    , [("1h", 260.8), ("1", 129.8), ("2", 150.5), ("3", 171.4), ("5", 198.4), ("6l", 112.8), ("6", 225.5)]
    )
  ,
    ( "GENDER/GPSL"
    ,
      [ ("1hh", 1042.9)
      , ("1h", 522.8)
      , ("1", 260.5)
      , ("2hh", 1209.1)
      , ("2h", 603.4)
      , ("2", 301.2)
      , ("3hh", 1381.0)
      , ("3h", 687.5)
      , ("3", 343.9)
      , ("5h", 795.6)
      , ("5", 397.5)
      , ("6h", 905.0)
      , ("6", 452.4)
      , ("6l", 225.7)
      ]
    )
  ,
    ( "GENDER/GSPL"
    , [("1", 141.0), ("2", 152.8), ("3", 168.0), ("4", 197.2), ("5", 209.8), ("6", 225.2), ("7", 245.8)]
    )
  ,
    ( "GENDER/GPPL"
    ,
      [ ("1hh", 1191.0)
      , ("1h", 595.5)
      , ("1", 290.6)
      , ("2hh", 1300.3)
      , ("2h", 648.3)
      , ("2", 318.8)
      , ("3hh", 1411.8)
      , ("3h", 699.2)
      , ("3", 355.3)
      , ("5h", 864.6)
      , ("5", 428)
      , ("6h", 930.8)
      , ("6", 471.4)
      , ("6l", 230.4)
      , ("7h", 1009.0)
      , ("7", 511.3)
      , ("7l", 249.0)
      ]
    )
  ]

cdm_akt_tbl :: String -> Common.Akt_Tbl
cdm_akt_tbl nm =
  case lookup nm cdm_freq of
    Just dat ->
      let (mnn, dt) = unzip (sort (map (T.cps_to_midi_detune . snd) dat))
      in zip mnn (zip [0 ..] dt)
    Nothing -> error "cdm_akt_tbl?"

-- * GSSL

cdm_gssl_range :: (Key, Key)
cdm_gssl_range = (45, 60) -- ?

cdm_gssl_akt_tbl :: Common.Akt_Tbl
cdm_gssl_akt_tbl = cdm_akt_tbl "GENDER/GSSL"

-- > map cdm_gssl_akt_et12_f [44 .. 60]
cdm_gssl_akt_et12_f :: Common.Akt_f
cdm_gssl_akt_et12_f = Common.akt_f_cdiff cdm_gssl_akt_tbl

cdm_gssl_nm :: [String]
cdm_gssl_nm = map (cdm_resolve "GENDER/GSSL") (words "6l 1 2 3 5 6 1h")

cdm_gssl_files :: [FilePath]
cdm_gssl_files = map cdm_file cdm_gssl_nm

cdm_gssl_load_msg :: Buffer_Id -> [Message]
cdm_gssl_load_msg = Common.au_loader [0, 1] cdm_gssl_files

cdm_gssl_load :: Buffer_Id -> IO ()
cdm_gssl_load = withSc3 . mapM_ async . cdm_gssl_load_msg

cdm_gssl_sfz :: [Sfz_Section]
cdm_gssl_sfz =
  let f (mnn, (_, dt)) = (mnn, negate (truncate dt))
  in Common.sfz_grp_rT 0.1
      : Common.sfz_region_prt_tun_seq cdm_gssl_nm (map f cdm_gssl_akt_tbl)

cdm_gssl_sfz_wr :: IO ()
cdm_gssl_sfz_wr = sfz_write_sections False (cdm_dir </> "gssl" <.> "sfz") cdm_gssl_sfz

-- * GSPL

cdm_gspl_range :: (Key, Key)
cdm_gspl_range = (49, 59) -- ?

cdm_gspl_akt_tbl :: Common.Akt_Tbl
cdm_gspl_akt_tbl = cdm_akt_tbl "GENDER/GSPL"

-- > map cdm_gspl_akt_et12_f [48 .. 60]
cdm_gspl_akt_et12_f :: Common.Akt_f
cdm_gspl_akt_et12_f = Common.akt_f_cdiff cdm_gspl_akt_tbl

cdm_gspl_nm :: [String]
cdm_gspl_nm = map (cdm_resolve "GENDER/GSPL" . show) [1 :: Int .. 7]

cdm_gspl_files :: [FilePath]
cdm_gspl_files = map cdm_file cdm_gspl_nm

cdm_gspl_load_msg :: Buffer_Id -> [Message]
cdm_gspl_load_msg = Common.au_loader [0, 1] cdm_gspl_files

cdm_gspl_load :: Buffer_Id -> IO ()
cdm_gspl_load = withSc3 . mapM_ async . cdm_gspl_load_msg

-- * GPSL

cdm_gpsl_range :: (Key, Key)
cdm_gpsl_range = (57, 89) -- ?

cdm_gpsl_akt_tbl :: Common.Akt_Tbl
cdm_gpsl_akt_tbl = cdm_akt_tbl "GENDER/GPSL"

cdm_gpsl_akt_et12_f :: Common.Akt_f
cdm_gpsl_akt_et12_f = Common.akt_f_cdiff cdm_gpsl_akt_tbl

cdm_gpsl_nm :: [String]
cdm_gpsl_nm = map (cdm_resolve "GENDER/GPSL") (words "6l 1 2 3 5 6 1h 2h 3h 5h 6h 1hh 2hh 3hh")

cdm_gpsl_files :: [FilePath]
cdm_gpsl_files = map cdm_file cdm_gpsl_nm

cdm_gpsl_load_msg :: Buffer_Id -> [Message]
cdm_gpsl_load_msg = Common.au_loader [0, 1] cdm_gpsl_files

cdm_gpsl_load :: Buffer_Id -> IO ()
cdm_gpsl_load = withSc3 . mapM_ async . cdm_gpsl_load_msg
