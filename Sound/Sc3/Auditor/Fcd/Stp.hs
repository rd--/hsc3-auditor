-- \* historical - Fcd/Stops
module Sound.Sc3.Auditor.Fcd.Stp where

import qualified Music.Theory.List as List {- hmt-base -}
import qualified Music.Theory.Monad as Monad {- hmt-base -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import Sound.Midi.Type {- midi-osc -}

import qualified Sound.Sc3.Auditor.Common as Common {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Fcd as Fcd {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Server.Smplr as Server {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Smplr as Smplr {- hsc3-auditor -}

fcd_stop_to_vc :: Server.Stop_Id -> Common.Voice_Name
fcd_stop_to_vc stp = List.lookup_err stp fcd_stops

-- | With zero-based indices.
fcd_stops :: [(Server.Stop_Id, Common.Voice_Name)]
fcd_stops = zip [0 ..] Fcd.fcd_vc

fcd_stops_ix :: [Server.Stop_Id]
fcd_stops_ix = map fst fcd_stops

fcd_range_stp :: Server.Stop_Id -> (Key, Key)
fcd_range_stp = Fcd.fcd_range . fcd_stop_to_vc

fcd_degree_stp :: Server.Stop_Id -> Int
fcd_degree_stp = Fcd.fcd_degree . fcd_stop_to_vc

fcd_gamut_stp :: Server.Stop_Id -> [Key]
fcd_gamut_stp = Common.range_to_gamut . fcd_range_stp

u8_to_f64 :: Key -> Double
u8_to_f64 = fromIntegral

fcd_load_zc_msg :: Server.Stop_Id -> Buffer_Id -> [Message]
fcd_load_zc_msg stp b0 = Fcd.fcd_zc_load_msg b0 (fcd_stop_to_vc stp)

fcd_load_or_send_zc_msg :: Server.Stop_Id -> Buffer_Id -> IO [Message]
fcd_load_or_send_zc_msg stp b0 = Fcd.fcd_zc_load_or_send_msg b0 (fcd_stop_to_vc stp)

fcd_load_zc_seq_msg :: [Server.Stop_Id] -> Buffer_Id -> [Message]
fcd_load_zc_seq_msg stp b0 = concatMap (\k -> fcd_load_zc_msg k (fcd_b0_of_stop b0 k)) stp

fcd_load_or_send_zc_seq_msg :: [Server.Stop_Id] -> Buffer_Id -> IO [Message]
fcd_load_or_send_zc_seq_msg stp b0 = Monad.concatMapM (\k -> fcd_load_or_send_zc_msg k (fcd_b0_of_stop b0 k)) stp

{- | Fcb b0 indices

>>> fcd_b0_indices 0
[0,29,58,87,116,145,174,203,232,281,330,367,404,428,452]
-}
fcd_b0_indices :: Buffer_Id -> [Buffer_Id]
fcd_b0_indices b0 = List.dx_d b0 (map fcd_degree_stp fcd_stops_ix)

fcd_b0_of_stop :: Buffer_Id -> Server.Stop_Id -> Buffer_Id
fcd_b0_of_stop b0 stp = fcd_b0_indices b0 !! stp

-- | Note: This trims start and end... extract could do this?
fcd_load_msg :: Buffer_Id -> Server.Stop_Id -> [Message]
fcd_load_msg b0 stp =
  let f = round . (*) (Fcd.fcd_sample_rate :: Double)
      lp = Just (f 0.25, f ((20 / 3) - 0.25))
  in Common.au_loader_lp [0] (Fcd.fcd_sf_names Nothing (fcd_stop_to_vc stp)) b0 (repeat lp)

fcd_load_or_send_msg :: Buffer_Id -> Server.Stop_Id -> IO [Message]
fcd_load_or_send_msg b0 stp =
  let f = round . (*) (Fcd.fcd_sample_rate :: Double)
      lp = Just (f 0.25, f ((20 / 3) - 0.25))
  in Common.au_loader_or_sender_lp [0] (Fcd.fcd_sf_names Nothing (fcd_stop_to_vc stp)) b0 (repeat lp)

fcd_load_seq_msg :: Buffer_Id -> [Server.Stop_Id] -> [[Message]]
fcd_load_seq_msg b0 = map (\k -> fcd_load_msg (fcd_b0_of_stop b0 k) k)

fcd_load_or_send_seq_msg :: Buffer_Id -> [Server.Stop_Id] -> IO [[Message]]
fcd_load_or_send_seq_msg b0 = mapM (\k -> fcd_load_or_send_msg (fcd_b0_of_stop b0 k) k)

{- | Fcd load all message

>>> length (fcd_load_all_msg 0 [0..7])
232

>>> sum (map fcd_degree_stp [0..7])
232
-}
fcd_load_all_msg :: Buffer_Id -> [Server.Stop_Id] -> [Message]
fcd_load_all_msg b0 stp = concat (fcd_load_seq_msg b0 stp)

fcd_load_or_send_all_msg :: Buffer_Id -> [Server.Stop_Id] -> IO [Message]
fcd_load_or_send_all_msg b0 stp = fmap concat (fcd_load_or_send_seq_msg b0 stp)

-- > fcd_load_all 0 [0]
fcd_load_all :: Buffer_Id -> [Server.Stop_Id] -> IO ()
fcd_load_all b0 k = do
  let m = fcd_load_all_msg b0 k
  Common.print_stderr ("fcd_load_all", length m)
  withSc3 (mapM_ async m)

fcd_load_or_send_all :: Buffer_Id -> [Server.Stop_Id] -> IO ()
fcd_load_or_send_all b0 k = do
  m <- fcd_load_or_send_all_msg b0 k
  Common.print_stderr ("fcd_load_or_send_all", length m)
  withSc3 (mapM_ async m)

fcd_load_all_zc :: Buffer_Id -> [Server.Stop_Id] -> IO ()
fcd_load_all_zc b0 k = do
  let m = fcd_load_zc_seq_msg k b0
  Common.print_stderr ("fcd_load_all_zc", length m)
  withSc3 (mapM_ async m)

fcd_load_or_send_all_zc :: Buffer_Id -> [Server.Stop_Id] -> IO ()
fcd_load_or_send_all_zc b0 k = do
  m <- fcd_load_or_send_zc_seq_msg k b0
  Common.print_stderr ("fcd_load_or_send_all_zc", length m)
  withSc3 (mapM_ async m)

{- | Level adjustments (linear multiplier) for stops, flute8 is quiet etc.

>>> map ((`sc3_round_to` 0.01) . fcd_level_adjust) [0..13]
[2.0,2.0,1.41,1.41,0.71,0.71,1.41,1.41,0.71,0.71,0.71,0.71,0.71,0.71]
-}
fcd_level_adjust :: Server.Stop_Id -> Double
fcd_level_adjust stp = db_to_amp (Fcd.fcd_lvl (fcd_stop_to_vc stp))

-- * Smplr/Server

fcd_akt_f_stp :: Server.Stop_Id -> Common.Akt_f
fcd_akt_f_stp = Fcd.fcd_akt_f . fcd_stop_to_vc

{- | Partially applied 'Smplr.smplr_calc'.
b0 is the index of buffer zero for stop zero.
-}
fcd_calc :: (Buffer_Id, Server.Stop_Id) -> ((Channel, Key, Velocity), Double) -> (Buffer_Id, Double)
fcd_calc (b0, stp) = Server.smplr_calc (fcd_range_stp stp) (fcd_akt_f_stp stp) (fcd_b0_of_stop b0 stp)

fcd_calc_plain :: (Buffer_Id, Server.Stop_Id) -> (Channel, Key, Velocity) -> Maybe (Buffer_Id, Double)
fcd_calc_plain opt nt = Just (fcd_calc opt (nt, 0))

-- stp = stop, ch = channel assignment mode, nid = node id, b0
-- = buffer zero, m = midi note number, dt = detune (cents), du =
-- duration, g = gain, bus = output bus, grp = group to allocate node
-- at, p2 = further synthesis parameters
--
-- > let opt = (fcd_range_stp,Smplr.CH_PAN_1,-1,0,(0.05,0.15),0,1,[("pan",0.5)],Nothing)
-- > withSc3 (send (fcd_smplr 0 opt (60,0) (Just 2) 1))
--
-- > withSc3 (send (fcd_smplr 0 opt (30,0) Nothing 1))
-- > withSc3 (send (n_set1 (-1) "gate" 0))
fcd_smplr :: Server.Stop_Id -> Server.Smplr_Fn
fcd_smplr stp opt mdt du (amp, amp_mn, amp_stp) =
  let (syn, rng, akt, ch, en, nid, b0, env, bus, grp, p2, lp) = opt
      stp' = stp `mod` length fcd_stops -- ...
      b0' = fcd_b0_of_stop b0 stp'
      vol' = (amp * fcd_level_adjust stp', amp_mn, amp_stp)
  in Server.smplr_msg (syn, rng, akt, ch, en, nid, b0', env, bus, grp, p2, lp) mdt du vol'

-- > fcd_grp_struct 1 10 4 == [(10,1),(11,10),(12,10),(13,10),(14,10)]
fcd_grp_struct :: Group_Id -> Group_Id -> Int -> [(Group_Id, Group_Id)]
fcd_grp_struct p_grp au_grp k =
  let f stp = (au_grp + stp, au_grp)
  in (au_grp, p_grp) : map f [1 .. k]

{- | Group structure is: au_grp is the auditor group and is placed in the
/existing/ group p_grp.  au_grp then has one child group for each
stop with IDs starting at au_grp+1.

> fcd_grp_init (1,10) 8
-}
fcd_grp_init :: (Int, Int) -> Int -> Message
fcd_grp_init (p_grp, au_grp) k =
  let f (p, q) = (p, AddToHead, q)
  in g_new (map f (fcd_grp_struct p_grp au_grp k))

fcd_init :: Int -> [Int] -> IO [BundleOf Message]
fcd_init b0 k = do
  ld <- fcd_load_or_send_all_msg b0 k
  let f = bundle 0 . return
      sy = Smplr.smplr_recv_all_msg
  return (map f sy ++ map f ld {- ++ [f (g_new [(1,AddToTail,0)])] -})
