-- | Wt = wave-table
module Sound.Sc3.Auditor.Fcd.Wt where

import Control.Monad {- base -}
import Text.Printf {- base -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.File.HSndFile as SF {- hsc3-sf-hsndfile -}

import qualified Sound.Sc3.Auditor.Common as Common
import qualified Sound.Sc3.Auditor.Fcd as Fcd

-- | Derive WT file name.
fcd_wt_gen_fn :: Common.Voice_Name -> Int -> Int -> String
fcd_wt_gen_fn nm nt wt = printf "%s/wt/%s.%02d.%02d.%s" Fcd.fcd_dir nm nt wt Fcd.fcd_format

-- | Plain load function, check file is mono and frame count is 600.
fcd_wt_load :: String -> Int -> Int -> IO [Double]
fcd_wt_load nm nt wt = do
  let fn = fcd_wt_gen_fn nm nt wt
  (hdr, dat) <- SF.read fn
  when (SF.frameCount hdr /= 512 || SF.channelCount hdr /= 1) (error "fcd_wt_load")
  return (dat !! 0)

fcd_wt_load_to_sc3 :: String -> Int -> Int -> Buffer_Id -> IO Message
fcd_wt_load_to_sc3 nm nt wt buf = do
  dat <- fcd_wt_load nm nt wt
  let m = b_alloc_setn1 buf 0 (to_wavetable dat)
  withSc3 (async m)

{-
mapM_ (\(k,nm) -> fcd_wt_load_to_sc3 nm 0 0k) fcd_stops
-}
