{- | Mrk-Mk2
 <http://www.submodern.com/slowburn/?p=736>
-}
module Sound.Sc3.Auditor.Mrk where

import System.FilePath {- filepath -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

-- | dir = directory
mrk_dir :: FilePath
mrk_dir = "/home/rohan/data/audio/instr/mrk-mk2/"

mrk_file :: String -> FilePath
mrk_file = (</>) mrk_dir

-- | nc = number-of-channels
mrk_nc :: Int
mrk_nc = 1

mrk_names_grp :: [(String, Int)]
mrk_names_grp =
  [ ("kick", 1)
  , ("snare", 1)
  , ("hat", 2)
  , ("tom", 1)
  , ("cym", 2)
  , ("bongo", 1)
  , ("block", 1)
  , ("clave", 1)
  ]

mrk_names :: [String]
mrk_names =
  let f (nm, k) = if k == 1 then [nm] else map (\z -> nm ++ show z) [1 .. k]
  in concatMap f mrk_names_grp

mrk_degree :: Int
mrk_degree = length mrk_names

mrk_alloc_read :: Buffer_Id -> [Message]
mrk_alloc_read b0 =
  let f nm i = b_allocRead (b0 + i) (mrk_file ("mae" ++ nm <.> "aiff")) 0 0
  in zipWith f mrk_names [0 ..]

mrk_load :: Buffer_Id -> IO ()
mrk_load = withSc3 . mapM_ async . mrk_alloc_read

{-

import Sound.Sc3.Auditor.Server.Plain
st = st_init_fp ("smplr-pl-pn-1",1,100,())
rt_midi_osc_au (st {st_msg = smplr_msg_f 0})

-}
