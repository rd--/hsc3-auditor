{- | <http://eamusic.dartmouth.edu/~travis/crotales/>
  <travis.garrison@dartmouth.edu>
-}
module Sound.Sc3.Auditor.Crotales where

import System.FilePath {- filepath -}
import Text.Printf {- base -}

import Sound.Osc.Core {- hsc3 -}
import Sound.Sc3 {- hsc3 -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import qualified Sound.Sc3.Auditor.Common as Common

crotale_dir :: FilePath
crotale_dir = "/home/rohan/data/audio/instr/crotales/"

crotale_nc :: Num n => n
crotale_nc = 1

crotale_range :: (Key, Key)
crotale_range = (84, 96)

crotale_akt_f :: Common.Akt_f
crotale_akt_f = Common.akt_f_rng crotale_range

crotale_degree :: Int
crotale_degree = Common.range_to_degree crotale_range

crotale_gamut :: [Key]
crotale_gamut = Common.range_to_gamut crotale_range

crotale_fn :: Int -> String -> String
crotale_fn k s = printf "crotale%02d(%s).wav" k s

crotale_names :: [String]
crotale_names = zipWith crotale_fn [1, 3 .. 25] (words "lowC C# D D# E F F# G G# A A# B highC")

crotale_files :: [FilePath]
crotale_files = let f nm = crotale_dir ++ nm in map f crotale_names

crotale_load_msg :: Buffer_Id -> [Message]
crotale_load_msg = Common.au_loader [0] crotale_files

crotale_load :: Buffer_Id -> IO ()
crotale_load = withSc3 . mapM_ async . crotale_load_msg

crotale_sfz :: [Sfz_Section]
crotale_sfz = Common.sfz_grp_rT 0.1 : Common.sfz_region_1_seq crotale_names crotale_gamut Nothing

crotale_sfz_wr :: IO ()
crotale_sfz_wr = sfz_write_sections False (crotale_dir </> "crotales.sfz") crotale_sfz
