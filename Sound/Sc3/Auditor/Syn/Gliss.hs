module Sound.Sc3.Auditor.Syn.Gliss where

import Music.Theory.Geometry.Vector {- hmt-base -}
import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}
import qualified Sound.Sc3.Ugen.Bindings.Db.External as External {- hsc3 -}

import Sound.Sc3.Auditor.Common {- hsc3-auditor -}

{- | Trivial synthesis instrument, with glissandi.
     Amplitude glissandi (from amp0 to amp1) is a multiplier for the LINEN envelope.

> import Sound.Sc3.Ugen.Dot
> draw (syn_gliss_ugen syn_sin_osc)

duration is of gliss,
sustain is of overall (ie. including post-gliss stasis)
-}
syn_gliss_ugen :: (Ugen -> Ugen) -> (Ugen -> Ugen) -> Ugen
syn_gliss_ugen osc_f post_proc =
  let dur = control kr "duration" 1
      sus = control kr "sustain" 1
      amp = k_pair_lin dur "amp" 0.1 -- amp0,amp1
      freq = k_pair_exp dur "freq" 1 -- freq0,freq1
      loc = k_pair_lin dur "pan" 0 -- pan0,pan1
      atk = control kr "attack" 0
      dcy = control kr "decay" 0.5
      e = envGen kr 1 1 0 1 RemoveSynth (envLinen atk (sus - atk - dcy) dcy 1)
      s = osc_f freq * e
  in pan2 (post_proc s) loc amp

-- > withSc3 (async (d_recv (syn_gliss syn_tri_dpw_osc)))
syn_gliss :: (Ugen -> Ugen) -> (Ugen -> Ugen) -> Synthdef
syn_gliss osc_f post_proc =
  let bus = control kr "bus" 0
  in synthdef "syn-gliss" (out bus (syn_gliss_ugen osc_f post_proc))

syn_sin_osc :: Ugen -> Ugen
syn_sin_osc f = sinOsc ar f 0

syn_tri_osc :: Ugen -> Ugen
syn_tri_osc f = lfTri ar f 0

syn_tri_dpw_osc :: Ugen -> Ugen
syn_tri_dpw_osc = External.dpw3Tri ar

{- | syn options

nid = node id,
(aT,rT) = (attack time,release time)
bus = output bus,
grp = group to allocate node at,
p2 = further synthesis parameters
-}
type SYN_GLISS_OPT = (Node_Id, V2 R, Bus_Id, Group_Id, Param)

{- | Make @syn-gliss@ type control 'Message'.

mnn = midi note number,
dt = detune (cents),
du = duration,
su = sustain,
g0,g1 = gain
-}
syn_gliss_msg_typ :: String -> SYN_GLISS_OPT -> V2 (Int, R) -> V2 R -> V2 R -> Message
syn_gliss_msg_typ nm (nid, (aT, rT), bus, grp, p2) (nt0, nt1) (du, su) (g0, g1) =
  let to_cps (mnn, dt) = midi_to_cps (fromIntegral mnn + (dt / 100))
      du_su =
        [ ("duration", realToFrac du)
        , ("sustain", realToFrac su)
        ]
      p1 =
        [ ("freq0", to_cps nt0)
        , ("freq1", to_cps nt1)
        , ("amp0", g0)
        , ("amp1", g1)
        , ("attack", aT) -- 0.05
        , ("decay", rT) -- 0.15
        , ("bus", fromIntegral bus)
        ]
  in s_new nm nid AddToHead grp (du_su ++ param_merge_r p2 p1)

syn_gliss_msg :: SYN_GLISS_OPT -> V2 (Int, R) -> V2 R -> V2 R -> Message
syn_gliss_msg = syn_gliss_msg_typ "syn-gliss"
