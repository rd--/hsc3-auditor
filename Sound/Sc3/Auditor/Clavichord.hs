-- | cl = clavichord
module Sound.Sc3.Auditor.Clavichord where

import System.FilePath {- filepath -}

import Sound.Osc.Core {- hsc3 -}
import Sound.Sc3 {- hsc3 -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import qualified Sound.Sc3.Auditor.Common as Common

-- | cl = clavichord
cl_dir :: FilePath
cl_dir = "/home/rohan/data/audio/instr/clavichord/"

-- | nc = number-of-channels
cl_nc :: Num n => n
cl_nc = 1

-- | C2 - C6
cl_range :: (Key, Key)
cl_range = (36, 84)

cl_akt_f :: Common.Akt_f
cl_akt_f = Common.akt_f_rng cl_range

cl_degree :: Int
cl_degree = Common.range_to_degree cl_range

cl_gamut :: [Key]
cl_gamut = Common.range_to_gamut cl_range

cl_names :: [String]
cl_names = map ((<.> "wav") . Common.mnn_to_iso_sharp) cl_gamut

-- > putStrLn $ unlines $ cl_files
cl_files :: [FilePath]
cl_files = map (cl_dir </>) cl_names

cl_load_msg :: Buffer_Id -> [Message]
cl_load_msg = Common.au_loader [0] cl_files

cl_load :: Buffer_Id -> IO ()
cl_load = withSc3 . mapM_ async . cl_load_msg

cl_sfz :: [Sfz_Section]
cl_sfz = Common.sfz_grp_rT 0.1 : Common.sfz_region_1_seq cl_names cl_gamut Nothing

cl_sfz_wr :: IO ()
cl_sfz_wr = sfz_write_sections False (cl_dir </> "cl.sfz") cl_sfz
