-- | Sampler synthdef.
module Sound.Sc3.Auditor.Smplr where

import Data.Function {- base -}
import Data.List {- base -}

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hsc3 -}
import qualified Sound.Sc3.Ugen.Bindings.Db.External as X {- hsc3 -}

import Sound.Sc3.Auditor.Common {- hsc3-auditor -}

-- * Opt

type Smplr_Opt = (Ch_Mode, En_Mode, Lp_Mode)

{- | The set of all possible options.

>>> length smplr_opt_univ
27
-}
smplr_opt_univ :: [Smplr_Opt]
smplr_opt_univ =
  [ (md, en, lp) | md <- ch_mode_univ 2, en <- [En_Gate, En_Dur, En_Buf], lp <- [Lp_None, Lp_Buf, Lp_Seg]
  ]

-- * Name

{- | Derive synthdef name given Opt.

>>> smplr_nm (Ch_Pan_1,En_Gate,Lp_Seg)
"smplr-lp-gt-pn-1"

>>> smplr_nm (Ch_Pan_1,En_Buf,Lp_None)
"smplr-no-pl-pn-1"

>>> smplr_nm (Ch_Direct 2,En_Gate,Lp_Buf)
"smplr-bu-gt-ch-2"

>>> smplr_nm (Ch_Direct 2,En_Gate,Lp_None)
"smplr-no-gt-ch-2"
-}
smplr_nm :: Smplr_Opt -> String
smplr_nm (ch_mode, en_mode, lp_mode) =
  concat
    [ "smplr"
    , case lp_mode of
        Lp_None -> "-no"
        Lp_Seg -> "-lp"
        Lp_Buf -> "-bu"
    , case en_mode of
        En_Gate -> "-gt"
        En_Dur -> "-du"
        En_Buf -> "-pl"
    , case ch_mode of
        Ch_Pan_1 -> "-pn-1"
        Ch_PanAz_1 nc -> "-pz-" ++ show nc
        Ch_Direct nc -> "-ch-" ++ show nc
    ]

-- * Ugen/Synthdef

{- | File playback instrument.

There are three rate controls: rate, tun (master-tuning) and pw (pitch-wheel).

There are four controls for loudness:
amp is (0,1) CVM-0x9 (key-velocity)
vol is (0,1) CC-0x07 (master-volume)
exp is (0,1) CC-0x0B (expression-controller)
lvl is (0,1) voice/part-volume

Amp-Eg: aT rT aC rC

Random: r_amp, r_delay, r_detune, r_pan, r_bus

Ch_Mode sets the number of channels at buffer and determines pan rule.
Ch_Pan_1 controls are: pan, r_pan.

En_Mode decides the release behaviour of the graph.
En_Dur controls are: sustain.

Lp_Mode decides the loop locations.
Lp_Seg controls are: loop_start, loop_end and interp.
Lp_Buf controls are: loop.

rate_f is applied to the playback rate and can be used to apply envelope functions &etc.
-}
smplr_ugen :: (Ugen -> Ugen) -> Smplr_Opt -> Ugen
smplr_ugen rate_f (ch_mode, en_mode, lp_mode) =
  let bufnum = control kr "bufnum" 0 -- sample buffer number
      rate = control kr "rate" 1 -- sample playback rate
      masterTuning = control kr "tun" 0 -- master tuning control (semitones)
      pitchWheel = control kr "pw" 0 -- pitch-wheel (CVM-0xE) (semitones)
      keyVelocity = control kr "amp" 0.1 -- key velocity CVM-0x9
      masterVolume = control kr "vol" 1.0 -- master volume CC-0x07 (0,1)
      expressionController = control kr "exp" 1.0 -- expression controller CC-0x0B (0,1)
      partVolume = control kr "lvl" 1.0 -- voice/part volume
      modulationWheel = control kr "mw" 0 -- modulation wheel CC-0x01 (0,1)
      lfo_rate = control kr "lfo_rate" 4 -- lfo frequency (hz), no sync.
      lfo_depth = control kr "lfo_depth" 0.1 -- lfo depth
      r_amp = control kr "r_amp" 0 -- random amplitude offset (0,+; linear)
      r_delay = control kr "r_delay" 0 -- random pre-delay time (0,+; seconds)
      r_detune = control kr "r_detune" 0 -- random detune (-,+; semitones)
      r_bus = control kr "r_bus" 0 -- random output bus (0,+; linear)
      attackTime = control kr "aT" 0.00 -- attack time
      releaseTime = control kr "rT" 0.01 -- release time
      attackCurve = control kr "aC" (-4) -- attack curve
      releaseCurve = control kr "rC" (-4) -- release curve
      gate_ = control kr "gate" 1 -- gate
      startpos = control kr "startpos" 0 -- frames
      bus = control kr "bus" 0 -- output bus (from bus0)
      bus0 = control kr "bus0" 0 -- output bus (zero)
      rate_mul = midiRatio (pitchWheel + masterTuning + rand2Id 'α' r_detune)
      rate_lfo = 1 + (sinOsc kr lfo_rate 0 * lfo_depth * modulationWheel)
      rate_scl = rate_f (bufRateScale kr bufnum * rate * rate_mul * rate_lfo)
      buf_en = if en_mode == En_Buf then RemoveSynth else DoNothing
      pb = case lp_mode of
        Lp_Seg ->
          let loop_start = control kr "loop_start" 0
              loop_end = control kr "loop_end" (-1)
              ip = control kr "interp" 2
          in X.loopBuf (ch_mode_nc ch_mode) ar bufnum rate_scl 1 startpos loop_start (loop_end + 1) ip -- loop_end is inclusive, loopBuf endLoop is exclusive (?)
        Lp_Buf ->
          let loop_def =
                from_loop
                  ( if en_mode == En_Gate && lp_mode == Lp_Buf
                      then Loop
                      else NoLoop
                  )
              loop = control kr "loop" loop_def
          in playBuf (ch_mode_nc ch_mode) ar bufnum rate_scl 1 startpos (WithLoop loop) buf_en
        Lp_None ->
          playBuf (ch_mode_nc ch_mode) ar bufnum rate_scl 1 startpos NoLoop buf_en
      e = case en_mode of
        En_Gate ->
          let c = (EnvNum attackCurve, EnvNum releaseCurve)
              env_data = envAsr_c attackTime 1 releaseTime c
          in envGen kr gate_ 1 0 1 RemoveSynth env_data
        En_Dur ->
          let sustain = control kr "sustain" 1
              sustainTime = sustain - attackTime - releaseTime
              env_data = envLinen attackTime sustainTime releaseTime 1
          in envGen kr 1 1 0 1 RemoveSynth env_data
        En_Buf -> line kr 0 1 attackTime DoNothing
      d = delayC (pb * e) r_delay (randId 'β' 0 r_delay)
      s = d * (keyVelocity + randId 'γ' 0 r_amp)
      s' =
        let pan = control kr "pan" 0 -- pan (-1 = left, 1 = right) CC-0x0A
            r_pan = control kr "r_pan" 0 -- random pan offset (-,+)
            pan' = pan + rand2Id 'δ' r_pan
            vol = masterVolume * expressionController * partVolume
        in case ch_mode of
            Ch_Pan_1 -> pan2 s pan' vol
            Ch_PanAz_1 nc -> panAz nc s pan' vol 2 0.5
            Ch_Direct _ -> s * vol
  in out (bus + bus0 + randId 'ε' 0 r_bus) s'

{- | Smplr (synthdef).

> audition (smplr (Ch_Pan_1,En_Buf,Lp_None))
-}
smplr :: Smplr_Opt -> Synthdef
smplr opt = synthdef (smplr_nm opt) (smplr_ugen id opt)

-- | Derive parameter names, ie. 'synthdefParam' of 'smplr'
smplr_param :: Smplr_Opt -> Param
smplr_param = synthdefParam . smplr

{- | 'smplr_param' of 'smplr_opt_univ'

>>> length smplr_param_all
30
-}
smplr_param_all :: Param
smplr_param_all = nubBy ((==) `on` fst) (sort (concatMap smplr_param smplr_opt_univ))

smplr_param_all_opt :: [(String, String)] -> Param
smplr_param_all_opt = param_opt_scan (map fst smplr_param_all)

-- | 'd_recv' messages for all smplr variants.
smplr_recv_all_msg :: [Message]
smplr_recv_all_msg = map (d_recv . smplr) smplr_opt_univ

smplr_recv_all :: Transport m => m ()
smplr_recv_all = mapM_ async smplr_recv_all_msg

-- | Load all smplr variants.
smplr_load_all :: IO ()
smplr_load_all = withSc3 smplr_recv_all
