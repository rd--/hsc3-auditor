-- | Pf (Bosendorfer)
module Sound.Sc3.Auditor.Pf where

import System.FilePath {- filepath -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Music.Theory.List as T {- hmt -}

import qualified Sound.Sc3.Auditor.Common as Common

-- | dir = directory
pf_dir :: FilePath
pf_dir = "/home/rohan/data/audio/instr/bosendorfer/"

-- | nc = number-of-channels
pf_nc :: Int
pf_nc = 2

{- | Pf stored tones (the accidental files are pitch-shifted and not used)

> pf_stored !! 28 == 69
-}
pf_stored :: [Key]
pf_stored = [21, 23] ++ [o + pc | o <- [24, 36 .. 96], pc <- [0, 2, 4, 5, 7, 9, 11]] ++ [108]

{- | A0 - C8, non-accidentals.

> pf_stored_n == 2 + 7 * 7 + 1
-}
pf_stored_n :: Int
pf_stored_n = length pf_stored

-- | A0 - C8
pf_range :: (Key, Key)
pf_range = (21, 108)

pf_degree :: Int
pf_degree = Common.range_to_degree pf_range

{- | Pf Buffer_Id lookup

> map (pf_akt_f . \n -> (0,n,60)) [21,22,23] == [(0,0),(0,100),(1,0)]
-}
pf_akt_f :: Common.Akt_f
pf_akt_f = Common.akt_f_simple (zip pf_stored [0 ..])

-- | Pf file-name derivation
pf_file_name_rel :: Common.Dynamic_Name -> Key -> FilePath
pf_file_name_rel dyn x = dyn </> Common.mnn_to_iso_sharp x <.> "aif"

-- | Dynamics (plain)
pf_dyn_pln :: [Common.Dynamic_Name]
pf_dyn_pln = words "008 032 040 056 072 096 104 112 128"

-- | Dynamics (damped)
pf_dyn_dmp :: [Common.Dynamic_Name]
pf_dyn_dmp = words "d012 d060 d084 d108 d127"

pf_dyn_to_rng_tbl :: [(Common.Dynamic_Name, (Key, Key))]
pf_dyn_to_rng_tbl =
  [ ("008", (0, 7))
  , ("032", (8, 31))
  , ("040", (32, 39))
  , ("056", (40, 55))
  , ("072", (56, 71))
  , ("096", (72, 95))
  , ("104", (96, 103))
  , ("112", (103, 111))
  , ("128", (112, 127))
  , ("d012", (0, 11))
  , ("d060", (12, 59))
  , ("d084", (60, 83))
  , ("d108", (84, 107))
  , ("d127", (108, 127))
  ]

pf_dyn_to_rng :: Common.Dynamic_Name -> (Key, Key)
pf_dyn_to_rng dyn = T.lookup_err dyn pf_dyn_to_rng_tbl

pf_sf_names_rel :: Common.Dynamic_Name -> [FilePath]
pf_sf_names_rel dyn = map (pf_file_name_rel dyn) pf_stored

-- > pf_sf_names_rel "008"
pf_sf_names :: Common.Dynamic_Name -> [FilePath]
pf_sf_names = map (pf_dir </>) . pf_sf_names_rel

-- > pf_load_msg "d012" 0
pf_load_msg :: Common.Dynamic_Name -> Buffer_Id -> [Message]
pf_load_msg dyn b0 = map (Common.ld_buf_msg [0, 1]) (zip3 [b0 ..] (pf_sf_names dyn) (repeat Nothing))

pf_load :: Common.Dynamic_Name -> Buffer_Id -> IO ()
pf_load dyn = withSc3 . mapM_ async . pf_load_msg dyn

-- | rT = releaseTime
pf_sfz :: Double -> Common.Dynamic_Name -> [Sfz_Section]
pf_sfz rT dyn =
  Common.sfz_grp_rT rT
    : Common.sfz_region_prt_seq (pf_sf_names_rel dyn) pf_stored

pf_sfz_wr :: Double -> Common.Dynamic_Name -> IO ()
pf_sfz_wr rT dyn = sfz_write_sections False (pf_dir </> dyn <.> "sfz") (pf_sfz rT dyn)

pf_sfz_vel :: Double -> [Common.Dynamic_Name] -> [Sfz_Section]
pf_sfz_vel rT =
  let f dyn =
        Common.sfz_grp_rT_vel (rT, pf_dyn_to_rng dyn)
          : Common.sfz_region_prt_seq (pf_sf_names_rel dyn) pf_stored
  in concatMap f

pf_def_rT :: Double
pf_def_rT = 2

pf_sfz_vel_wr :: IO ()
pf_sfz_vel_wr = do
  sfz_write_sections False (pf_dir </> "pf.sfz") (pf_sfz_vel pf_def_rT pf_dyn_pln)
  sfz_write_sections False (pf_dir </> "pf-d.sfz") (pf_sfz_vel pf_def_rT pf_dyn_dmp)
