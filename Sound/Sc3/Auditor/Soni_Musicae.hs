-- | <http://sonimusicae.free.fr>
module Sound.Sc3.Auditor.Soni_Musicae where

import System.FilePath {- filepath -}
import Text.Printf {- base -}

import qualified Music.Theory.List as T {- hmt-base -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import Sound.Osc.Core {- hsc3 -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Auditor.Common as Common

-- * SM

-- | Allow null voice, for Carillon
sm_vc_nm_prefix :: Common.Voice_Name -> String
sm_vc_nm_prefix vc =
  case vc of
    "" -> ""
    nm -> nm ++ "-"

-- | Rule to make SM name from voice name and midi-note-number.
sm_name :: Common.Voice_Name -> Key -> FilePath
sm_name vc mnn = concat [sm_vc_nm_prefix vc, Common.midi_to_name (mnn - 12), ".wav"]

-- > sm_names [60,72] "B" == ["B-C3.wav","B-C4.wav"]
sm_names :: [Key] -> Common.Voice_Name -> [FilePath]
sm_names gamut vc = map (sm_name vc) gamut

sm_files :: FilePath -> [Key] -> Common.Voice_Name -> [FilePath]
sm_files dir gamut vc = map (dir </>) (sm_names gamut vc)

sm_load_msg :: [Int] -> FilePath -> [Key] -> Common.Voice_Name -> Buffer_Id -> [Message]
sm_load_msg ch dir gamut vc = Common.au_loader ch (sm_files dir gamut vc)

sm_load :: [Int] -> FilePath -> [Key] -> Common.Voice_Name -> Buffer_Id -> IO ()
sm_load ch dir gamut vc b0 = withSc3 (mapM_ async (sm_load_msg ch dir gamut vc b0))

-- | NOTE: values in files are incorrect, see SF2 data
sm_loop :: FilePath -> [Key] -> Common.Voice_Name -> IO [(FilePath, (Int, Int, Int, Int))]
sm_loop dir gamut vc = do
  let nm = sm_names gamut vc
      fn = sm_files dir gamut vc
  lp <- mapM Common.wave_read_lp fn
  return (zip nm lp)

-- * PI <http://sonimusicae.free.fr/petititalien-en.html>

pi_dir :: FilePath
pi_dir = "/home/rohan/data/audio/instr/soni-musicae/Petit-Italien/"

pi_nc :: Num n => n
pi_nc = 2

-- | G#1-E6 (A3 = 415hz, ie. C3 = MIDDLE-C)
pi_range :: (Key, Key)
pi_range = Common.range_octpc_to_midi ((1, 8), (6, 4))

pi_akt_f :: Common.Akt_f
pi_akt_f = Common.akt_f_rng pi_range

pi_degree :: Int
pi_degree = Common.range_to_degree pi_range

pi_gamut :: [Key]
pi_gamut = Common.range_to_gamut pi_range

pi_vc :: [Common.Voice_Name]
pi_vc = words "I II III"

pi_names :: Common.Voice_Name -> [String]
pi_names = sm_names pi_gamut

pi_files :: Common.Voice_Name -> [FilePath]
pi_files = sm_files pi_dir pi_gamut

pi_load_msg :: Common.Voice_Name -> Buffer_Id -> [Message]
pi_load_msg = sm_load_msg [0, 1] pi_dir pi_gamut

pi_load :: Common.Voice_Name -> Buffer_Id -> IO ()
pi_load = sm_load [0, 1] pi_dir pi_gamut

pi_sfz :: Common.Voice_Name -> [Sfz_Section]
pi_sfz vc =
  Common.sfz_grp_rT 0.1
    : Common.sfz_region_1_seq (pi_names vc) pi_gamut Nothing

pi_sfz_wr :: Common.Voice_Name -> IO ()
pi_sfz_wr vc = sfz_write_sections False (pi_dir </> vc <.> "sfz") (pi_sfz vc)

-- * Blanchet 1720 <http://sonimusicae.free.fr/blanchet1-en.html>

bl_dir :: FilePath
bl_dir = "/home/rohan/data/audio/instr/soni-musicae/Blanchet-1720/"

bl_nc :: Num n => n
bl_nc = 2

-- | G#1-F6 (A3 = 415hz, ie. C3 = MIDDLE-C)
bl_range :: (Key, Key)
bl_range = Common.range_octpc_to_midi ((1, 8), (6, 5))

bl_akt_f :: Common.Akt_f
bl_akt_f = Common.akt_f_rng bl_range

bl_degree :: Int
bl_degree = Common.range_to_degree bl_range

bl_gamut :: [Key]
bl_gamut = Common.range_to_gamut bl_range

bl_vc :: [Common.Voice_Name]
bl_vc = words "I II III"

bl_names :: Common.Voice_Name -> [String]
bl_names = sm_names bl_gamut

bl_files :: Common.Voice_Name -> [FilePath]
bl_files = sm_files bl_dir bl_gamut

bl_load_msg :: Common.Voice_Name -> Buffer_Id -> [Message]
bl_load_msg = sm_load_msg [0, 1] bl_dir bl_gamut

bl_load :: Common.Voice_Name -> Buffer_Id -> IO ()
bl_load = sm_load [0, 1] bl_dir bl_gamut

bl_sfz :: Common.Voice_Name -> [Sfz_Section]
bl_sfz vc =
  Common.sfz_grp_rT 0.05
    : Common.sfz_region_1_seq (bl_names vc) bl_gamut Nothing

bl_sfz_wr :: Common.Voice_Name -> IO ()
bl_sfz_wr vc = sfz_write_sections False (bl_dir </> vc <.> "sfz") (bl_sfz vc)

-- * Orgue de Salon <http://sonimusicae.free.fr/orguedesalon-en.html>

os_dir :: FilePath
os_dir = "/home/rohan/data/audio/instr/soni-musicae/Orgue-de-Salon/"

os_nc :: Num n => n
os_nc = 2

-- | C2-G6 (A3 = 440hz, ie. C3 = MIDDLE-C)
os_range :: (Key, Key)
os_range = Common.range_octpc_to_midi ((2, 0), (6, 7))

os_akt_f :: Common.Akt_f
os_akt_f = Common.akt_f_rng os_range

os_degree :: Int
os_degree = Common.range_to_degree os_range

os_gamut :: [Key]
os_gamut = Common.range_to_gamut os_range

{- | OS voices

B = Bourdon 8'
C = Chimney Flute 8'
F = Flute 4'
F2 = Flute 2'
O = Octave 2'
-}
os_vc :: [Common.Voice_Name]
os_vc = words "B C F F2 O"

os_names :: Common.Voice_Name -> [String]
os_names = sm_names os_gamut

os_files :: Common.Voice_Name -> [FilePath]
os_files = sm_files os_dir os_gamut

os_load_msg :: Common.Voice_Name -> Buffer_Id -> [Message]
os_load_msg = sm_load_msg [0, 1] os_dir os_gamut

os_load :: Common.Voice_Name -> Buffer_Id -> IO ()
os_load = sm_load [0, 1] os_dir os_gamut

os_sfz :: Common.Voice_Name -> [Sfz_Section]
os_sfz vc =
  let nm_seq = os_names vc
      lp = map (flip T.lookup_err os_lp) nm_seq
  in Common.sfz_grp_rT 0.05
      : Common.sfz_region_1_seq nm_seq os_gamut (Just lp)

os_sfz_wr :: Common.Voice_Name -> IO ()
os_sfz_wr vc = sfz_write_sections False (os_dir </> vc <.> "sfz") (os_sfz vc)

{- | (NAME,MIDI-NOTE-NUMBER,LOOP-START,LOOP-END)

>> sf2dump Orgue-de-Salon.sf2 | grep Loop
-}
os_data :: [(String, Key, Int, Int)]
os_data =
  [ ("B-A1", 45, 30523, 110869)
  , ("B-A#1", 46, 32610, 79183)
  , ("B-A2", 57, 42893, 86314)
  , ("B-A#2", 58, 11958, 86330)
  , ("B-A3", 69, 76658, 142176)
  , ("B-A#3", 70, 26042, 77723)
  , ("B-A4", 81, 27377, 92954)
  , ("B-A#4", 82, 93634, 105031)
  , ("B-B1", 47, 17315, 77652)
  , ("B-B2", 59, 12277, 66756)
  , ("B-B3", 71, 44487, 102496)
  , ("B-B4", 83, 55386, 70306)
  , ("B-C1", 36, 87481, 151560)
  , ("B-C#1", 37, 61897, 131350)
  , ("B-C2", 48, 50041, 82726)
  , ("B-C#2", 49, 20742, 65663)
  , ("B-C3", 60, 49194, 50205)
  , ("B-C#3", 61, 26890, 97068)
  , ("B-C4", 72, 11328, 17397)
  , ("B-C#4", 73, 7321, 15833)
  , ("B-C5", 84, 77663, 124658)
  , ("B-C#5", 85, 23359, 83215)
  , ("B-D1", 38, 49776, 68089)
  , ("B-D#1", 39, 25089, 86962)
  , ("B-D2", 50, 44395, 88624)
  , ("B-D#2", 51, 89908, 145126)
  , ("B-D3", 62, 20750, 137236)
  , ("B-D#3", 63, 28657, 87449)
  , ("B-D4", 74, 44095, 97958)
  , ("B-D#4", 75, 37617, 111854)
  , ("B-D5", 86, 113106, 141326)
  , ("B-D#5", 87, 49064, 94245)
  , ("B-E1", 40, 45496, 51923)
  , ("B-E2", 52, 118352, 148063)
  , ("B-E3", 64, 19986, 44608)
  , ("B-E4", 76, 19210, 69001)
  , ("B-E5", 88, 25658, 78026)
  , ("B-F1", 41, 30644, 82994)
  , ("B-F#1", 42, 40464, 89115)
  , ("B-F2", 53, 57645, 59668)
  , ("B-F#2", 54, 55601, 63955)
  , ("B-F3", 65, 34675, 84309)
  , ("B-F#3", 66, 27538, 99872)
  , ("B-F4", 77, 26022, 107451)
  , ("B-F#4", 78, 47639, 76133)
  , ("B-F5", 89, 66102, 148576)
  , ("B-F#5", 90, 45747, 93201)
  , ("B-G1", 43, 39290, 80712)
  , ("B-G#1", 44, 35559, 81576)
  , ("B-G2", 55, 79802, 169442)
  , ("B-G#2", 56, 11668, 20385)
  , ("B-G3", 67, 53265, 100890)
  , ("B-G#3", 68, 61808, 74029)
  , ("B-G4", 79, 18647, 77475)
  , ("B-G#4", 80, 15104, 56039)
  , ("B-G5", 91, 89341, 165274)
  , ("C-A1", 45, 24814, 92119)
  , ("C-A#1", 46, 72653, 92334)
  , ("C-A2", 57, 15295, 67451)
  , ("C-A#2", 58, 9669, 73800)
  , ("C-A3", 69, 9116, 76916)
  , ("C-A#3", 70, 12114, 32930)
  , ("C-A4", 81, 13231, 91290)
  , ("C-A#4", 82, 16154, 55898)
  , ("C-B1", 47, 16111, 82446)
  , ("C-B2", 59, 45163, 79832)
  , ("C-B3", 71, 13826, 45342)
  , ("C-B4", 83, 114978, 170830)
  , ("C-C1", 36, 18351, 69550)
  , ("C-C#1", 37, 25294, 50178)
  , ("C-C2", 48, 30413, 73236)
  , ("C-C#2", 49, 31238, 89232)
  , ("C-C3", 60, 95969, 168399)
  , ("C-C#3", 61, 22234, 88232)
  , ("C-C4", 72, 36183, 65279)
  , ("C-C#4", 73, 16890, 72699)
  , ("C-C5", 84, 13956, 87181)
  , ("C-C#5", 85, 41462, 85680)
  , ("C-D1", 38, 62134, 82720)
  , ("C-D#1", 39, 23450, 165318)
  , ("C-D2", 50, 20943, 55799)
  , ("C-D#2", 51, 52582, 76981)
  , ("C-D3", 62, 18293, 69973)
  , ("C-D#3", 63, 31302, 125726)
  , ("C-D4", 74, 25872, 75747)
  , ("C-D#4", 75, 6936, 81105)
  , ("C-D5", 86, 11653, 84186)
  , ("C-D#5", 87, 23659, 86290)
  , ("C-E1", 40, 39754, 83615)
  , ("C-E2", 52, 24789, 86574)
  , ("C-E3", 64, 91953, 173757)
  , ("C-E4", 76, 30771, 78494)
  , ("C-E5", 88, 34126, 52131)
  , ("C-F1", 41, 21504, 67495)
  , ("C-F#1", 42, 10249, 75581)
  , ("C-F2", 53, 20074, 174989)
  , ("C-F#2", 54, 19494, 25429)
  , ("C-F3", 65, 45335, 76430)
  , ("C-F#3", 66, 38982, 90872)
  , ("C-F4", 77, 29317, 81949)
  , ("C-F#4", 78, 20067, 79505)
  , ("C-F5", 89, 11382, 43170)
  , ("C-F#5", 90, 56483, 81787)
  , ("C-G1", 43, 28636, 86252)
  , ("C-G#1", 44, 17795, 87189)
  , ("C-G2", 55, 31541, 178737)
  , ("C-G#2", 56, 26160, 79900)
  , ("C-G3", 67, 7230, 82065)
  , ("C-G#3", 68, 48762, 89828)
  , ("C-G4", 79, 16314, 42039)
  , ("C-G#4", 80, 23120, 91467)
  , ("C-G5", 91, 57551, 88607)
  , ("F2-A1", 45, 29619, 68623)
  , ("F2-A#1", 46, 62608, 95262)
  , ("F2-A2", 57, 11396, 54233)
  , ("F2-A#2", 58, 66315, 90126)
  , ("F2-A3", 69, 14421, 20614)
  , ("F2-A#3", 70, 35491, 41809)
  , ("F2-A4", 81, 58975, 79808)
  , ("F2-A#4", 82, 26746, 84786)
  , ("F2-B1", 47, 77767, 81877)
  , ("F2-B2", 59, 29867, 61763)
  , ("F2-B3", 71, 28468, 71421)
  , ("F2-B4", 83, 16766, 25522)
  , ("F2-C1", 36, 54819, 56505)
  , ("F2-C#1", 37, 31293, 68343)
  , ("F2-C2", 48, 33547, 41981)
  , ("F2-C#2", 49, 13388, 89780)
  , ("F2-C3", 60, 14464, 47080)
  , ("F2-C#3", 61, 30361, 55181)
  , ("F2-C4", 72, 17462, 26971)
  , ("F2-C#4", 73, 48219, 87130)
  , ("F2-C5", 84, 67369, 86619)
  , ("F2-C#5", 85, 14038, 79774)
  , ("F2-D1", 38, 19886, 28299)
  , ("F2-D#1", 39, 45687, 87095)
  , ("F2-D2", 50, 18761, 51878)
  , ("F2-D#2", 51, 12343, 38996)
  , ("F2-D3", 62, 9419, 40820)
  , ("F2-D#3", 63, 53628, 86312)
  , ("F2-D4", 74, 36783, 58824)
  , ("F2-D#4", 75, 46352, 90555)
  , ("F2-D5", 86, 12864, 20504)
  , ("F2-D#5", 87, 83255, 88462)
  , ("F2-E1", 40, 24731, 84825)
  , ("F2-E2", 52, 24181, 73487)
  , ("F2-E3", 64, 19724, 59260)
  , ("F2-E4", 76, 23143, 41869)
  , ("F2-E5", 88, 34790, 57167)
  , ("F2-F1", 41, 9862, 82260)
  , ("F2-F#1", 42, 10513, 72470)
  , ("F2-F2", 53, 26232, 77436)
  , ("F2-F#2", 54, 69079, 87444)
  , ("F2-F3", 65, 67558, 90049)
  , ("F2-F#3", 66, 39477, 50005)
  , ("F2-F4", 77, 12624, 18813)
  , ("F2-F#4", 78, 12958, 75693)
  , ("F2-F5", 89, 33973, 66649)
  , ("F2-F#5", 90, 36758, 44272)
  , ("F2-G1", 43, 10772, 57912)
  , ("F2-G#1", 44, 60605, 75263)
  , ("F2-G2", 55, 42363, 59182)
  , ("F2-G#2", 56, 39501, 86881)
  , ("F2-G3", 67, 57278, 86066)
  , ("F2-G#3", 68, 86929, 90807)
  , ("F2-G4", 79, 78836, 80933)
  , ("F2-G#4", 80, 20996, 36602)
  , ("F2-G5", 91, 58880, 73171)
  , ("F-A1", 45, 12378, 50050)
  , ("F-A#1", 46, 20727, 29237)
  , ("F-A2", 57, 51085, 80061)
  , ("F-A#2", 58, 60662, 79496)
  , ("F-A3", 69, 27400, 58525)
  , ("F-A#3", 70, 7824, 25523)
  , ("F-A4", 81, 20763, 31167)
  , ("F-A#4", 82, 68576, 80310)
  , ("F-B1", 47, 15580, 69545)
  , ("F-B2", 59, 51736, 68342)
  , ("F-B3", 71, 16304, 27147)
  , ("F-B4", 83, 35977, 73824)
  , ("F-C1", 36, 14581, 67882)
  , ("F-C#1", 37, 26866, 84468)
  , ("F-C2", 48, 13958, 57497)
  , ("F-C#2", 49, 17218, 35848)
  , ("F-C3", 60, 64159, 76644)
  , ("F-C#3", 61, 79798, 83696)
  , ("F-C4", 72, 14903, 33480)
  , ("F-C#4", 73, 41562, 70926)
  , ("F-C5", 84, 10138, 20069)
  , ("F-C#5", 85, 33914, 67560)
  , ("F-D1", 38, 14732, 79077)
  , ("F-D#1", 39, 23900, 32405)
  , ("F-D2", 50, 65155, 75249)
  , ("F-D#2", 51, 11957, 63912)
  , ("F-D3", 62, 16405, 33306)
  , ("F-D#3", 63, 3678, 23882)
  , ("F-D4", 74, 43098, 75559)
  , ("F-D#4", 75, 17923, 59766)
  , ("F-D5", 86, 81419, 82734)
  , ("F-D#5", 87, 66894, 88572)
  , ("F-E1", 40, 22659, 79881)
  , ("F-E2", 52, 38574, 87813)
  , ("F-E3", 64, 21294, 51325)
  , ("F-E4", 76, 31677, 62708)
  , ("F-E5", 88, 46594, 49038)
  , ("F-F1", 41, 23156, 72460)
  , ("F-F#1", 42, 36158, 75990)
  , ("F-F2", 53, 44511, 84076)
  , ("F-F#2", 54, 18110, 30517)
  , ("F-F3", 65, 49361, 51382)
  , ("F-F#3", 66, 30103, 46973)
  , ("F-F4", 77, 30184, 60042)
  , ("F-F#4", 78, 7774, 34435)
  , ("F-F5", 89, 52595, 79267)
  , ("F-F#5", 90, 31752, 70080)
  , ("F-G1", 43, 58604, 85193)
  , ("F-G#1", 44, 62094, 66130)
  , ("F-G2", 55, 29333, 85076)
  , ("F-G#2", 56, 46658, 48464)
  , ("F-G3", 67, 66222, 78547)
  , ("F-G#3", 68, 6187, 55707)
  , ("F-G4", 79, 28212, 70661)
  , ("F-G#4", 80, 28966, 63081)
  , ("F-G5", 91, 24384, 83738)
  , ("O-A1", 45, 58088, 60195)
  , ("O-A#1", 46, 72721, 85120)
  , ("O-A2", 57, 29335, 31241)
  , ("O-A#2", 58, 41703, 53674)
  , ("O-A3", 69, 50099, 60403)
  , ("O-A#3", 70, 61514, 65513)
  , ("O-A4", 81, 27880, 62220)
  , ("O-A#4", 82, 79501, 82713)
  , ("O-B1", 47, 63084, 64871)
  , ("O-B2", 59, 88425, 90525)
  , ("O-B3", 71, 34427, 37107)
  , ("O-B4", 83, 29039, 33820)
  , ("O-C1", 36, 81178, 83032)
  , ("O-C#1", 37, 29681, 48293)
  , ("O-C2", 48, 55400, 59697)
  , ("O-C#2", 49, 81521, 92107)
  , ("O-C3", 60, 38832, 40687)
  , ("O-C#3", 61, 50567, 51562)
  , ("O-C4", 72, 73955, 77836)
  , ("O-C#4", 73, 90407, 92618)
  , ("O-C5", 84, 45105, 50946)
  , ("O-C#5", 85, 32316, 83481)
  , ("O-D1", 38, 53543, 57750)
  , ("O-D#1", 39, 24006, 55065)
  , ("O-D2", 50, 40966, 71535)
  , ("O-D#2", 51, 18249, 80551)
  , ("O-D3", 62, 26513, 69167)
  , ("O-D#3", 63, 43665, 95485)
  , ("O-D4", 74, 70661, 88133)
  , ("O-D#4", 75, 57798, 86042)
  , ("O-D5", 86, 57604, 59821)
  , ("O-D#5", 87, 36889, 73909)
  , ("O-E1", 40, 15440, 45829)
  , ("O-E2", 52, 45065, 89166)
  , ("O-E3", 64, 52982, 59075)
  , ("O-E4", 76, 45650, 47524)
  , ("O-E5", 88, 17041, 49606)
  , ("O-F1", 41, 51554, 55724)
  , ("O-F#1", 42, 22855, 78632)
  , ("O-F2", 53, 81324, 83597)
  , ("O-F#2", 54, 72308, 74515)
  , ("O-F3", 65, 10887, 58433)
  , ("O-F#3", 66, 28776, 40050)
  , ("O-F4", 77, 42235, 44983)
  , ("O-F#4", 78, 22570, 23226)
  , ("O-F5", 89, 12670, 16438)
  , ("O-F#5", 90, 73945, 76280)
  , ("O-G1", 43, 74289, 76880)
  , ("O-G#1", 44, 59435, 78245)
  , ("O-G2", 55, 35424, 76447)
  , ("O-G#2", 56, 19548, 23640)
  , ("O-G3", 67, 69580, 71662)
  , ("O-G#3", 68, 85748, 91618)
  , ("O-G4", 79, 65199, 67748)
  , ("O-G#4", 80, 17287, 63339)
  , ("O-G5", 91, 42817, 88353)
  ]

os_lp :: [(FilePath, (Int, Int))]
os_lp = let f (nm, _, st, en) = (nm ++ ".wav", (st, en)) in map f os_data

-- * Ghent Carillon <http://sonimusicae.free.fr/carillondegand-en.html>

gc_dir :: FilePath
gc_dir = "/home/rohan/data/audio/instr/soni-musicae/Carillon-de-Gand/"

gc_nc :: Num n => n
gc_nc = 1

-- | A#1-F6
gc_range :: (Key, Key)
gc_range = Common.range_octpc_to_midi ((1, 10), (6, 5))

gc_degree :: Int
gc_degree = Common.range_to_degree gc_range

gc_gamut :: [Key]
gc_gamut = Common.range_to_gamut gc_range

gc_stored :: [Key]
gc_stored = 34 : 36 : [38 .. 89]

gc_akt_f :: Common.Akt_f
gc_akt_f = Common.akt_f_simple (zip gc_stored [0 ..])

gc_names :: [String]
gc_names = sm_names gc_stored ""

-- > gc_files
gc_files :: [FilePath]
gc_files = sm_files gc_dir gc_stored ""

gc_load_msg :: Buffer_Id -> [Message]
gc_load_msg = sm_load_msg [0] gc_dir gc_stored ""

gc_load :: Buffer_Id -> IO ()
gc_load = sm_load [0] gc_dir gc_stored ""

gc_sfz :: [Sfz_Section]
gc_sfz =
  Common.sfz_grp_lm "one_shot"
    : Common.sfz_region_prt_seq gc_names gc_stored

gc_sfz_wr :: IO ()
gc_sfz_wr = sfz_write_sections False (gc_dir </> "gc" <.> "sfz") gc_sfz

-- * Italian Spinet <http://duphly.free.fr/en/epinette.html>

-- | si = spinet-italian
si_dir :: FilePath
si_dir = "/home/rohan/data/audio/instr/soni-musicae/Italian-Spinet/"

-- | nc = number-of-channels
si_nc :: Num n => n
si_nc = 2

-- | A1-E6
si_range :: (Key, Key)
si_range = Common.range_octpc_to_midi ((1, 9), (6, 4))

si_akt_f :: Common.Akt_f
si_akt_f = Common.akt_f_rng si_range

si_degree :: Int
si_degree = Common.range_to_degree si_range

si_gamut :: [Key]
si_gamut = Common.range_to_gamut si_range

si_vc :: [Common.Voice_Name]
si_vc = words "I II"

si_names :: Common.Voice_Name -> [String]
si_names = sm_names si_gamut

si_files :: Common.Voice_Name -> [FilePath]
si_files = sm_files si_dir si_gamut

si_load_msg :: Common.Voice_Name -> Buffer_Id -> [Message]
si_load_msg = sm_load_msg [0, 1] si_dir si_gamut

si_load :: Common.Voice_Name -> Buffer_Id -> IO ()
si_load = sm_load [0, 1] si_dir si_gamut

si_sfz :: Common.Voice_Name -> [Sfz_Section]
si_sfz vc =
  Common.sfz_grp_rT 0.1
    : Common.sfz_region_1_seq (si_names vc) si_gamut Nothing

si_sfz_wr :: Common.Voice_Name -> IO ()
si_sfz_wr vc = sfz_write_sections False (si_dir </> vc <.> "sfz") (si_sfz vc)

-- * Diato <http://sonimusicae.free.fr/diato-en.html>

-- | di = diatonic-accordion, dir = directory
di_dir :: FilePath
di_dir = "/home/rohan/data/audio/instr/soni-musicae/Diato/"

-- | nc = number-of-channels
di_nc :: Num n => n
di_nc = 2

-- | D3-E6
di_range :: (Key, Key)
di_range = Common.range_octpc_to_midi ((3, 2), (6, 4))

di_akt_f :: Common.Akt_f
di_akt_f = Common.akt_f_rng di_range

di_degree :: Int
di_degree = Common.range_to_degree di_range

di_gamut :: [Key]
di_gamut = Common.range_to_gamut di_range

di_vc :: [Common.Voice_Name]
di_vc = words "I II"

di_names :: Common.Voice_Name -> [String]
di_names = sm_names di_gamut

-- > di_files "I"
di_files :: Common.Voice_Name -> [FilePath]
di_files = sm_files di_dir di_gamut

di_load_msg :: Common.Voice_Name -> Buffer_Id -> [Message]
di_load_msg = sm_load_msg [0, 1] di_dir di_gamut

di_load :: Common.Voice_Name -> Buffer_Id -> IO ()
di_load = sm_load [0, 1] di_dir di_gamut

di_sfz :: Common.Voice_Name -> [Sfz_Section]
di_sfz vc =
  let nm_seq = di_names vc
      f nm = T.lookup_err nm di_lp
  in Common.sfz_grp_rT 0.05
      : Common.sfz_region_1_seq nm_seq di_gamut (Just (map f nm_seq))

di_sfz_wr :: Common.Voice_Name -> IO ()
di_sfz_wr vc = sfz_write_sections False (di_dir </> vc <.> "sfz") (di_sfz vc)

{- | SF2 loop-point data

>> sf2dump Diato.sf2 | grep Loop
-}
di_data :: [(String, Int, Int)]
di_data =
  [ ("I-A#3", 0, 477443) -- MISSING...
  , ("I-D2", 220300, 460550)
  , ("I-D#2", 254498, 485774)
  , ("I-E2", 249864, 477432)
  , ("I-F2", 129193, 291267)
  , ("I-F#2", 116998, 263031)
  , ("I-G2", 238850, 365275)
  , ("I-G#2", 402404, 460387)
  , ("I-A2", 448451, 522758)
  , ("I-A#2", 358666, 496311)
  , ("I-B2", 405308, 512212)
  , ("I-C3", 350384, 499769)
  , ("I-C#3", 289686, 377925)
  , ("I-D3", 210203, 346862)
  , ("I-D#3", 122012, 248188)
  , ("I-E3", 311676, 440899)
  , ("I-F3", 392942, 484461)
  , ("I-F#3", 162699, 222977)
  , ("I-G3", 133135, 257053)
  , ("I-G#3", 479637, 500316)
  , ("I-A3", 159750, 276340)
  , ("I-A3#", 334792, 462570)
  , ("I-B3", 389690, 515980)
  , ("I-C4", 133105, 169109)
  , ("I-C#4", 344097, 405441)
  , ("I-D4", 343756, 417819)
  , ("I-D#4", 309006, 402591)
  , ("I-E4", 235893, 349991)
  , ("I-F4", 278128, 417638)
  , ("I-F#4", 40687, 179076)
  , ("I-G4", 242854, 378591)
  , ("I-G#4", 242067, 256827)
  , ("I-A4", 171803, 352915)
  , ("I-A#4", 181514, 245305)
  , ("I-B4", 170496, 254975)
  , ("I-C5", 139432, 275423)
  , ("I-C#5", 186880, 401407)
  , ("I-D5", 327948, 403360)
  , ("I-D#5", 105081, 211393)
  , ("I-E5", 240640, 319999)
  , ("II-D2", 128721, 292847)
  , ("II-D#2", 116213, 249423)
  , ("II-E2", 77245, 231819)
  , ("II-F2", 141687, 411314)
  , ("II-F#2", 133084, 364257)
  , ("II-G2", 189363, 353999)
  , ("II-G#2", 269385, 466179)
  , ("II-A2", 300614, 452094)
  , ("II-A#2", 195335, 345572)
  , ("II-B2", 136187, 329803)
  , ("II-C3", 112961, 288965)
  , ("II-C#3", 319147, 416134)
  , ("II-D3", 38546, 511083)
  , ("II-D#3", 172240, 351530)
  , ("II-E3", 218817, 454210)
  , ("II-F3", 287988, 464253)
  , ("II-F#3", 110674, 442133)
  , ("II-G3", 209916, 343253)
  , ("II-G#3", 405773, 493643)
  , ("II-A3", 410473, 523733)
  , ("II-A#3", 284731, 342805)
  , ("II-B3", 70644, 143329)
  , ("II-C4", 198716, 508706)
  , ("II-C#4", 220614, 458405)
  , ("II-D4", 363594, 503726)
  , ("II-D#4", 304153, 506389)
  , ("II-E4", 341112, 467492)
  , ("II-F4", 178477, 290323)
  , ("II-F#4", 228669, 456186)
  , ("II-G4", 342861, 417914)
  , ("II-G#4", 319692, 461766)
  , ("II-A4", 237388, 379086)
  , ("II-A#4", 224307, 358333)
  , ("II-B4", 248045, 428513)
  , ("II-C5", 110592, 381147)
  , ("II-C#5", 422813, 507533)
  , ("II-D5", 214913, 315607)
  , ("II-D#5", 241429, 317927)
  , ("II-E5", 223416, 345973)
  ]

di_lp :: [(FilePath, (Int, Int))]
di_lp = let f (nm, st, en) = (nm ++ ".wav", (st, en)) in map f di_data

-- * PF <http://sonimusicae.free.fr/matshelgesson-maestro-en.html>

-- | di = diatonic-accordion, dir = directory
pf_dir :: FilePath
pf_dir = "/home/rohan/data/audio/instr/soni-musicae/PF-Yamaha-CF3/"

pf_file :: FilePath -> FilePath
pf_file = (</>) pf_dir

-- | nc = number-of-channels
pf_nc :: Num n => n
pf_nc = 2

-- | A0-C8
pf_range :: (Key, Key)
pf_range = Common.range_octpc_to_midi ((0, 9), (8, 0))

pf_akt_f :: Common.Akt_f
pf_akt_f = Common.akt_f_rng pf_range

pf_degree :: Int
pf_degree = Common.range_to_degree pf_range

pf_gamut :: [Key]
pf_gamut = Common.range_to_gamut pf_range

pf_dyn :: [Common.Dynamic_Name]
pf_dyn = words "p mp mf f ff"

pf_name :: Common.Dynamic_Name -> Key -> String
pf_name dyn mnn = printf "mcg_%s_%03d.wav" dyn mnn

pf_names :: Common.Dynamic_Name -> [String]
pf_names dyn = map (pf_name dyn) pf_gamut

pf_files :: Common.Dynamic_Name -> [FilePath]
pf_files = map pf_file . pf_names

pf_load_msg :: Common.Dynamic_Name -> Buffer_Id -> [Message]
pf_load_msg dyn b0 = map (Common.ld_buf_msg [0, 1]) (zip3 [b0 ..] (pf_files dyn) (repeat Nothing))

pf_load :: Common.Voice_Name -> Buffer_Id -> IO ()
pf_load dyn = withSc3 . mapM_ async . pf_load_msg dyn

pf_sfz :: Common.Dynamic_Name -> [Sfz_Section]
pf_sfz vc =
  Common.sfz_grp_rT 0.1
    : Common.sfz_region_1_seq (pf_names vc) pf_gamut Nothing

pf_sfz_wr :: Common.Dynamic_Name -> IO ()
pf_sfz_wr vc = sfz_write_sections False (pf_dir </> vc <.> "sfz") (pf_sfz vc)
