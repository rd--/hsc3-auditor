-- | meta = meta-data, named au-instr
module Sound.Sc3.Auditor.Meta where

import Data.Maybe {- base -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import Sound.Midi.Type {- midi-osc -}

import qualified Sound.Sc3.Auditor.Common as Common
import qualified Sound.Sc3.Auditor.Server.Plain as Plain

import qualified Sound.Sc3.Auditor.A37z as A37z
import qualified Sound.Sc3.Auditor.Casacota as Ca
import qualified Sound.Sc3.Auditor.Cdm as Cdm
import qualified Sound.Sc3.Auditor.Celeste as Cel
import qualified Sound.Sc3.Auditor.Clavichord as Cl
import qualified Sound.Sc3.Auditor.Crotales as Crotales
import qualified Sound.Sc3.Auditor.Fcd as Fcd
import qualified Sound.Sc3.Auditor.Mellotron as Ml
import qualified Sound.Sc3.Auditor.Pf as Pf
import qualified Sound.Sc3.Auditor.Philharmonia as Ph
import qualified Sound.Sc3.Auditor.Soni_Musicae as Sm
import qualified Sound.Sc3.Auditor.Vienna as Vn

-- | (instr-name,buffer-zero,voice-name,dynamic-name)
type Au_Sel = (String, Buffer_Id, Common.Voice_Name, Common.Dynamic_Name)

-- | Select load_msg function by name.
au_load_msg :: Au_Sel -> [Message]
au_load_msg (instr, b0, vc, dyn) =
  case instr of
    "a37z" -> A37z.a37z_load_msg b0 vc
    "a37z-zc" -> A37z.a37z_zc_load_msg b0 vc
    "ca-esmuc" -> Ca.esmuc_load_msg b0 vc
    "ca-esmuc-lp" -> Ca.esmuc_load_lp_msg Ca.Ca_Stereo b0 vc
    "ca-esmuc-pedal" -> Ca.esmuc_pedal_load_msg b0 vc
    "ca-esmuc-pedal-lp" -> Ca.esmuc_pedal_load_lp_msg b0 vc
    "ca-hauslaib" -> Ca.hl_load_msg b0 vc
    "ca-hauslaib-lp" -> Ca.hl_load_lp_msg b0 vc
    "ca-zell" -> Ca.zell_load_msg b0 vc
    "cdm-gpsl" -> Cdm.cdm_gpsl_load_msg b0
    "cdm-gspl" -> Cdm.cdm_gspl_load_msg b0
    "cdm-gssl" -> Cdm.cdm_gssl_load_msg b0
    "cel" -> Cel.cel_load_msg b0 vc
    "cl" -> Cl.cl_load_msg b0
    "crotales" -> Crotales.crotale_load_msg b0
    "fcd" -> Fcd.fcd_load_msg b0 vc
    "fcd-zc" -> Fcd.fcd_zc_load_msg b0 vc
    "ml" -> Ml.ml_load_msg b0 vc
    "pf" -> Pf.pf_load_msg dyn b0
    "ph-bcl" -> Ph.bcl_load_msg b0
    "sm-bl" -> Sm.bl_load_msg vc b0
    "sm-di" -> Sm.di_load_msg vc b0
    "sm-gc" -> Sm.gc_load_msg b0
    "sm-os" -> Sm.os_load_msg vc b0
    "sm-pf" -> Sm.pf_load_msg dyn b0
    "sm-pi" -> Sm.pi_load_msg vc b0
    "sm-si" -> Sm.si_load_msg vc b0
    "vn-fl2_pV_nA_sus_pp" -> Vn.fl2_pV_nA_sus_pp_load_msg b0
    "vn-ha_es_mf" -> Vn.ha_es_mf_load_msg b0
    "vn-ha_es_pdlt_mp" -> Vn.ha_es_pdlt_mp_load_msg b0
    "vn-ha_rs_bisb_mp" -> Vn.ha_rs_bisb_mp_load_msg b0
    "vn-ha_rs_es_mu_p" -> Vn.ha_rs_es_mu_p_load_msg b0
    "vn-ho_LV_nA_sus_mp" -> Vn.ho_LV_nA_sus_mp_load_msg b0
    "vn-ho_oV_nA_sus_mp" -> Vn.ho_oV_nA_sus_mp_load_msg b0
    "vn-klb_pA_sus_p" -> Vn.klb_pA_sus_p_load_msg b0
    "vn-klb_stac_p1" -> Vn.klb_stac_p1_load_msg b0
    "vn-po_nA_sus_p" -> Vn.po_nA_sus_p_load_msg b0
    "vn-trc_oV_nA_sus_mp" -> Vn.trC_oV_nA_sus_mp_load_msg b0
    "vn-tu_oV_nA_sus_mp" -> Vn.tu_oV_nA_sus_mp_load_msg b0
    "vn-va_mV_sus_mf" -> Vn.va_mV_sus_mf_load_msg b0
    "vn-vc_mV_sus_mf" -> Vn.vc_mV_sus_mf_load_msg b0
    "vn-vi_mV_sus_mf" -> Vn.vi_mV_sus_mf_load_msg b0
    _ -> error (unwords ["au_load_msg", instr, vc, dyn])

au_load :: Au_Sel -> IO ()
au_load (instr, b0, vc, dyn) = withSc3 (mapM_ async (au_load_msg (instr, b0, vc, dyn)))

{- | Select instr data by name

NOTE: vc is required only for FCD, which should be corrected to (fcd-upper,fcd-lower,fcd-bass)
-}
au_instr_dat :: (String, Common.Voice_Name) -> (Common.Akt_f, (Key, Key), Int)
au_instr_dat (ins, vc) = do
  case ins of
    "a37z" -> (A37z.a37z_akt_f, A37z.a37z_range, A37z.a37z_nc) -- lp=buf
    "ca-esmuc" -> (Ca.esmuc_akt_f, Ca.esmuc_range, Ca.esmuc_nc)
    "ca-esmuc-pedal" -> (Ca.esmuc_pedal_akt_f, Ca.esmuc_pedal_range, Ca.esmuc_nc)
    "ca-hauslaib" -> (Ca.hl_akt_f, Ca.hl_range, Ca.hl_nc)
    "ca-zell" -> (Ca.zell_akt_f, Ca.zell_range, Ca.zell_nc) -- zell_akt_et12_f
    "cdm-gpsl" -> (Cdm.cdm_gpsl_akt_et12_f, Cdm.cdm_gpsl_range, Cdm.cdm_nc)
    "cdm-gspl" -> (Cdm.cdm_gspl_akt_et12_f, Cdm.cdm_gspl_range, Cdm.cdm_nc)
    "cdm-gssl" -> (Cdm.cdm_gssl_akt_et12_f, Cdm.cdm_gssl_range, Cdm.cdm_nc)
    "cel" -> (Cel.cel_akt_f, Cel.cel_range, Cel.cel_nc)
    "cl" -> (Cl.cl_akt_f, Cl.cl_range, Cl.cl_nc)
    "crotales" -> (Crotales.crotale_akt_f, Crotales.crotale_range, Crotales.crotale_nc)
    "fcd" -> (Fcd.fcd_akt_f vc, Fcd.fcd_range vc, Fcd.fcd_nc)
    "ml" -> (Ml.ml_akt_f, Ml.ml_range, Ml.ml_nc)
    "pf" -> (Pf.pf_akt_f, Pf.pf_range, Pf.pf_nc)
    "ph-bcl" -> (Ph.bcl_akt_f, Ph.bcl_range, Ph.bcl_nc)
    "sm-bl" -> (Sm.bl_akt_f, Sm.bl_range, Sm.bl_nc)
    "sm-di" -> (Sm.di_akt_f, Sm.di_range, Sm.di_nc)
    "sm-gc" -> (Sm.gc_akt_f, Sm.gc_range, Sm.gc_nc)
    "sm-os" -> (Sm.os_akt_f, Sm.os_range, Sm.os_nc)
    "sm-pf" -> (Sm.pf_akt_f, Sm.pf_range, Sm.pf_nc)
    "sm-pi" -> (Sm.pi_akt_f, Sm.pi_range, Sm.pi_nc)
    "sm-si" -> (Sm.si_akt_f, Sm.si_range, Sm.si_nc)
    "vn-fl2_pV_nA_sus_pp" -> (Vn.fl2_pV_nA_sus_pp_akt_f, Vn.fl2_range, Vn.vienna_nc)
    "vn-ha_es_mf" -> (Vn.ha_es_mf_akt_f, Vn.ha_es_mf_range, Vn.vienna_nc)
    "vn-ha_es_pdlt_mp" -> (Vn.ha_es_pdlt_mp_akt_f, Vn.ha_es_pdlt_mp_range, Vn.vienna_nc)
    "vn-ha_rs_bisb_mp" -> (Vn.ha_rs_bisb_mp_akt_f, Vn.ha_rs_bisb_mp_range, Vn.vienna_nc)
    "vn-ha_rs_es_mu_p" -> (Vn.ha_rs_es_mu_p_akt_f, Vn.ha_rs_es_mu_p_range, Vn.vienna_nc)
    "vn-ho_LV_nA_sus_mp" -> (Vn.ho_LV_nA_sus_mp_akt_f, Vn.ho_LV_nA_sus_mp_range, Vn.vienna_nc)
    "vn-ho_oV_nA_sus_mp" -> (Vn.ho_oV_nA_sus_mp_akt_f, Vn.ho_oV_nA_sus_mp_range, Vn.vienna_nc)
    "vn-klb_pA_sus_p" -> (Vn.klb_akt_f, Vn.klb_range, Vn.vienna_nc)
    "vn-klb_stac_p1" -> (Vn.klb_akt_f, Vn.klb_range, Vn.vienna_nc)
    "vn-po_nA_sus_p" -> (Vn.po_nA_sus_p_akt_f, Vn.po_nA_sus_p_range, Vn.vienna_nc)
    "vn-trc_oV_nA_sus_mp" -> (Vn.trC_oV_nA_sus_mp_akt_f, Vn.trC_oV_nA_sus_mp_range, Vn.vienna_nc)
    "vn-tu_oV_nA_sus_mp" -> (Vn.tu_oV_nA_sus_mp_akt_f, Vn.tu_oV_nA_sus_mp_range, Vn.vienna_nc)
    "vn-va_mV_sus_mf" -> (Vn.va_mV_sus_mf_akt_f, Vn.va_mV_sus_mf_range, Vn.vienna_nc)
    "vn-vc_mV_sus_mf" -> (Vn.vc_mV_sus_mf_akt_f, Vn.vc_mV_sus_mf_range, Vn.vienna_nc)
    "vn-vi_mV_sus_mf" -> (Vn.vi_mV_sus_mf_akt_f, Vn.vi_mV_sus_mf_range, Vn.vienna_nc)
    _ -> error (unwords ["au_instr_dat", ins, vc])

-- * Msg

type F64 = Double

-- | Derive message for meta definition.
meta_sc3_msg_f :: (F64, Common.Akt_f) -> (String, Synth_Id, Group_Id, Param) -> (Channel, F64, F64) -> Maybe Message
meta_sc3_msg_f (rt_mul, akt_f) (syn, nid, gid, prm) evt =
  let arg = Common.smplr_calc_rate rt_mul akt_f evt
  in Just (s_new syn nid AddToTail gid (param_merge_r prm arg))

-- | Biases to prm_opt.
meta_msg_f :: (F64, Common.Akt_f, Param) -> Plain.Msg_F usr
meta_msg_f (rt_mul, akt_f, prm_opt) evt (syn, nid, gid, prm, usr) =
  let msg = fromJust (meta_sc3_msg_f (rt_mul, akt_f) (syn, nid, gid, param_merge_r prm prm_opt) evt)
  in (nid, usr, [msg])

-- * Sfz

au_sfz_wr :: IO ()
au_sfz_wr = do
  mapM_ A37z.a37z_sfz_wr A37z.a37z_vc
  mapM_ A37z.a37z_zc_sfz_wr A37z.a37z_vc
  -- Ca
  mapM_ Ca.zell_sfz_wr Ca.zell_vc
  mapM_ Ca.hl_sfz_wr Ca.hl_vc
  mapM_ Ca.esmuc_sfz_wr Ca.esmuc_vc
  mapM_ Ca.esmuc_pedal_sfz_wr Ca.esmuc_pedal_vc
  mapM_ Cel.cel_sfz_wr Cel.cel_vc
  Cl.cl_sfz_wr
  Crotales.crotale_sfz_wr
  mapM_ Fcd.fcd_sfz_wr Fcd.fcd_vc
  mapM_ Fcd.fcd_zc_sfz_wr Fcd.fcd_vc
  mapM_ Ml.ml_sfz_wr Ml.ml_vc
  Ph.bcl_sfz_wr
  mapM_ (Pf.pf_sfz_wr Pf.pf_def_rT) Pf.pf_dyn_pln
  mapM_ (Pf.pf_sfz_wr Pf.pf_def_rT) Pf.pf_dyn_dmp
  -- Sm
  mapM_ Sm.bl_sfz_wr Sm.bl_vc
  mapM_ Sm.pi_sfz_wr Sm.pi_vc
  mapM_ Sm.os_sfz_wr Sm.os_vc
  Sm.gc_sfz_wr
  mapM_ Sm.si_sfz_wr Sm.si_vc
  mapM_ Sm.di_sfz_wr Sm.di_vc
  mapM_ Sm.pf_sfz_wr Sm.pf_dyn
  -- Vn
  Vn.ha_rs_bisb_mp_sfz_wr
  Vn.ha_rs_es_mu_p_sfz_wr
  Vn.ha_es_pdlt_mp_sfz_wr
  Vn.ha_es_mf_sfz_wr
  Vn.klb_pA_sus_p_sfz_rw
  Vn.fl2_pV_nA_sus_pp_sfz_wr
