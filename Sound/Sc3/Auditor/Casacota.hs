-- | Casacota <http://www.casacota.cat/perl?num=1404985795>
module Sound.Sc3.Auditor.Casacota where

import Data.Char {- base -}
import System.FilePath {- filepath -}
import Text.Printf {- base -}

import qualified Music.Theory.List as T {- hmt -}
import qualified Music.Theory.Tuning as T {- hmt -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Auditor.Casacota.Data as Casacota.Data
import qualified Sound.Sc3.Auditor.Common as Common

-- | dir = directory
ca_dir :: FilePath
ca_dir = "/home/rohan/data/audio/instr/casacota"

ca_file :: FilePath -> FilePath
ca_file = (</>) ca_dir

-- | nc = number-of-channels
ca_nc :: Int
ca_nc = 2

data Ca_Ch = Ca_Stereo | Ca_Left | Ca_Right

ca_ch :: Ca_Ch -> [Int]
ca_ch ch = case ch of Ca_Stereo -> [0, 1]; Ca_Left -> [0]; Ca_Right -> [1]

ca_ch_nc :: Ca_Ch -> Int
ca_ch_nc ch = case ch of Ca_Stereo -> 2; Ca_Left -> 1; Ca_Right -> 1

{- | Cached at Casacota.Data.
  Loop points are stored at WAV files as FRAMES not BYTES.
-}
ca_lp_read :: (FilePath -> FilePath) -> [FilePath] -> IO [(FilePath, Common.LoopPoints)]
ca_lp_read fn_f nm = do
  lp <- mapM Common.wave_read_lp (map fn_f nm)
  return (zip nm (map (\(_, _, st, en) -> (st, en)) lp))

-- * Zell 1737 <http://www.casacota.cat/perl?num=1404987512>

-- | dir = directory
zell_dir :: FilePath
zell_dir = ca_file "zell_1737_415_MeanTone5"

zell_file :: FilePath -> FilePath
zell_file = (</>) zell_dir

zell_nc :: Int
zell_nc = ca_nc

-- | C2-D6
zell_range :: (Key, Key)
zell_range = (36, 86)

zell_akt_f :: Common.Akt_f
zell_akt_f = Common.akt_f_rng zell_range

zell_degree :: Int
zell_degree = Common.range_to_degree zell_range

zell_gamut :: [Key]
zell_gamut = Common.range_to_gamut zell_range

-- | vc = voice
zell_vc :: [Common.Voice_Name]
zell_vc = ["8_i", "8_ii", "4", "8_llaut"]

-- | sf = sound-file
zell_sf_names_rel :: Common.Voice_Name -> [FilePath]
zell_sf_names_rel z = map (\n -> printf "%s/%s_%02d.wav" z z n) [1 .. zell_degree]

-- | Absolute file names, in sequence, of voice.
zell_sf_names :: Common.Voice_Name -> [FilePath]
zell_sf_names = map zell_file . zell_sf_names_rel

zell_load_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
zell_load_msg b0 vc = Common.au_loader [0, 1] (zell_sf_names vc) b0

zell_load :: Buffer_Id -> Common.Voice_Name -> IO ()
zell_load b0 = withSc3 . mapM_ async . zell_load_msg b0

zell_et12_sfz :: Common.Voice_Name -> [Sfz_Section]
zell_et12_sfz vc =
  let f (mnn, (_, dt)) = (mnn, negate (truncate dt))
  in Common.sfz_grp_rT 0.05
      : Common.sfz_region_prt_tun_seq (zell_sf_names_rel vc) (map f zell_akt_tbl)

zell_sfz :: Common.Voice_Name -> [Sfz_Section]
zell_sfz vc =
  Common.sfz_grp_rT 0.05
    : Common.sfz_region_1_seq (zell_sf_names_rel vc) zell_gamut Nothing

zell_sfz_wr :: Common.Voice_Name -> IO ()
zell_sfz_wr vc = do
  sfz_write_sections False (zell_dir </> (vc ++ "-et12") <.> "sfz") (zell_et12_sfz vc)
  sfz_write_sections False (zell_dir </> vc <.> "sfz") (zell_sfz vc)

-- * Zell-Tuning

-- | Zell tuning frequencies (in hz), measured, for Zell files, (C4-B4).
zell_cps :: Num n => [n]
zell_cps = [247, 259, 276, 295, 309, 330, 346, 369, 387, 413, 441, 462]

{- | Detune, in cents, from 12-ET at A440
  (Zell is at 415, ie. one semi-tone flat, which is adjuested for, differences are in -50,+50)

>>> midiCps 68
415.3046975799451
-}
zell_cdiff :: [T.Cents]
zell_cdiff =
  let et12_cps = map midiCps [60 :: Double .. 71]
  in map (+ 100.0) (zipWith T.cps_difference_cents et12_cps zell_cps)

zell_akt_tbl :: Common.Akt_Tbl
zell_akt_tbl = zip zell_gamut (zip [0 ..] (cycle zell_cdiff))

zell_akt_et12_f :: Common.Akt_f
zell_akt_et12_f = Common.akt_f_cdiff zell_akt_tbl

{- | Zell tuning in cents.

>>> map round zell_cents
[0,83,193,308,388,502,584,695,778,890,1004,1084]
-}
zell_cents :: [T.Cents]
zell_cents = zipWith (+) [0, 100 ..] zell_cdiff

zell_mnn_cps_tbl :: (Num i, Enum i, Fractional n) => [(i, n)]
zell_mnn_cps_tbl =
  let mnn = concatMap (\m -> map (\x -> x + 60 + (m * 12)) [0 .. 11]) [-5 .. 6]
      cps = concatMap (\m -> map (* m) zell_cps) [1 / 32, 1 / 16, 1 / 8, 1 / 4, 1 / 2, 1, 2, 4, 8, 16, 32, 64]
  in zip mnn cps

{- | Scala name for tuning very near to Zell tuning.

\$ hmt-scala stat scale nil holder
name        : holder
description : William Holder's equal beating meantone temperament (1694). 3/2 beats 2.8 Hz
degree      : 12
type        : non-uniform (Pitch_Cents,11:1)
perfect-oct : True
cents-i     : [0,81,194,307,388,503,584,696,778,890,1004,1085,1200]
\$
-}
zell_scl_nm :: String
zell_scl_nm = "holder"

-- * Hauslaib 1590 <http://www.casacota.cat/perl?num=1404987463>

-- | dir = directory
hl_dir :: FilePath
hl_dir = ca_file "Hauslaib_1590_415_MeanTone4"

hl_file :: FilePath -> FilePath
hl_file = (</>) hl_dir

hl_nc :: Int
hl_nc = ca_nc

-- | C3 - A6
hl_range :: (Key, Key)
hl_range = Common.range_octpc_to_midi ((3, 0), (6, 9))

hl_akt_f :: Common.Akt_f
hl_akt_f = Common.akt_f_rng hl_range

hl_degree :: Int
hl_degree = Common.range_to_degree hl_range

hl_gamut :: [Key]
hl_gamut = Common.range_to_gamut hl_range

hl_vc :: [String]
hl_vc = words "flauteadillos realejos_de_batalla realejos monacordio beintidosenas quinzenas"

-- | C3 is stored as 01-c1.wav, A4 is stored as 22-a2.wav, A6 is stored as 46-a4.wav
hl_nm_seq :: [String]
hl_nm_seq =
  let f i mnn = printf "%02d-%s.wav" i (map toLower (Common.mnn_to_iso_sharp (mnn - 24)))
  in zipWith f [1 :: Int ..] hl_gamut

-- | sf = sound-file
hl_sf_names_rel :: Common.Voice_Name -> [FilePath]
hl_sf_names_rel vc = map ((</>) vc) hl_nm_seq

hl_sf_names :: Common.Voice_Name -> [FilePath]
hl_sf_names = map hl_file . hl_sf_names_rel

hl_load_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
hl_load_msg b0 vc = Common.au_loader [0, 1] (hl_sf_names vc) b0

hl_load :: Buffer_Id -> Common.Voice_Name -> IO ()
hl_load b0 = withSc3 . mapM_ async . hl_load_msg b0

-- > mapM hl_lp_read hl_vc
hl_lp_read :: Common.Voice_Name -> IO (Common.Voice_Name, [(FilePath, Common.LoopPoints)])
hl_lp_read vc = fmap ((,) vc) (ca_lp_read hl_file (hl_sf_names_rel vc))

hl_vc_lp :: Common.Voice_Name -> [Common.LoopPoints]
hl_vc_lp vc = map snd (T.lookup_err vc Casacota.Data.hl_lp)

hl_load_lp_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
hl_load_lp_msg b0 vc = Common.au_loader_lp [0, 1] (hl_sf_names vc) b0 (map Just (hl_vc_lp vc))

hl_load_lp :: Buffer_Id -> Common.Voice_Name -> IO ()
hl_load_lp b0 = withSc3 . mapM_ async . hl_load_lp_msg b0

hl_sfz :: Common.Voice_Name -> [Sfz_Section]
hl_sfz vc =
  Common.sfz_grp_rT 0.05
    : Common.sfz_region_1_seq (hl_sf_names_rel vc) hl_gamut (Just (hl_vc_lp vc))

hl_sfz_wr :: Common.Voice_Name -> IO ()
hl_sfz_wr vc = sfz_write_sections False (hl_dir </> vc <.> "sfz") (hl_sfz vc)

-- * Escola Superior de Música de Catalunya <http://www.casacota.cat/perl?num=1404987288>

-- | dir = directory
esmuc_dir :: FilePath
esmuc_dir = ca_file "orgue_esmuc_415_Werckmeister3"

esmuc_file :: FilePath -> FilePath
esmuc_file = (</>) esmuc_dir

esmuc_nc :: Int
esmuc_nc = ca_nc

-- | C2 - G7
esmuc_range :: (Key, Key)
esmuc_range = Common.range_octpc_to_midi ((2, 0), (6, 7))

esmuc_akt_f :: Common.Akt_f
esmuc_akt_f = Common.akt_f_rng esmuc_range

esmuc_degree :: Int
esmuc_degree = Common.range_to_degree esmuc_range

esmuc_gamut :: [Key]
esmuc_gamut = Common.range_to_gamut esmuc_range

esmuc_vc :: [Common.Voice_Name]
esmuc_vc =
  concat
    [ words "om_flautat om_octava om_dotzena om_nasard_17 om_plens"
    , words "po_bordo po_xemeneia po_quinzena po_oboe"
    ]

-- | Transform voice name into file-name prefix
esmuc_vc_fn_prefix :: Common.Voice_Name -> String
esmuc_vc_fn_prefix = takeWhile (/= '_') . drop 3

-- | sf = sound-file
esmuc_sf_names_rel :: Common.Voice_Name -> [FilePath]
esmuc_sf_names_rel vc =
  let f i = printf "%s/%s_%02d.wav" vc (esmuc_vc_fn_prefix vc) i
  in map f [1 .. esmuc_degree]

-- > putStrLn $ unlines $ concat $ map esmuc_sf_names esmuc_vc
esmuc_sf_names :: Common.Voice_Name -> [FilePath]
esmuc_sf_names = map esmuc_file . esmuc_sf_names_rel

esmuc_load_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
esmuc_load_msg b0 vc = Common.au_loader [0, 1] (esmuc_sf_names vc) b0

esmuc_load :: Buffer_Id -> Common.Voice_Name -> IO ()
esmuc_load b0 = withSc3 . mapM_ async . esmuc_load_msg b0

-- > mapM esmuc_lp_read esmuc_vc
esmuc_lp_read :: Common.Voice_Name -> IO (Common.Voice_Name, [(FilePath, Common.LoopPoints)])
esmuc_lp_read vc = fmap ((,) vc) (ca_lp_read esmuc_file (esmuc_sf_names_rel vc))

esmuc_vc_lp :: Common.Voice_Name -> [Common.LoopPoints]
esmuc_vc_lp vc = map snd (T.lookup_err vc Casacota.Data.esmuc_lp)

esmuc_load_lp_msg :: Ca_Ch -> Buffer_Id -> Common.Voice_Name -> [Message]
esmuc_load_lp_msg ch b0 vc = Common.au_loader_lp (ca_ch ch) (esmuc_sf_names vc) b0 (map Just (esmuc_vc_lp vc))

esmuc_load_lp :: Ca_Ch -> Buffer_Id -> Common.Voice_Name -> IO ()
esmuc_load_lp ch b0 = withSc3 . mapM_ async . esmuc_load_lp_msg ch b0

esmuc_sfz :: Common.Voice_Name -> [Sfz_Section]
esmuc_sfz vc =
  Common.sfz_grp_rT 0.1
    : Common.sfz_region_1_seq (esmuc_sf_names_rel vc) esmuc_gamut (Just (esmuc_vc_lp vc))

esmuc_sfz_wr :: Common.Voice_Name -> IO ()
esmuc_sfz_wr vc = sfz_write_sections False (esmuc_dir </> vc <.> "sfz") (esmuc_sfz vc)

-- * Esmuc-Pedal

esmuc_pedal_nm :: [(Common.Voice_Name, String)]
esmuc_pedal_nm = [("pedal_16", "subbaix"), ("pedal_8", "baix")]

esmuc_pedal_vc :: [Common.Voice_Name]
esmuc_pedal_vc = map fst esmuc_pedal_nm

-- | Transform voice name into file-name prefix
esmuc_pedal_vc_fn_prefix :: Common.Voice_Name -> String
esmuc_pedal_vc_fn_prefix vc = T.lookup_err vc esmuc_pedal_nm

-- | sf = sound-file
esmuc_pedal_sf_names_rel :: Common.Voice_Name -> [FilePath]
esmuc_pedal_sf_names_rel vc =
  let f i = printf "%s/%s_%02d.wav" vc (esmuc_pedal_vc_fn_prefix vc) i
  in map f [1 .. esmuc_pedal_degree]

esmuc_pedal_sf_names :: Common.Voice_Name -> [FilePath]
esmuc_pedal_sf_names = map esmuc_file . esmuc_pedal_sf_names_rel

-- | C2 - F4
esmuc_pedal_range :: (Key, Key)
esmuc_pedal_range = Common.range_octpc_to_midi ((2, 0), (4, 5))

esmuc_pedal_akt_f :: Common.Akt_f
esmuc_pedal_akt_f = Common.akt_f_rng esmuc_pedal_range

esmuc_pedal_degree :: Int
esmuc_pedal_degree = Common.range_to_degree esmuc_pedal_range

esmuc_pedal_gamut :: [Key]
esmuc_pedal_gamut = Common.range_to_gamut esmuc_pedal_range

esmuc_pedal_load_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
esmuc_pedal_load_msg b0 vc = Common.au_loader [0, 1] (esmuc_pedal_sf_names vc) b0

esmuc_pedal_load :: Buffer_Id -> Common.Voice_Name -> IO ()
esmuc_pedal_load b0 = withSc3 . mapM_ async . esmuc_pedal_load_msg b0

-- > mapM esmuc_pedal_lp_read esmuc_pedal_vc
esmuc_pedal_lp_read :: Common.Voice_Name -> IO (Common.Voice_Name, [(FilePath, Common.LoopPoints)])
esmuc_pedal_lp_read vc = fmap ((,) vc) (ca_lp_read esmuc_file (esmuc_pedal_sf_names_rel vc))

esmuc_pedal_vc_lp :: Common.Voice_Name -> [Common.LoopPoints]
esmuc_pedal_vc_lp vc = map snd (T.lookup_err vc Casacota.Data.esmuc_pedal_lp)

esmuc_pedal_load_lp_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
esmuc_pedal_load_lp_msg b0 vc = Common.au_loader_lp [0, 1] (esmuc_pedal_sf_names vc) b0 (map Just (esmuc_pedal_vc_lp vc))

esmuc_pedal_load_lp :: Buffer_Id -> Common.Voice_Name -> IO ()
esmuc_pedal_load_lp b0 = withSc3 . mapM_ async . esmuc_pedal_load_lp_msg b0

esmuc_pedal_sfz :: Common.Voice_Name -> [Sfz_Section]
esmuc_pedal_sfz vc =
  Common.sfz_grp_rT 0.05
    : Common.sfz_region_1_seq (esmuc_pedal_sf_names_rel vc) esmuc_gamut (Just (esmuc_pedal_vc_lp vc))

esmuc_pedal_sfz_wr :: Common.Voice_Name -> IO ()
esmuc_pedal_sfz_wr vc = sfz_write_sections False (esmuc_dir </> vc <.> "sfz") (esmuc_pedal_sfz vc)
