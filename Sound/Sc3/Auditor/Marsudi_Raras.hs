-- | <http://www.marsudiraras.org/gamelan>
module Sound.Sc3.Auditor.Marsudi_Raras where

import Data.Char {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import System.FilePath {- filepath -}

import qualified Music.Theory.List as List {- hmt-base -}
import qualified Music.Theory.Maybe as Maybe {- hmt-base -}

import qualified Music.Theory.Gamelan as G {- hmt -}

-- * M

type M_Instrument_Name = String
type M_Scale = String
type M_Pitch = String
type M_Note = (M_Scale, M_Pitch)
type M_Annotation = () -- String

{- | Find first name that has indicated prefix.

> let s = nub (mapMaybe m_instrument_name marsudi_raras_set_nm)
> in sort s == sort marsudi_raras_instrument_names
-}
m_instrument_name :: String -> Maybe M_Instrument_Name
m_instrument_name nm = find (`isPrefixOf` nm) marsudi_raras_instrument_names

m_instrument_name_err :: String -> M_Instrument_Name
m_instrument_name_err = fromMaybe (error "instrument") . m_instrument_name

-- > mapMaybe m_scale marsudi_raras_set_nm
m_scale :: String -> Maybe M_Scale
m_scale nm =
  let i = m_instrument_name_err nm
      k = length i
  in case drop k nm of
      'P' : 'l' : _ -> Just "Pl"
      'S' : 'l' : _ -> Just "Sl"
      _ -> Nothing

m_scale_to_scale :: M_Scale -> G.Scale
m_scale_to_scale m =
  case m of
    "Pl" -> G.Pelog
    "Sl" -> G.Slendro
    _ -> error "m_scale_scale"

scale_to_m_scale :: G.Scale -> M_Scale
scale_to_m_scale s =
  case s of
    G.Pelog -> "Pl"
    G.Slendro -> "Sl"

-- > (nub . sort) (map m_tone marsudi_raras_set_nm)
m_tone :: String -> M_Pitch
m_tone nm =
  case dropWhile (not . isDigit) nm of
    d : 'h' : 'h' : 'a' : 'r' : 'd' : _ -> d : "h"
    d : 'h' : 'a' : 'r' : 'd' : _ -> d : []
    d : 'h' : 'h' : _ -> d : "hh"
    d : 'h' : _ -> d : "h"
    d : 'l' : _ -> d : "l"
    d : _ -> [d]
    [] -> []

-- > m_tone_err "1hh" == "1hh"
m_tone_err :: String -> M_Pitch
m_tone_err nm =
  case m_tone nm of
    [] -> error (show ("m_tone_err", nm))
    t -> t

-- > m_tone_to_tone "1hh" == G.Pitch 2 1
m_tone_to_tone :: M_Pitch -> G.Pitch
m_tone_to_tone t =
  let f = fromIntegral . digitToInt
  in case t of
      d : [] -> G.Pitch 0 (f d)
      d : 'h' : 'h' : [] -> G.Pitch 2 (f d)
      d : 'h' : [] -> G.Pitch 1 (f d)
      d : 'l' : [] -> G.Pitch (-1) (f d)
      _ -> error "m_tone_to_tone"

tone_to_m_tone :: G.Pitch -> M_Pitch
tone_to_m_tone (G.Pitch o d) =
  let o' = case o of
        -1 -> "l"
        0 -> ""
        1 -> "h"
        2 -> "hh"
        _ -> error "tone_to_m_tone"
  in show d ++ o'

m_note_to_note :: M_Note -> G.Note
m_note_to_note (s, t) = G.Note (m_scale_to_scale s) (m_tone_to_tone t)

note_to_m_note :: G.Note -> M_Note
note_to_m_note (G.Note s t) = (scale_to_m_scale s, tone_to_m_tone t)

-- * Marsudi_raras

type Marsudi_raras t = (M_Instrument_Name, Maybe M_Note, t)

instrument_name_tbl :: [(G.Instrument_Name, M_Instrument_Name)]
instrument_name_tbl =
  [ (G.Bonang_Barung, "BonangBarung")
  , (G.Bonang_Panerus, "BonangPanerus")
  , (G.Gender_Panembung, "Slentem")
  , (G.Gong_Ageng, "GongAgeng")
  , (G.Gong_Suwukan, "GongSuwukan")
  , (G.Kempul, "Kempul")
  , (G.Kempyang, "Kempyang")
  , (G.Kenong, "Kenong")
  , (G.Ketuk, "Ketuk")
  , (G.Saron_Barung, "Saron")
  , (G.Saron_Demung, "Demung")
  , (G.Saron_Panerus, "Peking")
  ]

marsudi_raras_instrument_names :: [M_Instrument_Name]
marsudi_raras_instrument_names = map snd instrument_name_tbl

-- > map to_instrument_name marsudi_raras_instrument_names
to_instrument_name :: M_Instrument_Name -> Maybe G.Instrument_Name
to_instrument_name nm = List.reverse_lookup nm instrument_name_tbl

-- > map from_instrument_name [minBound .. maxBound]
from_instrument_name :: G.Instrument_Name -> Maybe M_Instrument_Name
from_instrument_name nm = lookup nm instrument_name_tbl

from_instrument_name_err :: G.Instrument_Name -> M_Instrument_Name
from_instrument_name_err =
  Maybe.from_just "from_instrument_name"
    . from_instrument_name

marsudi_raras_scales :: [M_Scale]
marsudi_raras_scales = ["Pl", "Sl"]

{- | The ordering here (longer names precede shorter names with equal
prefixes) is required by the 'tone' function below.
-}
marsudi_raras_pitch_set :: [M_Pitch]
marsudi_raras_pitch_set =
  let t = map show [1 :: Integer .. 7]
  in map (++ "l") t ++ map (++ "hh") t ++ map (++ "h") t ++ t

-- > marsudi_raras "BonangPanerusSl1hh" == ("BonangPanerus",Just ("Sl","1hh"),"")
-- > map (marsudi_raras id) marsudi_raras_set_variants_nm
marsudi_raras :: (String -> t) -> String -> Marsudi_raras t
marsudi_raras ann_f nm =
  let i = m_instrument_name_err nm
      (p, st) = case m_scale nm of
        Nothing -> (i, Nothing)
        Just s ->
          let t = m_tone_err nm
          in (i ++ s ++ t, Just (s, t))
      a = ann_f (fromMaybe (error "marsudi_raras") (stripPrefix p nm))
  in (i, st, a)

-- > map marsudi_raras_plain marsudi_raras_set_nm
marsudi_raras_plain :: String -> Marsudi_raras ()
marsudi_raras_plain = marsudi_raras (\x -> if null x then () else (error "marsudi_raras_plain?"))

type TONE = G.Tone () -- G.Annotation

-- > map (marsudi_raras_to_tone . marsudi_raras_plain) marsudi_raras_set_nm
marsudi_raras_to_tone :: Marsudi_raras () -> G.Tone ()
marsudi_raras_to_tone (i, n, ()) =
  let i' = fromJust (to_instrument_name i)
      n' = fmap m_note_to_note n
      nm = marsudi_raras_nm (i', n', Nothing)
      f = lookup nm marsudi_raras_nm_fr
  in G.Tone i' n' f Nothing

-- > map tone_marsudi_raras_nm marsudi_raras_tone_set == marsudi_raras_set_nm
tone_marsudi_raras_nm :: TONE -> String
tone_marsudi_raras_nm (G.Tone i n _ a) = marsudi_raras_nm (i, n, a)

-- * Tone set

type TONE_SET = G.Tone_Set () -- G.Annotation

-- > sort (mapMaybe G.tone_frequency marsudi_raras_tone_set)
--
-- > let ts = G.tone_set_instrument marsudi_raras_tone_set (G.Bonang_Barung,Just G.Slendro)
-- > G.tone_set_gamut ts
marsudi_raras_tone_set :: G.Tone_Set ()
marsudi_raras_tone_set =
  let f = map (marsudi_raras_to_tone . marsudi_raras_plain)
  in f marsudi_raras_set_nm

-- | Lookup /equivalent/ tone in 'marsudi_raras_tone_set'.
marsudi_raras_equivalent :: G.Tone () -> Maybe (G.Tone ())
marsudi_raras_equivalent t = find (G.tone_equivalent t) marsudi_raras_tone_set

{-
marsudi_raras_tone_set_variants :: TONE_SET
marsudi_raras_tone_set_variants =
    let f = map (marsudi_raras_to_tone . marsudi_raras id)
    in f marsudi_raras_set_variants_nm
-}

-- > length marsudi_raras_instruments == 20
-- > mapMaybe G.instrument_frequencies marsudi_raras_instruments
marsudi_raras_instruments :: [G.Instrument]
marsudi_raras_instruments = G.instruments marsudi_raras_tone_set

-- > fmap G.instrument_gamut (marsudi_raras_instrument G.Bonang_Barung)
marsudi_raras_instrument :: G.Instrument_Name -> Maybe G.Instrument
marsudi_raras_instrument nm =
  let f = (== nm) . G.instrument_name
  in find f marsudi_raras_instruments

marsudi_raras_nm :: (G.Instrument_Name, Maybe G.Note, Maybe ()) -> String
marsudi_raras_nm (i, n, _) =
  let f (p, q) = p ++ q
  in from_instrument_name_err i ++ maybe "" (f . note_to_m_note) n

{- | Files are renamed as indicated (see @sh/marsudi_raras-ln.sh@).

> import Music.Theory.Math as T
> import Music.Theory.Pitch as T
> let mnn = sort . map (T.cps_to_fmidi . snd) $ marsudi_raras_nm_fr
> unwords . map (T.double_pp 2) $ mnn
> L.tbl_write_au (L.prj_file "eof/au/tbl/04.tbl.au") mnn
-}
marsudi_raras_nm_fr :: [(String, Double)]
marsudi_raras_nm_fr =
  [ ("BonangBarungPl1l", 292.89)
  , ("BonangBarungPl1", 592.82)
  , ("BonangBarungPl2l", 317.02)
  , ("BonangBarungPl2", 641.65)
  , ("BonangBarungPl3l", 347.87)
  , ("BonangBarungPl3", 697.03)
  , ("BonangBarungPl4l", 406.62)
  , ("BonangBarungPl4", 826.00)
  , ("BonangBarungPl5l", 434.70)
  , ("BonangBarungPl5", 884.35)
  , ("BonangBarungPl6l", 461.16)
  , ("BonangBarungPl6", 940.27)
  , ("BonangBarungPl7l", 522.27)
  , ("BonangBarungPl7", 1057.93)
  , ("BonangBarungSl1h", 1084.94)
  , ("BonangBarungSl1l", 264.29)
  , ("BonangBarungSl1", 535.46)
  , ("BonangBarungSl2h", 1240.54)
  , ("BonangBarungSl2l", 300.54)
  , ("BonangBarungSl2", 619.76)
  , ("BonangBarungSl3l", 352.27)
  , ("BonangBarungSl3", 709.77)
  , ("BonangBarungSl5l", 409.00)
  , ("BonangBarungSl5", 820.96)
  , ("BonangBarungSl6l", 470.23)
  , ("BonangBarungSl6", 937.43)
  , ("BonangPanerusPl1h", 1220.21)
  , ("BonangPanerusPl1", 595.23)
  , ("BonangPanerusPl2h", 1315.73)
  , ("BonangPanerusPl2", 642.50)
  , ("BonangPanerusPl3h", 1415.26)
  , ("BonangPanerusPl3", 696.63)
  , ("BonangPanerusPl4h", 1652.21)
  , ("BonangPanerusPl4", 825.14)
  , ("BonangPanerusPl5h", 1761.08)
  , ("BonangPanerusPl5", 883.82)
  , ("BonangPanerusPl6h", 1862.46)
  , ("BonangPanerusPl6", 940.45)
  , ("BonangPanerusPl7h", 2118.70)
  , ("BonangPanerusPl7", 1060.53)
  , ("BonangPanerusSl1hh", 2130.34)
  , ("BonangPanerusSl1h", 1086.60)
  , ("BonangPanerusSl1", 536.23)
  , ("BonangPanerusSl2hh", 2492.29)
  , ("BonangPanerusSl2h", 1243.65)
  , ("BonangPanerusSl2", 617.14)
  , ("BonangPanerusSl3h", 1423.75)
  , ("BonangPanerusSl3", 706.69)
  , ("BonangPanerusSl5h", 1626.63)
  , ("BonangPanerusSl5", 819.16)
  , ("BonangPanerusSl6h", 1862.06)
  , ("BonangPanerusSl6", 940.63)
  , ("DemungPl1", 293.36)
  , ("DemungPl2", 315.96)
  , ("DemungPl3", 345.34)
  , ("DemungPl4", 404.14)
  , ("DemungPl5", 435.32)
  , ("DemungPl6", 464.17)
  , ("DemungPl7", 520.42)
  , ("DemungSl1h", 535.67)
  , ("DemungSl1", 264.24)
  , ("DemungSl2", 302.37)
  , ("DemungSl3", 351.58)
  , ("DemungSl5", 402.80)
  , ("DemungSl6l", 230.68)
  , ("DemungSl6", 463.24)
  , ("GongAgeng", 44.51)
  , ("GongSuwukan", 76.34)
  , ("KempulPl4", 103.33)
  , ("KempulPl5", 109.09)
  , ("KempulPl6", 117.54)
  , ("KempulSl1l", 67.15)
  , ("KempulSl1", 132.22)
  , ("KempulSl2", 150.91)
  , ("KempulSl3l", 87.49) -- l
  , ("KempulSl5l", 103.33) -- l
  , ("KempulSl6l", 117.54) -- l
  , ("KempyangPl6", 1048.59)
  , ("KempyangSl6", 939.65)
  , ("KenongPl1h", 592.96) -- h
  , ("KenongPl2", 321.92)
  , ("KenongPl3", 343.72)
  , ("KenongPl4", 403.04)
  , ("KenongPl5", 435.36)
  , ("KenongPl6", 463.61)
  , ("KenongPl7", 522.25)
  , ("KenongSl1h", 535.41) -- h
  , ("KenongSl2", 300.96)
  , ("KenongSl3", 351.99)
  , ("KenongSl5", 403.04)
  , ("KenongSl6", 463.61)
  , ("KetukSl2", 301.60)
  , ("KetukSl6l", 230.89) -- l
  , ("PekingPl1", 1209.27)
  , ("PekingPl2", 1300.49)
  , ("PekingPl3", 1431.20)
  , ("PekingPl4", 1657.96)
  , ("PekingPl5", 1838.96)
  , ("PekingPl6", 1950.75)
  , ("PekingPl7", 2158.92)
  , ("SaronPl1", 593.51)
  , ("SaronPl2", 640.29)
  , ("SaronPl3", 697.11)
  , ("SaronPl4", 821.13)
  , ("SaronPl5", 883.18)
  , ("SaronPl6", 939.82)
  , ("SaronPl7", 1058.41)
  , ("SaronSl1h", 1084.91)
  , ("SaronSl1", 535.21)
  , ("SaronSl2", 614.95)
  , ("SaronSl3", 710.40)
  , ("SaronSl5", 820.12)
  , ("SaronSl6l", 462.78)
  , ("SaronSl6", 938.41)
  , ("SlentemPl1", 144.92)
  , ("SlentemPl2", 158.89)
  , ("SlentemPl3", 171.90)
  , ("SlentemPl4", 200.31)
  , ("SlentemPl5", 217.29)
  , ("SlentemPl6", 231.46)
  , ("SlentemPl7", 259.65)
  , ("SlentemSl1h", 264.53)
  , ("SlentemSl1", 129.42)
  , ("SlentemSl2", 151.09)
  , ("SlentemSl3", 174.74)
  , ("SlentemSl5", 199.57)
  , ("SlentemSl6l", 115.02)
  , ("SlentemSl6", 231.09)
  ]

marsudi_raras_set_nm :: [String]
marsudi_raras_set_nm = map fst marsudi_raras_nm_fr

marsudi_raras_set_fr :: [Double]
marsudi_raras_set_fr = map snd marsudi_raras_nm_fr

-- * Sc3

-- > length (sf_set marsudi_raras_tone_set "wav" 0) == 128
sf_set :: TONE_SET -> String -> Int -> [(TONE, FilePath, Double, Int)]
sf_set g x n =
  let f nm = nm <.> x
  in zip4 g (map (f . tone_marsudi_raras_nm) g) (repeat 1) [n ..]

-- * Variants

-- > let s = marsudi_raras_set_nm ++ marsudi_raras_set_variants_nm
-- > in s == nub s
--
-- > unwords (map (\i -> i <.> "wav") marsudi_raras_set_variants_nm)
marsudi_raras_set_variants_nm :: [String]
marsudi_raras_set_variants_nm =
  [ "BonangBarungPl1lsoft"
  , "BonangBarungPl1soft"
  , "BonangBarungPl2lsoft"
  , "BonangBarungPl2soft"
  , "BonangBarungPl3lsoft"
  , "BonangBarungPl3soft"
  , "BonangBarungPl4lsoft"
  , "BonangBarungPl4soft"
  , "BonangBarungPl5lsoft"
  , "BonangBarungPl5soft"
  , "BonangBarungPl6lsoft"
  , "BonangBarungPl6soft"
  , "BonangBarungPl7lsoft"
  , "BonangBarungPl7soft"
  , "BonangBarungSl1hsoft"
  , "BonangBarungSl1la"
  , "BonangBarungSl1softer"
  , "BonangBarungSl1soft"
  , "BonangBarungSl2hsoft"
  , "BonangBarungSl2lsoft"
  , "BonangBarungSl2soft"
  , "BonangBarungSl3lsoft"
  , "BonangBarungSl3soft"
  , "BonangBarungSl5soft"
  , "BonangBarungSl6a"
  , "BonangBarungSl6lsoft"
  , "BonangPanerusPl1a"
  , "BonangPanerusPl1hsoft"
  , "BonangPanerusPl1soft"
  , "BonangPanerusPl2hsoft"
  , "BonangPanerusPl2soft"
  , "BonangPanerusPl3ha"
  , "BonangPanerusPl3soft"
  , "BonangPanerusPl4a"
  , "BonangPanerusPl4hsoft"
  , "BonangPanerusPl5hsoft"
  , "BonangPanerusPl5soft"
  , "BonangPanerusPl6hsoft"
  , "BonangPanerusPl6soft"
  , "BonangPanerusPl7a"
  , "BonangPanerusSl1a"
  , "BonangPanerusSl1hhhard"
  , "BonangPanerusSl1hhsoft"
  , "BonangPanerusSl1hsoft"
  , "BonangPanerusSl2a"
  , "BonangPanerusSl2hha"
  , "BonangPanerusSl2hsoft"
  , "BonangPanerusSl3a"
  , "BonangPanerusSl3hsoft"
  , "BonangPanerusSl5a"
  , "BonangPanerusSl5hsoft"
  , "BonangPanerusSl6hharder"
  , "BonangPanerusSl6hhard"
  , "BonangPanerusSl6hsoft"
  , "BonangPanerusSl6soft"
  , "DemungPl1soft"
  , "DemungPl2hard"
  , "DemungPl2soft"
  , "DemungPl3a"
  , "DemungPl4a"
  , "DemungPl5a"
  , "DemungPl6a"
  , "DemungPl7hard"
  , "DemungPl7soft"
  , "DemungSl1hard"
  , "DemungSl1hhard"
  , "DemungSl1hsoft"
  , "DemungSl1soft"
  , "DemungSl2hard"
  , "DemungSl3hard"
  , "DemungSl3soft"
  , "DemungSl5hard"
  , "DemungSl5soft"
  , "DemungSl6lhard"
  , "DemungSl6lsoft"
  , "DemungSl6soft"
  , "GongAgenga"
  , "GongAgengb"
  , "KempulPl4a"
  , "KempulPl5a"
  , "KempulPl5hard"
  , "KempulPl6hard"
  , "KempulSl1a"
  , "KempulSl1lhard"
  , "KempulSl2soft"
  , "KempulSl3a"
  , "KempulSl5a"
  , "KempulSl6hard"
  , "KempyangPl6soft"
  , "KempyangSl6soft"
  , "Kempyang"
  , "KenongPl1soft"
  , "KenongPl2soft"
  , "KenongPl3a"
  , "KenongPl4a"
  , "KenongPl5soft"
  , "KenongPl6a"
  , "KenongPl7soft"
  , "KenongSl1soft"
  , "KenongSl2a"
  , "KenongSl2b"
  , "KenongSl3a"
  , "KenongSl5a"
  , "KenongSl6a"
  , "KenongSl6soft"
  , "KetukSl2a"
  , "KetukSl6hard"
  , "PekingPl1hard"
  , "PekingPl2hard"
  , "PekingPl3hard"
  , "PekingPl4hard"
  , "PekingPl5hard"
  , "PekingPl6hard"
  , "PekingPl7hard"
  , "PekingPl7soft"
  , "SaronPl1hard"
  , "SaronPl2hard"
  , "SaronPl3hard"
  , "SaronPl4hard"
  , "SaronPl5soft"
  , "SaronPl6soft"
  , "SaronPl7hard"
  , "SaronPl7soft"
  , "SaronSl1hard"
  , "SaronSl1hhard"
  , "SaronSl1hsoft"
  , "SaronSl1soft"
  , "SaronSl2a"
  , "SaronSl2wrong1"
  , "SaronSl2wrong"
  , "SaronSl3soft1"
  , "SaronSl3soft"
  , "SaronSl5wrong1"
  , "SaronSl5wrong"
  , "SaronSl6lhard"
  , "SaronSl6lsoft"
  , "SaronSl6soft"
  , "SlentemPl1hard"
  , "SlentemSl1hard"
  , "SlentemSl1hsoft"
  , "SlentemSl2hard"
  , "SlentemSl3hard"
  , "SlentemSl5hard"
  , "SlentemSl6hard"
  , "SlentemSl6lhard"
  ]
