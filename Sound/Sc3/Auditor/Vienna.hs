module Sound.Sc3.Auditor.Vienna where

import Data.Function {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import System.FilePath {- filepath -}

import qualified Music.Theory.List as List {- hmt-base -}
import qualified Music.Theory.Pitch as Pitch {- hmt -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import qualified Sound.Sc3.Auditor.Common as Common

-- * Vienna

vienna_dir :: FilePath
vienna_dir = "/home/rohan/data/audio/instr/vienna/"

vienna_nc :: Int
vienna_nc = 2

type V_Technique = [String]
type V_Dynamic = String
type V_Note = String

-- | (#,[technique],dynamic,note-name)
type VDescr = (Int, V_Technique, V_Note)

vdescr_id :: VDescr -> Int
vdescr_id (n, _, _) = n

vdescr_technique :: VDescr -> V_Technique
vdescr_technique (_, tec, _) = tec

vdescr_note_name :: VDescr -> V_Note
vdescr_note_name (_, _, nnm) = nnm

vdescr_dynamic :: VDescr -> V_Dynamic
vdescr_dynamic (_, tec, _) = last tec

vdescr_pitch :: VDescr -> Pitch.Pitch
vdescr_pitch = fromMaybe (error "vdescr_pitch") . Pitch.parse_iso_pitch . vdescr_note_name

vdescr_midi :: VDescr -> Pitch.Midi
vdescr_midi = Pitch.pitch_to_midi . vdescr_pitch

vdescr_match :: V_Technique -> VDescr -> Bool
vdescr_match tec' (_n, tec, _nnm) = tec' == tec

vdescr_group :: String -> VDescr -> String
vdescr_group ty (_n, tec, _nnm) =
  let tec' = intercalate "_" tec
  in ty ++ "_" ++ tec'

vdescr_to_filename :: String -> VDescr -> FilePath
vdescr_to_filename ty v =
  let (n, _tec, nnm) = v
  in concat [show n, "_", vdescr_group ty v, "_", nnm ++ ".wav"]

type VDescr_Meta = ([Int], [String], String)

vdescr_gen :: VDescr_Meta -> [VDescr]
vdescr_gen (n, a, p) =
  if Common.is_gamut n && length n == length (words p)
    then zip3 n (repeat a) (words p)
    else error "vdescr_gen?"

vdescr_to_meta :: [VDescr] -> [VDescr_Meta]
vdescr_to_meta sq =
  let f x =
        let k = map vdescr_id x
            a = vdescr_technique (List.head_err x)
            n = unwords (map vdescr_note_name x)
        in if Common.is_gamut k && length k /= length n
            then error "vdescr_to_meta?"
            else (k, a, n)
  in map f (groupBy ((==) `on` vdescr_technique) sq)

-- | [(Oct,[Pc])] -> [Mnn]
vn_mnn_seq_f :: [(Int, [Int])] -> [Pitch.Midi]
vn_mnn_seq_f dat =
  let f (i, j) = zip j (repeat i)
  in map Pitch.octpc_to_midi (concatMap f dat)

-- * Strings/Vi

vi_dir :: FilePath
vi_dir = vienna_dir </> "Strings/VI/"

vi_file :: FilePath -> FilePath
vi_file = (</>) vi_dir

vi_group :: VDescr -> String
vi_group = vdescr_group "VI-14"

vi_to_filename :: VDescr -> FilePath
vi_to_filename = vdescr_to_filename "VI-14"

vi_sample_set :: V_Technique -> [(Pitch.Midi, FilePath)]
vi_sample_set sel =
  let f d = (vdescr_midi d, vi_to_filename d)
  in sort (map f (filter (vdescr_match sel) vi_set))

vi_mV_sus_mf_sample_set :: [(Pitch.Midi, FilePath)]
vi_mV_sus_mf_sample_set = vi_sample_set (["mV", "sus", "mf"])

vi_mV_sus_mf_range :: Num n => (n, n)
vi_mV_sus_mf_range = (56, 96)

vi_mV_sus_mf_stored :: [Pitch.Midi]
vi_mV_sus_mf_stored = map fst vi_mV_sus_mf_sample_set

vi_mV_sus_mf_akt_f :: Common.Akt_f
vi_mV_sus_mf_akt_f = Common.akt_f_simple (zip vi_mV_sus_mf_stored [0 ..])

vi_mV_sus_mf_load_msg :: Buffer_Id -> [Message]
vi_mV_sus_mf_load_msg = Common.au_loader [0, 1] (map (vi_file . snd) vi_mV_sus_mf_sample_set)

vi_mV_sus_mf_load :: Buffer_Id -> IO ()
vi_mV_sus_mf_load = withSc3 . mapM_ async . vi_mV_sus_mf_load_msg

vi_meta :: [VDescr_Meta]
vi_meta =
  [
    ( [1 .. 21]
    , ["stac", "ff1"]
    , "A4 A5 A6 B3 B4 B5 B6 C#5 C#6 C4 C7 D#4 D#5 D#6 F4 F5 F6 G#3 G4 G5 G6"
    )
  ,
    ( [22 .. 43]
    , ["stac", "mf1"]
    , "A3 A4 A5 A6 B3 B4 B5 B6 C#4 C#5 C#6 C7 D#4 D#5 D#6 F4 F5 F6 G#3 G5 G6 G4"
    )
  ,
    ( [44 .. 65]
    , ["mV", "sus", "mf"]
    , "A3 A4 A5 A6 B3 B4 B5 B6 C#4 C#5 C#6 C7 D#4 D#5 D#6 F4 F5 F6 G#3 G4 G5 G6"
    )
  ,
    ( [66 .. 88]
    , ["sV", "espr", "4s", "ff"]
    , "A#3 A#4 A#5 A#6 C#7 C4 C5 C6 C7 D4 D5 D6 E4 E5 E6 F#4 F#5 F#6 G#3 G#4 G#5 G#6 G3"
    )
  ,
    ( [89 .. 110]
    , ["trem", "sus", "ff"]
    , "A#3 A#4 A#5 A#6 C4 C5 C6 C7 D4 D5 D6 E4 E5 E6 F#4 F#5 F#6 G#3 G#4 G#5 G#6 G3"
    )
  ,
    ( [111 .. 132]
    , ["trem", "sus", "mf"]
    , "A#3 A#4 A#5 A#6 C4 C5 C6 C7 D4 D5 D6 E4 E5 E6 F#4 F#5 F#6 G#3 G#4 G#5 G#6 G3"
    )
  ,
    ( [133 .. 153]
    , ["mV", "4-120", "f1"]
    , "A#3 A#4 A#5 A#6 C4 C5 C6 C7 D4 D5 D6 E4 E5 E6 F#4 F#5 F#6 G#3 G#4 G#5 G#6"
    )
  ,
    ( [154 .. 174]
    , ["mV", "4-120", "mp1"]
    , "A#3 A#4 A#5 A#6 C4 C5 C6 C7 D4 D5 D6 E4 E5 E6 F#4 F#5 F#6 G#3 G#4 G#5 G#6"
    )
  ,
    ( [175 .. 196]
    , ["8-120", "f1"]
    , "A3 A4 A5 A6 B3 B4 B5 B6 C#4 C#5 C#6 C7 D#4 D#5 D#6 F4 F5 F6 G3 G4 G5 G6"
    )
  ,
    ( [197 .. 218]
    , ["8-120", "p1"]
    , "A3 A4 A5 A6 B3 B4 B5 B6 C#4 C#5 C#6 C7 D#4 D#5 D#6 F4 F5 F6 G3 G4 G5 G6"
    )
  ,
    ( [219 .. 239]
    , ["mV", "sforz", "sfz1"]
    , "A#3 A#4 A#5 A#6 C4 C5 C6 C7 D4 D5 D6 E4 E5 E6 F#4 F#5 F#6 G#3 G#4 G#5 G#6"
    )
  ,
    ( [240 .. 260]
    , ["mV", "pfp", "4s", "mf-f"]
    , "A#3 A#4 A#5 A#6 C4 C5 C6 C7 D4 D5 D6 E4 E5 E6 F#4 F#5 F#6 G#3 G#4 G#5 G#6"
    )
  ,
    ( [261 .. 281]
    , ["mV", "pfp", "4s", "p-mf"]
    , "A#3 A#4 A#5 A#6 C4 C5 C6 C7 D4 D5 D6 E4 E5 E6 F#4 F#5 F#6 G#3 G#4 G#5 G#6"
    )
  ,
    ( [282 .. 303]
    , ["pz", "mV", "ff1"]
    , "C7 G3 A#3 A#4 A#5 A#6 C4 C5 C6 D4 D5 D6 E4 E5 E6 F#4 F#5 F#6 G#3 G#4 G#5 G#6"
    )
  ,
    ( [304 .. 325]
    , ["pz", "mV", "mf1"]
    , "A#3 A#4 A#5 A#6 C4 C5 C6 C7 D4 D5 D6 E4 E5 E6 F#4 F#5 F#6 G#3 G#4 G#5 G#6 G3"
    )
  ]

-- > nub (sort (map vi_group vi_set))
-- > vi_to_filename (vi_set !! 0) == "1_VI-14_stac_ff1_A4.wav"
-- > map vdescr_midi vi_set
vi_set :: [VDescr]
vi_set = concatMap vdescr_gen vi_meta

-- * Strings/VA

va_dir :: FilePath
va_dir = vienna_dir </> "Strings/VA/"

va_file :: FilePath -> FilePath
va_file = (</>) va_dir

va_group :: VDescr -> String
va_group = vdescr_group "VA-10"

va_to_filename :: VDescr -> FilePath
va_to_filename = vdescr_to_filename "VA-10"

va_sample_set :: V_Technique -> [(Pitch.Midi, FilePath)]
va_sample_set sel =
  let f d = (vdescr_midi d, va_to_filename d)
  in sort (map f (filter (vdescr_match sel) va_set))

va_mV_sus_mf_sample_set :: [(Pitch.Midi, FilePath)]
va_mV_sus_mf_sample_set = va_sample_set (["mV", "sus", "mf"])

va_mV_sus_mf_range :: Num n => (n, n)
va_mV_sus_mf_range = (49, 85)

va_mV_sus_mf_stored :: [Pitch.Midi]
va_mV_sus_mf_stored = map fst va_mV_sus_mf_sample_set

va_mV_sus_mf_akt_f :: Common.Akt_f
va_mV_sus_mf_akt_f = Common.akt_f_simple (zip va_mV_sus_mf_stored [0 ..])

va_mV_sus_mf_osc :: (Buffer_Id, String) -> Message
va_mV_sus_mf_osc (b, fn) = b_allocReadChannel b fn 0 0 [0, 1]

va_mV_sus_mf_load_msg :: Buffer_Id -> [Message]
va_mV_sus_mf_load_msg = Common.au_loader [0, 1] (map (va_file . snd) va_mV_sus_mf_sample_set)

va_mV_sus_mf_load :: Buffer_Id -> IO ()
va_mV_sus_mf_load = withSc3 . mapM_ async . va_mV_sus_mf_load_msg

va_meta :: [VDescr_Meta]
va_meta =
  [
    ( [1 .. 20]
    , ["mV", "4-120", "f1"]
    , "A#3 A#4 A#5 C#3 C#6 C4 C5 C6 D3 D4 D5 E3 E4 E5 F#3 F#4 F#5 G#3 G#4 G#5"
    )
  ,
    ( [21 .. 40]
    , ["mV", "4-120", "mp1"]
    , "A#3 A#4 A#5 C#3 C#6 C4 C5 C6 D3 D4 D5 E3 E4 E5 F#3 F#4 F#5 G#3 G#4 G#5"
    )
  ,
    ( [41 .. 59]
    , ["trem", "sus", "f"]
    , "A#3 A#4 A#5 C3 C4 C5 C6 D3 D4 D5 E3 E4 E5 F#3 F#4 F#5 G#3 G#4 G#5"
    )
  ,
    ( [60 .. 78]
    , ["trem", "sus", "p"]
    , "A#3 A#4 A#5 C3 C4 C5 C6 D3 D4 D5 E3 E4 E5 F#3 F#4 F#5 G#3 G#4 G#5"
    )
  ,
    ( [79 .. 97]
    , ["mV", "pfp", "6s", "mf"]
    , "A3 A4 A5 B3 B4 B5 C#3 C#4 C#5 C#6 D#3 D#4 D#5 F3 F4 F5 G3 G4 G5"
    )
  ,
    ( [98 .. 116]
    , ["mV", "pfp", "6s", "p"]
    , "A3 A4 A5 B3 B4 B5 C#3 C#4 C#5 C#6 D#3 D#4 D#5 F3 F4 F5 G3 G4 G5"
    )
  ,
    ( [117 .. 135]
    , ["mV", "sforz", "sfz"]
    , "A#3 A#4 A#5 C#3 C4 C5 C6 D3 D4 D5 E3 E4 E5 F#3 F#4 F#5 G#3 G#4 G#5"
    )
  ,
    ( [136 .. 154]
    , ["pz", "f1"]
    , "A#3 A#4 A#5 C3 C4 C5 C6 D3 D4 D5 E3 E4 E5 F#3 F#4 F#5 G#3 G#4 G#5"
    )
  ,
    ( [155 .. 173]
    , ["pz", "p1"]
    , "A#3 A#4 A#5 C3 C4 C5 C6 D3 D4 D5 E3 E4 E5 F#3 F#4 F#5 G#3 G#4 G#5"
    )
  ,
    ( [174 .. 193]
    , ["mV", "sus", "mf"]
    , "A#3 A#4 A#5 C#3 C#6 C4 C5 C6 D3 D4 D5 E3 E4 E5 F#3 F#4 F#5 G#3 G#4 G#5"
    )
  ,
    ( [194 .. 213]
    , ["sV", "espr", "4s", "ff"]
    , "A#3 A#4 A#5 C#3 C#6 C4 C5 C6 D3 D4 D5 E3 E4 E5 F#3 F#4 F#5 G#3 G#4 G#5"
    )
  ,
    ( [214 .. 233]
    , ["stac", "ff1"]
    , "A3 A4 A5 B3 B4 B5 C#3 C#4 C#5 C3 C6 D#3 D#4 D#5 F3 F4 F5 G3 G4 G5"
    )
  ,
    ( [234 .. 253]
    , ["stac", "mf1"]
    , "A3 A4 A5 B3 B4 B5 C#3 C#4 C#5 C3 C6 D#3 D#4 D#5 F3 F4 F5 G3 G4 G5"
    )
  ]

va_set :: [VDescr]
va_set = concatMap vdescr_gen va_meta

-- * Strings/VC

vc_dir :: FilePath
vc_dir = vienna_dir </> "Strings/VC/"

vc_file :: FilePath -> FilePath
vc_file = (</>) vc_dir

vc_group :: VDescr -> String
vc_group = vdescr_group "VC-8"

vc_to_filename :: VDescr -> FilePath
vc_to_filename = vdescr_to_filename "VC-8"

vc_sample_set :: V_Technique -> [(Pitch.Midi, FilePath)]
vc_sample_set sel =
  let f d = (vdescr_midi d, vc_to_filename d)
  in sort (map f (filter (vdescr_match sel) vc_set))

vc_mV_sus_mf_sample_set :: [(Pitch.Midi, FilePath)]
vc_mV_sus_mf_sample_set = vc_sample_set (["mV", "sus", "mf"])

vc_mV_sus_mf_range :: Num n => (n, n)
vc_mV_sus_mf_range = (37, 73)

vc_mV_sus_mf_stored :: [Pitch.Midi]
vc_mV_sus_mf_stored = map fst vc_mV_sus_mf_sample_set

vc_mV_sus_mf_akt_f :: Common.Akt_f
vc_mV_sus_mf_akt_f = Common.akt_f_simple (zip vc_mV_sus_mf_stored [0 ..])

vc_mV_sus_mf_osc :: (Buffer_Id, String) -> Message
vc_mV_sus_mf_osc (b, fn) = b_allocReadChannel b fn 0 0 [0, 1]

vc_mV_sus_mf_load_msg :: Buffer_Id -> [Message]
vc_mV_sus_mf_load_msg = Common.au_loader [0, 1] (map (vc_file . snd) vc_mV_sus_mf_sample_set)

vc_mV_sus_mf_load :: Buffer_Id -> IO ()
vc_mV_sus_mf_load = withSc3 . mapM_ async . vc_mV_sus_mf_load_msg

vc_meta :: [VDescr_Meta]
vc_meta =
  [
    ( [1 .. 20]
    , ["mV", "4-120", "f1"]
    , "A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C2 C5 D#2 D#3 D#4 F2 F3 F4 G2 G3 G4"
    )
  ,
    ( [21 .. 40]
    , ["mV", "sforz", "sfz"]
    , "A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C2 C5 D#2 D#3 D#4 F2 F3 F4 G2 G3 G4"
    )
  ,
    ( [41 .. 59]
    , ["mV", "pfp", "6s", "mf"]
    , "A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C#5 D#2 D#3 D#4 F2 F3 F4 G2 G3 G4"
    )
  ,
    ( [60 .. 78]
    , ["mV", "pfp", "6s", "p"]
    , "A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C#5 D#2 D#3 D#4 F2 F3 F4 G2 G3 G4"
    )
  ,
    ( [79 .. 98]
    , ["trem", "sus", "ff"]
    , "A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C2 C5 D#2 D#3 D#4 F2 F3 F4 G2 G3 G4"
    )
  ,
    ( [99 .. 118]
    , ["pz", "mV", "ff"]
    , "A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C2 C5 D#2 D#3 D#4 F2 F3 F4 G2 G3 G4"
    )
  ,
    ( [119 .. 138]
    , ["pz", "mV", "mf"]
    , "A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C2 C5 D#2 D#3 D#4 F2 F3 F4 G2 G3 G4"
    )
  ,
    ( [139 .. 158]
    , ["stac", "ff1"]
    , "A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C2 C5 D#2 D#3 D#4 F2 F3 F4 G2 G3 G4"
    )
  ,
    ( [159 .. 178]
    , ["stac", "mf1"]
    , "A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C2 C5 D#2 D#3 D#4 F2 F3 F4 G2 G3 G4"
    )
  ,
    ( [179 .. 198]
    , ["mV", "sus", "mf"]
    , "A#2 A#3 A#4 C#2 C#5 C3 C4 C5 D2 D3 D4 E2 E3 E4 F#2 F#3 F#4 G#2 G#3 G#4"
    )
  ,
    ( [199 .. 218]
    , ["sV", "espr", "4s", "ff"]
    , "A#2 A#3 A#4 C#2 C#5 C3 C4 C5 D2 D3 D4 E2 E3 E4 F#2 F#3 F#4 G#2 G#3 G#4"
    )
  ,
    ( [219 .. 238]
    , ["trem", "sus", "mf"]
    , "A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C2 C5 D#2 D#3 D#4 F2 F3 F4 G2 G3 G4"
    )
  ,
    ( [239 .. 255]
    , ["mV", "4-120", "f1"]
    , "A#2 A#3 A#4 C3 C4 D2 D3 D4 E2 E3 E4 F#2 F#3 F#4 G#2 G#3 G#4"
    )
  ,
    ( [256 .. 292]
    , ["mV", "4-120", "mf1"]
    , "A#2 A#3 A#4 A2 A3 A4 B2 B3 B4 C#2 C#3 C#4 C2 C3 C4 C5 D#2 D#3 D#4 D2 D3 D4 E2 E3 E4 F#2 F#3 F#4 F2 F3 F4 G#2 G#3 G#4 G2 G3 G4"
    )
  ]

vc_set :: [VDescr]
vc_set = concatMap vdescr_gen vc_meta

-- * HARP

-- HA = HARP ; RS = ; ES = ; bisb = ; mu = mute ; f,mf,mp,p=dynamic

ha_dir :: FilePath
ha_dir = "/home/rohan/data/audio/instr/vienna/Harp/HARP_1/"

ha_nc :: Num n => n
ha_nc = 2

-- * HARP / RS_bisb_mp

-- > length ha_rs_bisb_mp_mnn_seq == 38
ha_rs_bisb_mp_mnn_seq :: [Pitch.Midi]
ha_rs_bisb_mp_mnn_seq =
  let dat =
        [ (10, [3 .. 6])
        , (11, [2 .. 5])
        , (1, [3 .. 7])
        , (0, [3 .. 7])
        , (3, [3 .. 6])
        , (4, [3 .. 6])
        , (6, [3 .. 6])
        , (5, [3 .. 6])
        , (8, [3 .. 6])
        ]
  in vn_mnn_seq_f dat

ha_rs_bisb_mp_range :: (Pitch.Midi, Pitch.Midi)
ha_rs_bisb_mp_range = Common.seq_to_range ha_rs_bisb_mp_mnn_seq

ha_rs_bisb_mp_fn_seq :: [FilePath]
ha_rs_bisb_mp_fn_seq =
  let f n mnn = concat [show n, "_HA_RS_bisb_mp_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [731 :: Int .. 768] ha_rs_bisb_mp_mnn_seq

ha_rs_bisb_mp_fn :: [FilePath]
ha_rs_bisb_mp_fn = map snd (sort (zip ha_rs_bisb_mp_mnn_seq ha_rs_bisb_mp_fn_seq))

ha_rs_bisb_mp_files :: [FilePath]
ha_rs_bisb_mp_files = map (ha_dir </>) ha_rs_bisb_mp_fn

ha_rs_bisb_mp_stored :: [Pitch.Midi]
ha_rs_bisb_mp_stored = sort ha_rs_bisb_mp_mnn_seq

ha_rs_bisb_mp_akt_f :: Common.Akt_f
ha_rs_bisb_mp_akt_f = Common.akt_f_simple (zip ha_rs_bisb_mp_stored [0 ..])

ha_rs_bisb_mp_load_msg :: Buffer_Id -> [Message]
ha_rs_bisb_mp_load_msg = Common.au_loader [0, 1] ha_rs_bisb_mp_files

ha_rs_bisb_mp_load :: Buffer_Id -> IO ()
ha_rs_bisb_mp_load = withSc3 . mapM_ async . ha_rs_bisb_mp_load_msg

ha_rs_bisb_mp_sfz :: [Sfz_Section]
ha_rs_bisb_mp_sfz =
  Common.sfz_grp_rT 2
    : Common.sfz_region_prt_seq ha_rs_bisb_mp_fn ha_rs_bisb_mp_stored

ha_rs_bisb_mp_sfz_wr :: IO ()
ha_rs_bisb_mp_sfz_wr = sfz_write_sections False (ha_dir </> "HA_RS_bisb_mp.sfz") ha_rs_bisb_mp_sfz

-- * HARP / RS-ES-MU-P

-- > length ha_rs_es_mu_p_mnn_seq == 78
ha_rs_es_mu_p_mnn_seq :: [Pitch.Midi]
ha_rs_es_mu_p_mnn_seq =
  let dat =
        [ (10, [1 .. 6])
        , (9, [1 .. 6])
        , (11, [1 .. 6])
        , (1, [2 .. 7])
        , (0, [2 .. 7])
        , (3, [1 .. 7])
        , (2, [2 .. 7])
        , (4, [1 .. 7])
        , (6, [1 .. 7])
        , (5, [1 .. 7])
        , (8, [1 .. 7])
        , (7, [1 .. 7])
        ]
  in vn_mnn_seq_f dat

ha_rs_es_mu_p_fn_seq :: [FilePath]
ha_rs_es_mu_p_fn_seq =
  let f n mnn = concat [show n, "_HA_RS_ES_mu_p_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [925 :: Int .. 1002] ha_rs_es_mu_p_mnn_seq

ha_rs_es_mu_p_gamut :: [Pitch.Midi]
ha_rs_es_mu_p_gamut = sort ha_rs_es_mu_p_mnn_seq

ha_rs_es_mu_p_fn :: [FilePath]
ha_rs_es_mu_p_fn = map snd (sort (zip ha_rs_es_mu_p_mnn_seq ha_rs_es_mu_p_fn_seq))

ha_rs_es_mu_p_files :: [FilePath]
ha_rs_es_mu_p_files = map (ha_dir </>) ha_rs_es_mu_p_fn

ha_rs_es_mu_p_range :: (Pitch.Midi, Pitch.Midi)
ha_rs_es_mu_p_range = Common.gamut_to_range ha_rs_es_mu_p_gamut

ha_rs_es_mu_p_akt_f :: Common.Akt_f
ha_rs_es_mu_p_akt_f = Common.akt_f_rng ha_rs_es_mu_p_range

ha_rs_es_mu_p_load_msg :: Buffer_Id -> [Message]
ha_rs_es_mu_p_load_msg = Common.au_loader [0, 1] ha_rs_es_mu_p_files

ha_rs_es_mu_p_load :: Buffer_Id -> IO ()
ha_rs_es_mu_p_load = withSc3 . mapM_ async . ha_rs_es_mu_p_load_msg

ha_rs_es_mu_p_sfz :: [Sfz_Section]
ha_rs_es_mu_p_sfz =
  Common.sfz_grp_rT 2
    : Common.sfz_region_1_seq ha_rs_es_mu_p_fn ha_rs_es_mu_p_gamut Nothing

ha_rs_es_mu_p_sfz_wr :: IO ()
ha_rs_es_mu_p_sfz_wr = sfz_write_sections False (ha_dir </> "HA_RS_ES_mu_p.sfz") ha_rs_es_mu_p_sfz

-- * HARP / ES-PDLT-MP

ha_es_pdlt_mp_mnn_seq :: [Pitch.Midi]
ha_es_pdlt_mp_mnn_seq =
  let dat =
        [ (10, [1 .. 6])
        , (9, [1 .. 6])
        , (11, [1 .. 6])
        , (1, [2 .. 7])
        , (0, [2 .. 7])
        , (3, [1 .. 7])
        , (2, [2 .. 7])
        , (4, [1 .. 7])
        , (6, [1 .. 7])
        , (5, [1 .. 7])
        , (8, [1 .. 6])
        , (7, [1 .. 7])
        ]
  in vn_mnn_seq_f dat

ha_es_pdlt_mp_fn_seq :: [FilePath]
ha_es_pdlt_mp_fn_seq =
  let f n mnn = concat [show n, "_HA_ES_pdlt_mp_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [650 :: Int .. 726] ha_es_pdlt_mp_mnn_seq

ha_es_pdlt_mp_gamut :: [Pitch.Midi]
ha_es_pdlt_mp_gamut = sort ha_es_pdlt_mp_mnn_seq

ha_es_pdlt_mp_fn :: [FilePath]
ha_es_pdlt_mp_fn = map snd (sort (zip ha_es_pdlt_mp_mnn_seq ha_es_pdlt_mp_fn_seq))

ha_es_pdlt_mp_files :: [FilePath]
ha_es_pdlt_mp_files = map (ha_dir </>) ha_es_pdlt_mp_fn

ha_es_pdlt_mp_range :: (Pitch.Midi, Pitch.Midi)
ha_es_pdlt_mp_range = Common.gamut_to_range ha_es_pdlt_mp_gamut

ha_es_pdlt_mp_akt_f :: Common.Akt_f
ha_es_pdlt_mp_akt_f = Common.akt_f_rng ha_es_pdlt_mp_range

ha_es_pdlt_mp_load_msg :: Buffer_Id -> [Message]
ha_es_pdlt_mp_load_msg = Common.au_loader [0, 1] ha_es_pdlt_mp_files

ha_es_pdlt_mp_load :: Buffer_Id -> IO ()
ha_es_pdlt_mp_load = withSc3 . mapM_ async . ha_es_pdlt_mp_load_msg

ha_es_pdlt_mp_sfz :: [Sfz_Section]
ha_es_pdlt_mp_sfz =
  Common.sfz_grp_rT 2
    : Common.sfz_region_1_seq ha_es_pdlt_mp_fn ha_es_pdlt_mp_gamut Nothing

ha_es_pdlt_mp_sfz_wr :: IO ()
ha_es_pdlt_mp_sfz_wr = sfz_write_sections False (ha_dir </> "HA_ES_pdlt_mp.sfz") ha_es_pdlt_mp_sfz

-- * HARP / ES-mf

ha_es_mf_mnn_seq :: [Pitch.Midi]
ha_es_mf_mnn_seq =
  let dat =
        [ (10, [1 .. 6])
        , (9, [1 .. 6])
        , (11, [0 .. 6])
        , (1, [1 .. 7])
        , (0, [1 .. 7])
        , (3, [1 .. 7])
        , (2, [1 .. 7])
        , (4, [1 .. 7])
        , (6, [1 .. 7])
        , (5, [1 .. 7])
        , (8, [1 .. 7])
        , (7, [1 .. 7])
        ]
  in vn_mnn_seq_f dat

ha_es_mf_fn_seq :: [FilePath]
ha_es_mf_fn_seq =
  let f n mnn = concat [show n, "_HA_ES_mf_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [121 :: Int .. 202] ha_es_mf_mnn_seq

ha_es_mf_gamut :: [Pitch.Midi]
ha_es_mf_gamut = sort ha_es_mf_mnn_seq

ha_es_mf_fn :: [FilePath]
ha_es_mf_fn = map snd (sort (zip ha_es_mf_mnn_seq ha_es_mf_fn_seq))

ha_es_mf_files :: [FilePath]
ha_es_mf_files = map (ha_dir </>) ha_es_mf_fn

ha_es_mf_range :: (Pitch.Midi, Pitch.Midi)
ha_es_mf_range = Common.gamut_to_range ha_es_mf_gamut

ha_es_mf_akt_f :: Common.Akt_f
ha_es_mf_akt_f = Common.akt_f_rng ha_es_mf_range

ha_es_mf_load_msg :: Buffer_Id -> [Message]
ha_es_mf_load_msg = Common.au_loader [0, 1] ha_es_mf_files

ha_es_mf_load :: Buffer_Id -> IO ()
ha_es_mf_load = withSc3 . mapM_ async . ha_es_mf_load_msg

ha_es_mf_sfz :: [Sfz_Section]
ha_es_mf_sfz =
  Common.sfz_grp_rT 2
    : Common.sfz_region_1_seq ha_es_mf_fn ha_es_mf_gamut Nothing

ha_es_mf_sfz_wr :: IO ()
ha_es_mf_sfz_wr = sfz_write_sections False (ha_dir </> "HA_ES_mf.sfz") ha_es_mf_sfz

-- * KLB

klb_dir :: FilePath
klb_dir = "/home/rohan/data/audio/instr/vienna/Woodwind/KLB/"

klb_nc :: Num n => n
klb_nc = 2

klb_mnn_seq :: [Pitch.Midi]
klb_mnn_seq =
  let dat = [(10, [3 .. 5]), (0, [4 .. 6]), (2, [3 .. 6]), (4, [3 .. 6]), (6, [3 .. 6]), (8, [3 .. 5]), (7, [6])]
  in vn_mnn_seq_f dat

klb_stored :: [Pitch.Midi]
klb_stored = sort klb_mnn_seq

klb_range :: (Pitch.Midi, Pitch.Midi)
klb_range = Common.seq_to_range klb_stored

klb_akt_f :: Common.Akt_f
klb_akt_f = Common.akt_f_simple (zip klb_stored [0 ..])

-- * KLB-pA-sus-p

klb_pA_sus_p_fn_seq :: [FilePath]
klb_pA_sus_p_fn_seq =
  let f n mnn = concat [show n, "_KLB_pA_sus_p_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [67 :: Int .. 88] klb_mnn_seq

klb_pA_sus_p_fn :: [FilePath]
klb_pA_sus_p_fn = map snd (sort (zip klb_mnn_seq klb_pA_sus_p_fn_seq))

klb_pA_sus_p_files :: [FilePath]
klb_pA_sus_p_files = map (klb_dir </>) klb_pA_sus_p_fn

klb_pA_sus_p_load_msg :: Buffer_Id -> [Message]
klb_pA_sus_p_load_msg = Common.au_loader [0, 1] klb_pA_sus_p_files

klb_pA_sus_p_load :: Buffer_Id -> IO ()
klb_pA_sus_p_load = withSc3 . mapM_ async . klb_pA_sus_p_load_msg

klb_pA_sus_p_sfz :: [Sfz_Section]
klb_pA_sus_p_sfz =
  Common.sfz_grp_rT 0.35
    : Common.sfz_region_prt_seq klb_pA_sus_p_fn klb_stored

klb_pA_sus_p_sfz_rw :: IO ()
klb_pA_sus_p_sfz_rw = sfz_write_sections False (klb_dir </> "KLB_pA_sus_p.sfz") klb_pA_sus_p_sfz

-- * KLB-stac-p1

klb_stac_p1_fn_seq :: [FilePath]
klb_stac_p1_fn_seq =
  let f n mnn = concat [show n, "_KLB_stac_p1_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [23 :: Int .. 44] klb_mnn_seq

klb_stac_p1_fn :: [FilePath]
klb_stac_p1_fn = map snd (sort (zip klb_mnn_seq klb_stac_p1_fn_seq))

klb_stac_p1_files :: [FilePath]
klb_stac_p1_files = map (klb_dir </>) klb_stac_p1_fn

klb_stac_p1_load_msg :: Buffer_Id -> [Message]
klb_stac_p1_load_msg = Common.au_loader [0, 1] klb_stac_p1_files

klb_stac_p1_load :: Buffer_Id -> IO ()
klb_stac_p1_load = withSc3 . mapM_ async . klb_stac_p1_load_msg

-- * FL2

fl2_dir :: FilePath
fl2_dir = "/home/rohan/data/audio/instr/vienna/Woodwind/FL2/"

fl2_nc :: Num n => n
fl2_nc = 2

fl2_mnn_seq :: [Pitch.Midi]
fl2_mnn_seq =
  let dat = [(9, [4 .. 6]), (11, [4 .. 6]), (1, [4 .. 6]), (0, [4, 7]), (3, [4 .. 6]), (5, [4 .. 6]), (7, [4 .. 6])]
  in vn_mnn_seq_f dat

fl2_range :: (Pitch.Midi, Pitch.Midi)
fl2_range = Common.seq_to_range fl2_mnn_seq

-- * FL2-pV-nA-sus-pp

fl2_pV_nA_sus_pp_fn_seq :: [FilePath]
fl2_pV_nA_sus_pp_fn_seq =
  let f n mnn = concat [show n, "_FL2_pV_nA_sus_pp_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [105 :: Int .. 124] fl2_mnn_seq

fl2_pV_nA_sus_pp_fn :: [FilePath]
fl2_pV_nA_sus_pp_fn = map snd (sort (zip fl2_mnn_seq fl2_pV_nA_sus_pp_fn_seq))

fl2_pV_nA_sus_pp_files :: [FilePath]
fl2_pV_nA_sus_pp_files = map (fl2_dir </>) fl2_pV_nA_sus_pp_fn

fl2_pV_nA_sus_pp_stored :: [Pitch.Midi]
fl2_pV_nA_sus_pp_stored = sort fl2_mnn_seq

fl2_pV_nA_sus_pp_akt_f :: Common.Akt_f
fl2_pV_nA_sus_pp_akt_f = Common.akt_f_simple (zip fl2_pV_nA_sus_pp_stored [0 ..])

fl2_pV_nA_sus_pp_load_msg :: Buffer_Id -> [Message]
fl2_pV_nA_sus_pp_load_msg = Common.au_loader [0, 1] fl2_pV_nA_sus_pp_files

fl2_pV_nA_sus_pp_load :: Buffer_Id -> IO ()
fl2_pV_nA_sus_pp_load = withSc3 . mapM_ async . fl2_pV_nA_sus_pp_load_msg

fl2_pV_nA_sus_pp_sfz :: [Sfz_Section]
fl2_pV_nA_sus_pp_sfz =
  Common.sfz_grp_rT 0.35
    : Common.sfz_region_prt_seq fl2_pV_nA_sus_pp_fn fl2_pV_nA_sus_pp_stored

fl2_pV_nA_sus_pp_sfz_wr :: IO ()
fl2_pV_nA_sus_pp_sfz_wr = sfz_write_sections False (fl2_dir </> "FL2_pV_nA_sus_pp.sfz") fl2_pV_nA_sus_pp_sfz

-- * HO

ho_dir :: FilePath
ho_dir = "/home/rohan/data/audio/instr/vienna/Brass/HO"

ho_nc :: Num n => n
ho_nc = 2

-- * HO-LV-nA-sus-mp

ho_LV_nA_sus_mp_mnn_seq :: [Pitch.Midi]
ho_LV_nA_sus_mp_mnn_seq =
  let dat = [(10, [3 .. 4]), (1, [3]), (0, [3 .. 5]), (2, [3 .. 4]), (4, [3 .. 4]), (6, [3 .. 4]), (8, [3 .. 4])]
  in vn_mnn_seq_f dat

ho_LV_nA_sus_mp_fn_seq :: [FilePath]
ho_LV_nA_sus_mp_fn_seq =
  let f n mnn = concat [show n, "_HO_LV_nA_sus_mp_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [146 :: Int .. 159] ho_LV_nA_sus_mp_mnn_seq

ho_LV_nA_sus_mp_fn :: [FilePath]
ho_LV_nA_sus_mp_fn = map snd (sort (zip ho_LV_nA_sus_mp_mnn_seq ho_LV_nA_sus_mp_fn_seq))

ho_LV_nA_sus_mp_files :: [FilePath]
ho_LV_nA_sus_mp_files = map (ho_dir </>) ho_LV_nA_sus_mp_fn

ho_LV_nA_sus_mp_stored :: [Pitch.Midi]
ho_LV_nA_sus_mp_stored = sort ho_LV_nA_sus_mp_mnn_seq

ho_LV_nA_sus_mp_range :: (Pitch.Midi, Pitch.Midi)
ho_LV_nA_sus_mp_range = Common.seq_to_range ho_LV_nA_sus_mp_stored

ho_LV_nA_sus_mp_akt_f :: Common.Akt_f
ho_LV_nA_sus_mp_akt_f = Common.akt_f_simple (zip ho_LV_nA_sus_mp_stored [0 ..])

ho_LV_nA_sus_mp_load_msg :: Buffer_Id -> [Message]
ho_LV_nA_sus_mp_load_msg = Common.au_loader [0, 1] ho_LV_nA_sus_mp_files

ho_LV_nA_sus_mp_load :: Buffer_Id -> IO ()
ho_LV_nA_sus_mp_load = withSc3 . mapM_ async . ho_LV_nA_sus_mp_load_msg

-- * HO-oV-nA-sus-mp

ho_oV_nA_sus_mp_mnn_seq :: [Pitch.Midi]
ho_oV_nA_sus_mp_mnn_seq =
  let dat = [(10, [2 .. 3]), (11, [1]), (1, [2]), (0, [3]), (3, [2]), (2, [3]), (4, [2 .. 3]), (6, [2 .. 3]), (8, [2 .. 3])]
  in vn_mnn_seq_f dat

ho_oV_nA_sus_mp_fn_seq :: [FilePath]
ho_oV_nA_sus_mp_fn_seq =
  let f n mnn = concat [show n, "_HO_oV_nA_sus_mp_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [190 :: Int .. 202] ho_oV_nA_sus_mp_mnn_seq

ho_oV_nA_sus_mp_fn :: [FilePath]
ho_oV_nA_sus_mp_fn = map snd (sort (zip ho_oV_nA_sus_mp_mnn_seq ho_oV_nA_sus_mp_fn_seq))

ho_oV_nA_sus_mp_files :: [FilePath]
ho_oV_nA_sus_mp_files = map (ho_dir </>) ho_oV_nA_sus_mp_fn

ho_oV_nA_sus_mp_stored :: [Pitch.Midi]
ho_oV_nA_sus_mp_stored = sort ho_oV_nA_sus_mp_mnn_seq

ho_oV_nA_sus_mp_range :: (Pitch.Midi, Pitch.Midi)
ho_oV_nA_sus_mp_range = Common.seq_to_range ho_oV_nA_sus_mp_stored

ho_oV_nA_sus_mp_akt_f :: Common.Akt_f
ho_oV_nA_sus_mp_akt_f = Common.akt_f_simple (zip ho_oV_nA_sus_mp_stored [0 ..])

ho_oV_nA_sus_mp_load_msg :: Buffer_Id -> [Message]
ho_oV_nA_sus_mp_load_msg = Common.au_loader [0, 1] ho_oV_nA_sus_mp_files

ho_oV_nA_sus_mp_load :: Buffer_Id -> IO ()
ho_oV_nA_sus_mp_load = withSc3 . mapM_ async . ho_oV_nA_sus_mp_load_msg

-- * TU

tu_dir :: FilePath
tu_dir = "/home/rohan/data/audio/instr/vienna/Brass/TU"

tu_nc :: Num n => n
tu_nc = 2

-- * TU-oV-nA-sus-mp

tu_oV_nA_sus_mp_mnn_seq :: [Pitch.Midi]
tu_oV_nA_sus_mp_mnn_seq =
  let dat = [(10, [1 .. 3]), (0, [2 .. 4]), (2, [2 .. 4]), (4, [2 .. 4]), (6, [1 .. 3]), (5, [1, 4]), (8, [1 .. 3])]
  in vn_mnn_seq_f dat

tu_oV_nA_sus_mp_fn_seq :: [FilePath]
tu_oV_nA_sus_mp_fn_seq =
  let f n mnn = concat [show n, "_TU_oV_nA_sus_mp_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [101 :: Int .. 120] tu_oV_nA_sus_mp_mnn_seq

tu_oV_nA_sus_mp_fn :: [FilePath]
tu_oV_nA_sus_mp_fn = map snd (sort (zip tu_oV_nA_sus_mp_mnn_seq tu_oV_nA_sus_mp_fn_seq))

tu_oV_nA_sus_mp_files :: [FilePath]
tu_oV_nA_sus_mp_files = map (tu_dir </>) tu_oV_nA_sus_mp_fn

tu_oV_nA_sus_mp_stored :: [Pitch.Midi]
tu_oV_nA_sus_mp_stored = sort tu_oV_nA_sus_mp_mnn_seq

tu_oV_nA_sus_mp_range :: (Pitch.Midi, Pitch.Midi)
tu_oV_nA_sus_mp_range = Common.seq_to_range tu_oV_nA_sus_mp_stored

tu_oV_nA_sus_mp_akt_f :: Common.Akt_f
tu_oV_nA_sus_mp_akt_f = Common.akt_f_simple (zip tu_oV_nA_sus_mp_stored [0 ..])

tu_oV_nA_sus_mp_load_msg :: Buffer_Id -> [Message]
tu_oV_nA_sus_mp_load_msg = Common.au_loader [0, 1] tu_oV_nA_sus_mp_files

tu_oV_nA_sus_mp_load :: Buffer_Id -> IO ()
tu_oV_nA_sus_mp_load = withSc3 . mapM_ async . tu_oV_nA_sus_mp_load_msg

-- * TrC

trC_dir :: FilePath
trC_dir = "/home/rohan/data/audio/instr/vienna/Brass/TrC"

trC_nc :: Num n => n
trC_nc = 2

-- * TrC-oV-nA-sus-mp

trC_oV_nA_sus_mp_mnn_seq :: [Pitch.Midi]
trC_oV_nA_sus_mp_mnn_seq =
  let dat = [(9, [3 .. 5]), (11, [3 .. 5]), (1, [4 .. 5]), (0, [6]), (3, [4 .. 5]), (6, [3]), (5, [4 .. 5]), (7, [3 .. 5])]
  in vn_mnn_seq_f dat

trC_oV_nA_sus_mp_fn_seq :: [FilePath]
trC_oV_nA_sus_mp_fn_seq =
  let f n mnn = concat [show n, "_TrC_oV_nA_sus_mp_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [84 :: Int .. 100] trC_oV_nA_sus_mp_mnn_seq

trC_oV_nA_sus_mp_fn :: [FilePath]
trC_oV_nA_sus_mp_fn = map snd (sort (zip trC_oV_nA_sus_mp_mnn_seq trC_oV_nA_sus_mp_fn_seq))

trC_oV_nA_sus_mp_files :: [FilePath]
trC_oV_nA_sus_mp_files = map (trC_dir </>) trC_oV_nA_sus_mp_fn

trC_oV_nA_sus_mp_stored :: [Pitch.Midi]
trC_oV_nA_sus_mp_stored = sort trC_oV_nA_sus_mp_mnn_seq

trC_oV_nA_sus_mp_range :: (Pitch.Midi, Pitch.Midi)
trC_oV_nA_sus_mp_range = Common.seq_to_range trC_oV_nA_sus_mp_stored

trC_oV_nA_sus_mp_akt_f :: Common.Akt_f
trC_oV_nA_sus_mp_akt_f = Common.akt_f_simple (zip trC_oV_nA_sus_mp_stored [0 ..])

trC_oV_nA_sus_mp_load_msg :: Buffer_Id -> [Message]
trC_oV_nA_sus_mp_load_msg = Common.au_loader [0, 1] trC_oV_nA_sus_mp_files

trC_oV_nA_sus_mp_load :: Buffer_Id -> IO ()
trC_oV_nA_sus_mp_load = withSc3 . mapM_ async . trC_oV_nA_sus_mp_load_msg

-- * PO

po_dir :: FilePath
po_dir = "/home/rohan/data/audio/instr/vienna/Brass/PO-3"

po_nc :: Num n => n
po_nc = 2

-- * PO-oV-nA-sus-mp

po_nA_sus_p_mnn_seq :: [Pitch.Midi]
po_nA_sus_p_mnn_seq =
  let dat = [(9, [2 .. 3]), (11, [2 .. 3]), (1, [3 .. 4]), (3, [3 .. 4]), (4, [2]), (5, [2 .. 4]), (7, [2 .. 4])]
  in vn_mnn_seq_f dat

po_nA_sus_p_fn_seq :: [FilePath]
po_nA_sus_p_fn_seq =
  let f n mnn = concat [show n, "_PO-3_nA_sus_p_", Common.mnn_to_iso_sharp mnn, ".wav"]
  in zipWith f [46 :: Int .. 60] po_nA_sus_p_mnn_seq

po_nA_sus_p_fn :: [FilePath]
po_nA_sus_p_fn = map snd (sort (zip po_nA_sus_p_mnn_seq po_nA_sus_p_fn_seq))

po_nA_sus_p_files :: [FilePath]
po_nA_sus_p_files = map (po_dir </>) po_nA_sus_p_fn

po_nA_sus_p_stored :: [Pitch.Midi]
po_nA_sus_p_stored = sort po_nA_sus_p_mnn_seq

po_nA_sus_p_range :: (Pitch.Midi, Pitch.Midi)
po_nA_sus_p_range = Common.seq_to_range po_nA_sus_p_stored

po_nA_sus_p_akt_f :: Common.Akt_f
po_nA_sus_p_akt_f = Common.akt_f_simple (zip po_nA_sus_p_stored [0 ..])

po_nA_sus_p_load_msg :: Buffer_Id -> [Message]
po_nA_sus_p_load_msg = Common.au_loader [0, 1] po_nA_sus_p_files

po_nA_sus_p_load :: Buffer_Id -> IO ()
po_nA_sus_p_load = withSc3 . mapM_ async . po_nA_sus_p_load_msg
