-- | Farfisa Compact Duo
module Sound.Sc3.Auditor.Fcd where

import Data.Maybe {- base -}
import System.FilePath {- filepath -}

import qualified Music.Theory.List as T {- hmt-base -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}
import Sound.Sc3.Common.Base {- hsc3 -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import qualified Sound.Sc3.Auditor.Common as Common
import qualified Sound.Sc3.Auditor.Fcd.Data as Fcd.Data

-- | dir = directory
fcd_dir :: FilePath
fcd_dir = "/home/rohan/data/audio/instr/farfisa/aad"

fcd_file :: FilePath -> FilePath
fcd_file = (</>) fcd_dir

-- | nc = number-of-channels
fcd_nc :: Int
fcd_nc = 1

fcd_ch_mode :: Common.Ch_Mode
fcd_ch_mode = Common.nc_to_ch_mode fcd_nc

fcd_vc_rng_tbl :: [([Common.Voice_Name], (Key, Key))]
fcd_vc_rng_tbl =
  [
    ( T.interleave
        ["flute8", "oboe8", "trumpet8", "strings8"]
        ["flute8-vib", "oboe8-vib", "trumpet8-vib", "strings8-vib"]
    , (59, 87) -- B3-E6 (partial)
    )
  , (["green-16", "green-2+2_3"], (48, 96)) -- C3-C7
  , (["dolce-8", "principale-8"], (48, 84)) -- C3-C6
  , (["bass-sharp", "bass-soft"], (36, 59)) -- C2-B3
  ]

fcd_vc :: [Common.Voice_Name]
fcd_vc = concatMap fst fcd_vc_rng_tbl

-- | Level adjustments (db) for voices
fcd_lvl :: Common.Voice_Name -> Double
fcd_lvl vc = T.lookup_err vc (zip fcd_vc [6, 6, 3, 3, -3, -3, 3, 3, -3, -3, -3, -3, -3, -3])

-- | Stored as @flac@.
fcd_format :: String
fcd_format = "flac"

{- | 'fcd_range_octpc' as midi note numbers.

>>> map (\(p,q) -> (q,p)) (T.collate (zip (map fcd_range fcd_vc) fcd_vc))
[(["bass-sharp","bass-soft"],(36,59)),(["dolce-8","principale-8"],(48,84)),(["green-16","green-2+2_3"],(48,96)),(["flute8","flute8-vib","oboe8","oboe8-vib","trumpet8","trumpet8-vib","strings8","strings8-vib"],(59,87))]
-}
fcd_range :: Common.Voice_Name -> (Key, Key)
fcd_range vc = lookup_by_note ("fcd_range: " ++ vc) elem vc fcd_vc_rng_tbl

{- | Fcd Akt-f

>>> map (\n -> fcd_akt_f "flute8" (0,n,127)) [50,60..90]
[Nothing,Just (1,0.0),Just (11,0.0),Just (21,0.0),Nothing]

>>> map (\vc -> fcd_akt_f vc (0,60,127)) ["flute8","dolce-8"]
[Just (1,0.0),Just (12,0.0)]
-}
fcd_akt_f :: Common.Voice_Name -> Common.Akt_f
fcd_akt_f = Common.akt_f_rng . fcd_range

{- | Fcd #

>>> map fcd_degree fcd_vc
[29,29,29,29,29,29,29,29,49,49,37,37,24,24]
-}
fcd_degree :: Common.Voice_Name -> Int
fcd_degree = Common.range_to_degree . fcd_range

fcd_gamut :: Common.Voice_Name -> [Key]
fcd_gamut = Common.range_to_gamut . fcd_range

fcd_sample_rate :: Num n => n
fcd_sample_rate = 48000

-- | sf = sound-file
fcd_sf_name_rel :: Maybe FilePath -> Common.Voice_Name -> Key -> FilePath
fcd_sf_name_rel dir vc n = vc </> fromMaybe "" dir </> show n <.> fcd_format

fcd_sf_names_rel :: Maybe FilePath -> Common.Voice_Name -> [FilePath]
fcd_sf_names_rel dir vc = map (fcd_sf_name_rel dir vc) (fcd_gamut vc)

{- | Absolute file names, in sequence, of voice.

>>> putStr $ unlines $ map (head . fcd_sf_names Nothing) fcd_vc
/home/rohan/data/audio/instr/farfisa/aad/flute8/59.flac
/home/rohan/data/audio/instr/farfisa/aad/flute8-vib/59.flac
/home/rohan/data/audio/instr/farfisa/aad/oboe8/59.flac
/home/rohan/data/audio/instr/farfisa/aad/oboe8-vib/59.flac
/home/rohan/data/audio/instr/farfisa/aad/trumpet8/59.flac
/home/rohan/data/audio/instr/farfisa/aad/trumpet8-vib/59.flac
/home/rohan/data/audio/instr/farfisa/aad/strings8/59.flac
/home/rohan/data/audio/instr/farfisa/aad/strings8-vib/59.flac
/home/rohan/data/audio/instr/farfisa/aad/green-16/48.flac
/home/rohan/data/audio/instr/farfisa/aad/green-2+2_3/48.flac
/home/rohan/data/audio/instr/farfisa/aad/dolce-8/48.flac
/home/rohan/data/audio/instr/farfisa/aad/principale-8/48.flac
/home/rohan/data/audio/instr/farfisa/aad/bass-sharp/36.flac
/home/rohan/data/audio/instr/farfisa/aad/bass-soft/36.flac
-}
fcd_sf_names :: Maybe FilePath -> Common.Voice_Name -> [FilePath]
fcd_sf_names dir = map fcd_file . fcd_sf_names_rel dir

fcd_load_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
fcd_load_msg b0 vc = Common.au_loader [0] (fcd_sf_names Nothing vc) b0

fcd_load_or_send_msg :: Buffer_Id -> Common.Voice_Name -> IO [Message]
fcd_load_or_send_msg b0 vc = Common.au_loader_or_sender [0] (fcd_sf_names Nothing vc) b0

fcd_load :: Buffer_Id -> Common.Voice_Name -> IO ()
fcd_load b0 = withSc3 . mapM_ async . fcd_load_msg b0

fcd_load_or_send :: Buffer_Id -> Common.Voice_Name -> IO ()
fcd_load_or_send b0 vc = do
  msg <- fcd_load_or_send_msg b0 vc
  withSc3 (mapM_ async msg)

fcd_sfz :: Common.Voice_Name -> [Sfz_Section]
fcd_sfz vc =
  Common.sfz_grp_rT_vol (0.1, fcd_lvl vc)
    : Common.sfz_region_1_seq (fcd_sf_names_rel Nothing vc) (fcd_gamut vc) Nothing

fcd_sfz_wr :: Common.Voice_Name -> IO ()
fcd_sfz_wr vc = sfz_write_sections False (fcd_dir </> vc <.> "sfz") (fcd_sfz vc)

-- * Zc

fcd_zc_load_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
fcd_zc_load_msg b0 vc = Common.au_loader [0] (fcd_sf_names (Just "zc") vc) b0

fcd_zc_load_or_send_msg :: Buffer_Id -> Common.Voice_Name -> IO [Message]
fcd_zc_load_or_send_msg b0 vc = Common.au_loader_or_sender [0] (fcd_sf_names (Just "zc") vc) b0

fcd_zc_load :: Buffer_Id -> Common.Voice_Name -> IO ()
fcd_zc_load b0 = withSc3 . mapM_ async . fcd_zc_load_msg b0

-- | [(voice,[(start-index,num-frames)])]
fcd_zc_ix :: Num n => [(Common.Voice_Name, [(n, n)])]
fcd_zc_ix = zip fcd_vc (map Common.sf_from_raw_du Fcd.Data.fcd_zc_ix_raw)

-- | [(voice,[(0,end-frame)])]
fcd_zc_lp :: Num n => [(Common.Voice_Name, [(n, n)])]
fcd_zc_lp = map (\(vc, ix) -> (vc, map (\(_st, du) -> (0, du - 1)) ix)) fcd_zc_ix

fcd_zc_sfz :: Common.Voice_Name -> [Sfz_Section]
fcd_zc_sfz vc =
  Common.sfz_grp_rT_vol (0.1, fcd_lvl vc)
    : Common.sfz_region_1_seq (fcd_sf_names_rel (Just "zc") vc) (fcd_gamut vc) (lookup vc fcd_zc_lp)

fcd_zc_sfz_wr :: Common.Voice_Name -> IO ()
fcd_zc_sfz_wr vc = sfz_write_sections False (fcd_dir </> (vc ++ "-zc") <.> "sfz") (fcd_zc_sfz vc)
