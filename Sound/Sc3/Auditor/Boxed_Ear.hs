-- | Boxed Ear <http://www.boxedear.com/free.html>
module Sound.Sc3.Auditor.Boxed_Ear where

import System.FilePath {- filepath -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

-- * CR78

{-

DIAL: BD SD RS HH CY M C HB LB LC ACCENT

CY HH METALLIC; ADD VOICE: TAMBOURINE GUIRO

-}

-- | dir = directory
cr78_dir :: FilePath
cr78_dir = "/home/rohan/data/audio/instr/CR-78/"

cr78_file :: String -> FilePath
cr78_file = (</>) cr78_dir

-- | nc = number-of-channels
cr78_nc :: Int
cr78_nc = 1

cr78_names_grp :: [[String]]
cr78_names_grp =
  [ ["Kick", "Kick Accent"] -- BD
  , ["Snare", "Snare Accent"] -- SD
  , ["Rim Shot"] -- RS
  , ["HiHat", "HiHat Accent", "HiHat Metal"] -- HH
  , ["Cymbal"] -- CY
  , ["Cowbell"] -- C
  , ["Bongo High", "Bongo Low"] -- HB LB
  , ["Conga Low"] -- LC
  , ["Tamb 1", "Tamb 2"]
  , ["Guiro 1", "Guiro 2"]
  ]

cr78_names :: [String]
cr78_names = concat cr78_names_grp

cr78_degree :: Int
cr78_degree = length cr78_names

cr78_alloc_read :: Buffer_Id -> [Message]
cr78_alloc_read b0 =
  let f nm i = b_allocRead (b0 + i) (cr78_file (nm <.> "wav")) 0 0
  in zipWith f cr78_names [0 ..]

cr78_load :: Buffer_Id -> IO ()
cr78_load = withSc3 . mapM_ async . cr78_alloc_read

-- * R8-MK2

-- | dir = directory
r8_dir :: FilePath
r8_dir = "/home/rohan/data/audio/instr/R8-MK2/"

r8_file :: FilePath -> FilePath
r8_file = (</>) r8_dir

-- | nc = number-of-channels
r8_nc :: Int
r8_nc = 1

r8_names_grp :: [[String]]
r8_names_grp =
  [ map ("808" ++) (words "K_A K_B S_A S_B SIDE_A CHH OHH T_A T_B CNG_A CNG_B COW CLAV CLAP")
  , map ("909" ++) (words "K_A K_B S SIDE CHH OHH T")
  , map ("78" ++) (words "K S CHH OHH BNG_A BNG_B COW TAMB GUIR MARC MBEAT_A MBEAT_B MBEAT_C")
  , words "RING1 BRRDC1 CABASA SHOGUI LNGGUI DRYCLAP"
  , words "GATEK2 HARDK PUNCHK FATS1"
  , words "VIDEOK VIDEOS"
  , words "HIGHQ_A HIGHQ_B HIGHQ_C"
  , words "FINGSNAP_A FINGSNAP_B FINGSNAP_C"
  , words "OPENDI_A OPENDI_B"
  , words "PIPE1 SHADOW SPARK1 SQUASK"
  ]

r8_names :: [String]
r8_names = concat r8_names_grp

r8_degree :: Int
r8_degree = length r8_names

r8_alloc_read :: Buffer_Id -> [Message]
r8_alloc_read b0 =
  let f nm i = b_allocRead i (r8_file (nm <.> "wav")) 0 0
  in zipWith f r8_names [b0 ..]

r8_load :: Buffer_Id -> IO ()
r8_load = withSc3 . mapM_ async . r8_alloc_read

{-

import Sound.Sc3.Auditor.Server.Plain
st = st_init_fp ("smplr-pl-pn-1",1,100,())
rt_midi_osc_au (st {st_msg = smplr_msg_f 0})

-}
