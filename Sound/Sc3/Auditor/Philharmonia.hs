-- | <https://www.philharmonia.co.uk/explore/sound_samples>
module Sound.Sc3.Auditor.Philharmonia where

import System.FilePath {- filepath -}

import qualified Music.Theory.Pitch as T {- hmt -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import Sound.Osc.Core {- hsc3 -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Auditor.Common as Common

ph_octpc_pp :: T.OctPc -> String
ph_octpc_pp (oct, pc) =
  let nm = words "C Cs D Ds E F Fs G Gs A As B"
  in nm !! pc ++ show oct

ph_midi_pp :: T.Midi -> String
ph_midi_pp = ph_octpc_pp . T.midi_to_octpc

-- * BCL

bcl_dir :: FilePath
bcl_dir = "/home/rohan/data/audio/instr/philharmonia/bass-clarinet/"

bcl_nc :: Num n => n
bcl_nc = 1

bcl_fn :: Int -> String -> Key -> String
bcl_fn du dyn nt = concat ["bass-clarinet_", ph_midi_pp nt, "_", show du, "_", dyn, "_normal.flac"]

-- | D2-G5
bcl_range :: (Key, Key)
bcl_range = Common.range_octpc_to_midi ((2, 2), (5, 7))

bcl_akt_f :: Common.Akt_f
bcl_akt_f = Common.akt_f_rng bcl_range

bcl_degree :: Int
bcl_degree = Common.range_to_degree bcl_range

bcl_gamut :: [Key]
bcl_gamut = Common.range_to_gamut bcl_range

bcl_names :: [String]
bcl_names = map (bcl_fn 15 "pianissimo") bcl_gamut

bcl_files :: [FilePath]
bcl_files = map (\nm -> bcl_dir </> "flac" </> nm) bcl_names

bcl_load_msg :: Buffer_Id -> [Message]
bcl_load_msg = Common.au_loader [0] bcl_files

bcl_load :: Buffer_Id -> IO ()
bcl_load = withSc3 . mapM_ async . bcl_load_msg

-- > putStrLn $ unlines $ bcl_sfz
bcl_sfz :: [Sfz_Section]
bcl_sfz =
  Common.sfz_ctl_dir "flac"
    : Common.sfz_grp_rT 0.1
    : Common.sfz_region_1_seq bcl_names bcl_gamut Nothing

bcl_sfz_wr :: IO ()
bcl_sfz_wr = sfz_write_sections False (bcl_dir </> "bcl.sfz") bcl_sfz
