-- | Functions to generate a click track from a metric structure.
module Sound.Sc3.Auditor.Ct where

import Data.List {- base -}

import qualified Music.Theory.Duration.ClickTrack as T {- hmt -}
import qualified Music.Theory.Time.Seq as T {- hmt -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Auditor.Common as Common {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Pp as Pp {- hsc3-auditor -}

type Ct_Sf = (FilePath, Buffer_Id, Double)
type Ct_Node_Sf = T.Ct_Node -> [Ct_Sf]

ct_note :: Ct_Sf -> Pp.Note
ct_note (_, i, j) = Pp.Note (i, 0) j

ct_realise :: Ct_Node_Sf -> T.Dseq Double T.Ct_Node -> T.Tseq Double [Ct_Sf]
ct_realise ix_fn = T.tseq_map ix_fn . T.dseq_to_tseq 0 T.Ct_End

ct_normalise_sf :: [Ct_Sf] -> [(Buffer_Id, FilePath)]
ct_normalise_sf = nub . sort . map (\(fn, ix, _) -> (ix, fn))

ct_sf_seq :: [(Buffer_Id, FilePath)] -> [FilePath]
ct_sf_seq sq =
  let (ix, fn) = unzip sq
  in if ix `isPrefixOf` [0 ..]
      then fn
      else error "ct_sf_seq"

ct_auditor :: Ct_Node_Sf -> T.Dseq Double T.Ct_Node -> ([FilePath], T.Tseq Double Pp.Chord)
ct_auditor ix_fn sq =
  let sf = ct_realise ix_fn sq
      pp = T.tseq_map (map ct_note) sf
      fn_set = ct_sf_seq (ct_normalise_sf (concatMap snd sf))
  in (fn_set, pp)

ct_ldr :: [FilePath] -> [Message]
ct_ldr fn = Common.au_loader [0] fn 0

ct_sc :: Ct_Node_Sf -> T.Ct -> Nrt
ct_sc ix_fn ct =
  let (rq, n) = T.ct_count ct
      pre = T.ct_leadin (rq, fromRational (T.ct_tempo0_err ct), n)
      (fn_set, pp) = ct_auditor ix_fn (pre ++ T.ct_dseq ct)
  in Pp.pp_nrt Nothing 1 (Just (ct_ldr fn_set)) pp

ct_render :: FilePath -> FilePath -> Ct_Node_Sf -> T.Ct -> IO ()
ct_render osc_fn wav_fn ix_fn =
  nrt_render_plain (osc_fn, wav_fn, 1, 48000, PcmInt16, [])
    . ct_sc ix_fn
