-- | Sfz
module Sound.Sc3.Auditor.Sfz where

import Control.Monad {- base -}
import Data.List {- base -}

import Sound.Osc.Core {- hsc3 -}
import Sound.Sc3 {- hsc3 -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Math.Types {- hsc3-data -}
import qualified Sound.Sc3.Data.Sfz as Sfz {- hsc3-data -}

import qualified Sound.Sc3.Auditor.Common as Common
import qualified Sound.Sc3.Auditor.Server.Plain as Plain
import qualified Sound.Sc3.Auditor.Smplr as Smplr

-- * Types

-- | nc = number-of-channels
type Nc = Int

-- | (pitch_keycenter,lokey,hikey), see sfz_region_key
type Sfz_Key = (Key, Key, Key)

-- | loop_mode (loop_start,loop_end), see sfz_region_loop_data
type Sfz_Loop = (String, Maybe (U32, U32))

-- | (Buf,Nc,Fn,Vol,Pan,Key,Tun,Chan,Vel,Loop,Amp-Eg)
type Sfz_Node = (Buffer_Id, Nc, FilePath, F64, F64, Sfz_Key, I8, (Channel, Channel), (Velocity, Velocity), Sfz_Loop, (F64, F64))

sfz_node_bufnum :: Sfz_Node -> Buffer_Id
sfz_node_bufnum (b, _, _, _, _, _, _, _, _, _, _) = b

sfz_node_nc :: Sfz_Node -> Nc
sfz_node_nc (_, nc, _, _, _, _, _, _, _, _, _) = nc

sfz_node_vol :: Sfz_Node -> F64
sfz_node_vol (_, _, _, vol, _, _, _, _, _, _, _) = vol

sfz_node_lp :: Sfz_Node -> Sfz_Loop
sfz_node_lp (_, _, _, _, _, _, _, _, _, lp, _) = lp

sfz_node_ampeg :: Sfz_Node -> (F64, F64)
sfz_node_ampeg (_, _, _, _, _, _, _, _, _, _, eg) = eg

-- * Parse

sfz_node_parse :: Buffer_Id -> Nc -> Sfz.Sfz_Region -> Sfz_Node
sfz_node_parse k nc r =
  ( k
  , nc
  , Sfz.sfz_region_sample r
  , Sfz.sfz_region_volume r -- db
  , Sfz.sfz_region_pan r -- -100 - 100
  , Sfz.sfz_region_key r -- ie. (center,lo,hi)
  , Sfz.sfz_region_tune r -- cents (-100 - 100)
  ,
    ( Sfz.sfz_region_lochan r - 1
    , Sfz.sfz_region_hichan r - 1
    )
  ,
    ( Sfz.sfz_region_lovel r
    , Sfz.sfz_region_hivel r
    )
  , Sfz.sfz_region_loop_data r
  ,
    ( Sfz.sfz_region_ampeg_attack r
    , Sfz.sfz_region_ampeg_release r
    )
  )

-- * Io

-- | (Sfz-File,Sfz-Data,Sfz-Node)
type Sfz_Instr = (FilePath, Sfz.Sfz_Data, [Sfz_Node])

-- | Number of Sfz_Node at Sfz_Instr
sfz_instr_degree :: Sfz_Instr -> Int
sfz_instr_degree (_, _, nd) = length nd

-- | Load Sfz file as Sfz_Instr.
sfz_load_instr :: Buffer_Id -> FilePath -> IO Sfz_Instr
sfz_load_instr b0 fn = do
  dat <- Sfz.sfz_load_data fn
  let (_, _, rgn) = dat
  nc <- Sfz.sfz_data_get_nc fn dat
  return (fn, dat, zipWith3 sfz_node_parse [b0 ..] nc rgn)

{- | Load sequence of Sfz.

> fn_seq <- fmap lines (readFile "/home/rohan/sw/hsc3-auditor/etc/sfz.text")
> sfz_seq <- sfz_load_instr_seq 0 (take 99 fn_seq)
-}
sfz_load_instr_seq :: Buffer_Id -> [FilePath] -> IO [Sfz_Instr]
sfz_load_instr_seq b0 =
  let f (b, is) fn = do
        i <- sfz_load_instr b fn
        return (b + sfz_instr_degree i, i : is)
  in fmap (reverse . snd) . foldM f (b0, [])

-- * Match

-- | Does midi-note-event (channel,midi-note-number,velocity) match Sfz node.
sfz_node_match :: (Channel, Key, Velocity) -> Sfz_Node -> Bool
sfz_node_match (ch, mnn, vel) (_, _, _, _, _, (_, k1, k2), _, (c1, c2), (v1, v2), _, _) =
  ch >= c1
    && ch <= c2
    && mnn >= k1
    && mnn <= k2
    && vel >= v1
    && vel <= v2

sfz_node_find :: (Channel, Key, Velocity) -> [Sfz_Node] -> Maybe Sfz_Node
sfz_node_find nt = find (sfz_node_match nt)

-- * Sc3/Buffer

-- | scsynth messages to load buffer data for Sfz_Instr
sfz_load_msg :: Buffer_Id -> Sfz_Instr -> IO [Message]
sfz_load_msg b0 (fn, (ctl, _, rgn), nd) = do
  let to_ch x = [0 .. x - 1]
      nc = map sfz_node_nc nd
      nm = map (Sfz.sfz_region_sample_resolve fn ctl) rgn
      bu = map ((+ b0) . sfz_node_bufnum) nd
      lp = repeat Nothing
  zipWithM Common.ld_or_send_buf_msg (map to_ch nc) (zip3 bu nm lp)

-- | Load data for Sfz_Instr
sfz_load :: Buffer_Id -> Sfz_Instr -> IO ()
sfz_load b0 sfz = do
  msg <- sfz_load_msg b0 sfz
  withSc3 (mapM_ async msg)

-- * Smplr

sfz_node_to_syn_opt :: Common.En_Mode -> Sfz_Node -> (Smplr.Smplr_Opt, Maybe (U32, U32))
sfz_node_to_syn_opt en nd =
  let ch = Common.nc_to_ch_mode (sfz_node_nc nd)
  in case sfz_node_lp nd of
      ("no_loop", _) -> ((ch, en, Common.Lp_None), Nothing)
      ("one_shot", _) -> ((ch, Common.En_Buf, Common.Lp_None), Nothing)
      ("loop_continuous", lp) -> ((ch, en, Common.Lp_Seg), lp)
      _ -> error "sfz_node_to_syn_opt"

-- > map (\mnn -> sfz_akt_f (0,60,0) (0,mnn,127)) [59,60,61]
sfz_akt_f :: (Buffer_Id, Key, I8) -> Common.Akt_f
sfz_akt_f (b, kc, kt) (_, mnn, _) = Just (b, (int_to_f64 mnn - int_to_f64 kc) * 100 + i8_to_f64 kt)

sfz_node_akt_f :: Sfz_Node -> Common.Akt_f
sfz_node_akt_f (b, _, _, _, _, (kc, _, _), kt, _, _, _, _) = sfz_akt_f (b, kc, kt)

sfz_node_set_akt_f :: [Sfz_Node] -> Common.Akt_f
sfz_node_set_akt_f nd_set evt =
  case sfz_node_find evt nd_set of
    Nothing -> Nothing
    Just nd -> sfz_node_akt_f nd evt

-- * Sc3/Node

-- | ie. meta_sc3_msg_f, biases to opt_prm...
sfz_sc3_msg_f :: F64 -> Common.En_Mode -> [Sfz_Node] -> (String, Synth_Id, Group_Id, Param) -> (Channel, F64, F64) -> Maybe Message
sfz_sc3_msg_f rt_mul en_mode nd_set (_syn, nid, gid, prm_opt) (ch, fmnn, fvel) =
  case sfz_node_find (ch, floor fmnn, floor fvel) nd_set of
    Nothing -> Nothing
    Just nd ->
      let (syn_opt, lp) = sfz_node_to_syn_opt en_mode nd
          lp_prm = case lp of
            Nothing -> []
            Just (st, en) ->
              [ ("loop_start", u32_to_f64 st)
              , ("loop_end", u32_to_f64 en)
              ]
          (_, _, _, vol, _, _, _, _, _, _, (aT, rT)) = nd
          sfz_prm = [("lvl", db_to_amp vol), ("aT", aT), ("rT", rT)]
          rt_prm = Common.smplr_calc_rate rt_mul (sfz_node_akt_f nd) (ch, fmnn, fvel)
          syn_prm = param_merge_r_seq [lp_prm, sfz_prm, rt_prm, prm_opt]
      in Just (s_new (Smplr.smplr_nm syn_opt) nid AddToTail gid syn_prm)

-- * Plain

-- | Biases to opt_prm over prm, ie. options over-write Sfz.
sfz_msg_f :: (usr -> ([Sfz_Node], Param)) -> Plain.Msg_F usr
sfz_msg_f get_f evt (_syn, nid, gid, prm, usr) =
  let (nd_set, opt_prm) = get_f usr
  in case sfz_sc3_msg_f 1 Common.En_Gate nd_set ("?", nid, gid, param_merge_r prm opt_prm) evt of
      Nothing -> (nid, usr, [])
      Just msg -> (nid, usr, [msg])

{-
fn = "/home/rohan/data/audio/instr/bosendorfer/pf.sfz"
fn = "/home/rohan/data/audio/instr/bosendorfer/008.sfz"
fn = "/home/rohan/data/audio/instr/casacota/zell_1737_415_MeanTone5/8_i.sfz"
(_,_,nd) <- sfz_load_instr 0 fn
nd !! 0
sfz_node_find (0,60,60) nd

fn = "/home/rohan/data/audio/instr/audiosonic/flute.sfz"
(_,dat,nd) <- sfz_load_instr 0 fn
dat

-}
