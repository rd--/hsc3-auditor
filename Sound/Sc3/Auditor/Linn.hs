{- | Linn sample sets.

HR = <http://machines.hyperreal.org/manufacturers/Linn/LinnDrum/>
TA = <http://trashaudio.com/2012/10/linndrum-lm2-samples/>
-}
module Sound.Sc3.Auditor.Linn where

import System.FilePath {- filepath -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

-- * LINN-HR

-- | dir = directory
linn_hr_dir :: FilePath
linn_hr_dir = "/home/rohan/data/audio/instr/linndrum"

linn_hr_file :: FilePath -> FilePath
linn_hr_file = (</>) linn_hr_dir

-- | nc = number-of-channels
linn_hr_nc :: Int
linn_hr_nc = 1

{- | File names, in groups.

sd=snare-drum, sst=sidestick, chh=closed-hihat, ohh=open-hihat

pitch:ll=lower,l=low,medium,h=high,hh=higher
decay:s=short,medium,l=long
-}
linn_hr_names_grp :: [[String]]
linn_hr_names_grp =
  [ ["kick", "kickme"]
  , ["sdh", "sd", "sdl"]
  , ["ssth", "sst", "sstl"]
  , ["chhs", "chh", "chhl"]
  , ["ohh"]
  , ["tomhh", "tomh"]
  , ["tom"]
  , ["toml", "tomll"]
  , ["ride"]
  , ["crash"]
  , ["cabasa"]
  , ["tamb"]
  , ["congahh", "congah"]
  , ["conga"]
  , ["congal", "congall", "congalll"]
  , ["cowb"]
  , ["clap"]
  ]

linn_hr_names :: [String]
linn_hr_names = concat linn_hr_names_grp

linn_hr_degree :: Int
linn_hr_degree = length linn_hr_names

linn_hr_alloc_read :: Buffer_Id -> [Message]
linn_hr_alloc_read b0 =
  let f nm i = b_allocRead (b0 + i) (linn_hr_file (nm <.> ".wav")) 0 0
  in zipWith f linn_hr_names [0 ..]

linn_hr_load :: Buffer_Id -> IO ()
linn_hr_load = withSc3 . mapM_ async . linn_hr_alloc_read

-- * LINN-TA

-- | dir = directory
linn_ta_dir :: FilePath
linn_ta_dir = "/home/rohan/data/audio/instr/LinnDrum-LM-2/Normalized/"

linn_ta_file :: FilePath -> FilePath
linn_ta_file = (</>) linn_ta_dir

-- | nc = number-of-channels
linn_ta_nc :: Int
linn_ta_nc = 2

linn_ta_names_grp :: [(String, Int)]
linn_ta_names_grp =
  [ ("Kick", 1)
  , ("Snare", 12)
  , ("Rimshot", 9)
  , ("HatClosed", 7)
  , ("HatOpen", 1)
  , ("Tom", 13)
  , ("Ride", 1)
  , ("Crash", 1)
  , ("Cabasa", 1)
  , ("Tambourine", 1)
  , ("Cowbell", 1)
  , ("Clap", 1)
  ]

linn_ta_names :: [String]
linn_ta_names =
  let f (nm, k) = if k == 1 then [nm] else map (\z -> nm ++ show z) [1 .. k]
  in concatMap f linn_ta_names_grp

linn_ta_degree :: Int
linn_ta_degree = length linn_ta_names

linn_ta_alloc_read :: Buffer_Id -> [Message]
linn_ta_alloc_read b0 =
  let f nm i = b_allocRead (b0 + i) (linn_ta_file (nm <.> "aif")) 0 0
  in zipWith f linn_ta_names [0 ..]

linn_ta_load :: Buffer_Id -> IO ()
linn_ta_load = withSc3 . mapM_ async . linn_ta_alloc_read

{-

LM-1:

BUTTON LAYOUT:

CONGA HI TOM HI snare bass hihat HIHAT DECAY CLAPS   tamb cabasa
CONGA LO TOM LO SNARE BASS HIHAT COWBELL     RIMSHOT TAMB CABASA

MIXER SEQUENCE:

CONGA HI CONGA LO TOM HI TOM LO SNARE BASS HIHAT COWBELL CLAPS RIMSHOT TAMB CABASA CUSTOM

LinnDrum:

BUTTON LAYOUT:

CABASA 1 2 TAMBOURINE 1 2 CONGA HI LO COWBELL CLAPS
HIHAT 1 2 OPEN TOMS HI MID LO RIDE 1 2
SNARE SIDESTICK 1 2 3 BASS 1 2 CRASH [PERCUSSION]

MIXER SEQUENCE:

BASS SNARE SIDESTICK HIHAT TOMS HI MID LO RIDE CRASH CABASA TAMB CONGA HI LO COWBELL CLAPS CLICK

-}
