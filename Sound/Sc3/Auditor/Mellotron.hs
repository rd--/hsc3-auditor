{- | <http://leisureland.us/audio/MellotronSamples/MellotronSamples.htm>

Some files at MkIIViolins must be renamed (Bb -> A#).
-}
module Sound.Sc3.Auditor.Mellotron where

import System.FilePath {- filepath -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import qualified Sound.Sc3.Auditor.Common as Common

-- | ml = mellotron
ml_dir :: FilePath
ml_dir = "/home/rohan/data/audio/instr/mellotron/"

-- | nc = number-of-channels
ml_nc :: Int
ml_nc = 1

-- | G2 - F5
ml_range :: (Key, Key)
ml_range = Common.range_octpc_to_midi ((2, 7), (5, 5))

ml_akt_f :: Common.Akt_f
ml_akt_f = Common.akt_f_rng ml_range

ml_degree :: Int
ml_degree = Common.range_to_degree ml_range

ml_gamut :: [Key]
ml_gamut = Common.range_to_gamut ml_range

ml_names :: [String]
ml_names = map ((<.> "wav") . Common.mnn_to_iso_sharp) ml_gamut

-- > putStr $ unlines $ concatMap ml_files ml_voices
ml_vc :: [Common.Voice_Name]
ml_vc =
  [ "Cello"
  , "CombinedChoir"
  , "GC3Brass"
  , "M300A"
  , "M300B"
  , "MkIIBrass"
  , "MkIIFlute"
  , "MkIIViolins"
  , "StringSection"
  , "Woodwind2"
  ]

ml_n_vc :: Int
ml_n_vc = length ml_vc

ml_files_rel :: Common.Voice_Name -> [FilePath]
ml_files_rel vc = let f nm = vc </> nm in map f ml_names

ml_files :: Common.Voice_Name -> [FilePath]
ml_files = map (ml_dir </>) . ml_files_rel

ml_load_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
ml_load_msg b0 vc = Common.au_loader [0] (ml_files vc) b0

ml_load :: Buffer_Id -> Common.Voice_Name -> IO ()
ml_load b0 vc = withSc3 (mapM_ async (ml_load_msg b0 vc))

ml_sfz :: Common.Voice_Name -> [Sfz_Section]
ml_sfz vc =
  Common.sfz_grp_rT 0.1
    : Common.sfz_region_1_seq (ml_files_rel vc) ml_gamut Nothing

-- > mapM_ ml_sfz_wr ml_vc
ml_sfz_wr :: Common.Voice_Name -> IO ()
ml_sfz_wr vc = sfz_write_sections False (ml_dir </> vc <.> "sfz") (ml_sfz vc)
