-- | Pp form of auditor functions.
module Sound.Sc3.Auditor.Pp where

import Data.Maybe {- base -}

import qualified Music.Theory.Time.Seq as T {- hmt -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Auditor.Smplr.Nc as Smplr

-- | Amplitude (linear gain)
type Amplitude = Double

-- | Number of channels
type Nc = Int

type Ix = (Buffer_Id, Double) -- detune

-- | 'Buffer_Id' and 'Amplitude'
data Note = Note
  { note_index :: Ix
  , note_amplitude :: Amplitude
  }
  deriving (Eq, Ord, Show)

-- | Chord
type Chord = [Note]

to_p :: (a -> (Buffer_Id, Double), a -> Amplitude) -> (Time, [a]) -> (Time, Chord)
to_p (i, a) (t, x) =
  let f e = Note (i e) (a e)
  in (t, map f x)

chord_indices :: Chord -> [Ix]
chord_indices = map note_index

pp_start_times :: T.Tseq Time Chord -> [Time]
pp_start_times = map fst

pp_chords :: T.Tseq Time Chord -> [Chord]
pp_chords = map snd

-- | Start time of last element (this is not the same as 'tseq_dur').
pp_duration :: T.Tseq Time Chord -> Time
pp_duration = fst . last

-- | (duration/sec,fade-time/sec)
type Opt_Dur = Maybe (Double, Double)

chd_msg :: Opt_Dur -> Chord -> [Message]
chd_msg opt =
  let f n =
        let (b, dt) = note_index n
            p =
              [ ("bufnum", fromIntegral b)
              , ("rate", midiRatio (dt / 100))
              , ("amp", note_amplitude n)
              ]
            p' = case opt of
              Nothing -> p
              Just (d, ft) -> ("dur", d) : ("fadeTime", ft) : p
        in s_new "smplr" (-1) AddToTail 1 p'
  in map f

p_osc :: Opt_Dur -> (Time, Chord) -> Maybe (BundleOf Message)
p_osc opt (t, c) = if null c then Nothing else Just (bundle t (chd_msg opt c))

pp_nrt :: Opt_Dur -> Nc -> Maybe [Message] -> T.Tseq Time Chord -> Nrt
pp_nrt opt nc ld_m pp =
  let group_zero = g_new [(1, AddToTail, 0)]
      sc_init =
        let h = bundle 0 [group_zero, Smplr.smplr_nc_msg nc]
        in h : maybe [] (\ld -> map (bundle 0 . return) ld) ld_m
      sc_end = [bundle (pp_duration pp + 12) [g_freeAll [1]]]
  in Nrt (sc_init ++ mapMaybe (p_osc opt) pp ++ sc_end)

-- | Variant of 'pp_nrt' that writes @Nrt@ score to named file using 'writeNrt'.
pp_nrt_write :: Opt_Dur -> FilePath -> Nc -> [Message] -> T.Tseq Time Chord -> IO ()
pp_nrt_write opt nm nc ld = writeNrt nm . pp_nrt opt nc (Just ld)

-- | 'audition' of 'pp_nrt' (two channels, no loader).
pp_audition :: Opt_Dur -> T.Tseq Time Chord -> IO ()
pp_audition opt = nrt_audition . pp_nrt opt 2 Nothing
