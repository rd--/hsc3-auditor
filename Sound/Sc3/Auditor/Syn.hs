-- | Synthesisers.
module Sound.Sc3.Auditor.Syn where

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

{- | Append either @-gt@ or @-du@.

> syn_nm ("triangle-tone",True) == "triangle-tone-gt"
-}
syn_nm :: (String, Bool) -> String
syn_nm (osc_ty, use_gate) = osc_ty ++ if use_gate then "-gt" else "-du"

osc_tone :: Bool -> (Ugen -> Ugen) -> Ugen
osc_tone use_gate gen =
  let l = control kr "pan" 0
      a = control kr "amp" 0.1
      aT = control kr "aT" 0 -- attack time
      rT = control kr "rT" 0.5 -- release time
      w = control kr "r_pan" 0
      bus = control kr "bus" 0
      mnn = control kr "mnn" 69 -- fractional midi note number
      pw = control kr "pw" 0 -- CC, semitones
      f = midiCps (mnn + pw)
      s = gen f * a
      e =
        if use_gate
          then envGen kr (control kr "gate" 1) 1 0 1 RemoveSynth (envAsr aT 1 rT EnvSin)
          else
            let sus = control kr "sustain" 1
            in envGen kr 1 1 0 1 RemoveSynth (envLinen aT (sus - aT - rT) rT 1)
  in out bus (pan2 s (l + randId 'α' (-w) w) e)

sine_tone :: Bool -> Synthdef
sine_tone use_gate =
  let nm = syn_nm ("sine-tone", use_gate)
  in synthdef nm (osc_tone use_gate (\f -> sinOsc ar f 0))

triangle_tone :: Bool -> Synthdef
triangle_tone use_gate =
  let nm = syn_nm ("triangle-tone", use_gate)
  in synthdef nm (osc_tone use_gate (\f -> lfTri ar f 0))

-- | (Program,Osc-Type,Node-Id,Group-Id,Attack-Time,Release-Time,Param)
type Opt = (Int, String, Int, Int, Double, Double, Param)

-- | Pw = Pitch-Wheel/Semitones, Mnn=Fractional Midi Note Number (0-127), Gain (0-1)
syn_message :: Opt -> Double -> Double -> Double -> Maybe Double -> Message
syn_message (prg, nm, nid, grp, aT, rT, p3) pw mnn ampl du =
  let p1 =
        [ ("gate", 1)
        , ("mnn", mnn)
        , ("pw", pw)
        , ("aT", aT)
        , ("rT", rT)
        , ("amp", ampl)
        , ("prg", fromIntegral prg)
        ]
      p2 = maybe p1 (\x -> ("sustain", x) : p1) du
  in s_new nm nid AddToTail grp (param_merge_r p3 p2)

{- | 'd_recv' messages for all syn variants.

> withSc3 (mapM_ async syn_recv_all_msg)
-}
syn_recv_all_msg :: [Message]
syn_recv_all_msg = [d_recv (fn gt) | fn <- [sine_tone, triangle_tone], gt <- [False, True]]
