{- | Audiosonic Mod. 37 Z

Recordings of Audiosonic Mod. 37 Z from C3 to C6 (37 tones).

Voices are flute, oboe, strings. Each with and without vibrato.

Files are each recorded in one pass, metronome m=60, measure=8/4.
-}
module Sound.Sc3.Auditor.A37z where

import Data.Maybe {- base -}
import System.FilePath {- filepath -}

import Sound.Midi.Type {- midi-osc -}

import Sound.Sc3.Data.Sfz {- hsc3-data -}

import Sound.Osc.Core {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Auditor.A37z.Data as A37z.Data
import qualified Sound.Sc3.Auditor.Common as Common

-- | dir = directory
a37z_dir :: FilePath
a37z_dir = "/home/rohan/data/audio/instr/audiosonic/"

a37z_file :: FilePath -> FilePath
a37z_file = (++) a37z_dir

-- | nc = number-of-channels
a37z_nc :: Int
a37z_nc = 1

a37z_ch_mode :: Common.Ch_Mode
a37z_ch_mode = Common.nc_to_ch_mode a37z_nc

a37z_vc :: [Common.Voice_Name]
a37z_vc = ["flute", "oboe", "strings", "flute-vib", "oboe-vib", "strings-vib"]

-- | Stored as @wav@.
a37z_format :: String
a37z_format = "wav"

-- | C3-C6
a37z_range :: (Key, Key)
a37z_range = Common.range_octpc_to_midi ((3, 0), (6, 0))

a37z_akt_f :: Common.Akt_f
a37z_akt_f = Common.akt_f_rng a37z_range

a37z_degree :: Int
a37z_degree = Common.range_to_degree a37z_range

a37z_gamut :: [Key]
a37z_gamut = Common.range_to_gamut a37z_range

a37z_sample_rate :: Num n => n
a37z_sample_rate = 48000

-- | sf = sound-file
a37z_sf_name_rel :: Maybe FilePath -> Common.Voice_Name -> Key -> FilePath
a37z_sf_name_rel sub_dir vc n = vc </> fromMaybe "" sub_dir </> show n <.> a37z_format

a37z_sf_names_rel :: Maybe FilePath -> Common.Voice_Name -> [FilePath]
a37z_sf_names_rel sub_dir vc = map (a37z_sf_name_rel sub_dir vc) a37z_gamut

{- | Absolute file names, in sequence, of voice.

> putStrLn $ unlines $ concat $ map (a37z_sf_names Nothing) a37z_vc
-}
a37z_sf_names :: Maybe FilePath -> Common.Voice_Name -> [FilePath]
a37z_sf_names dir = map a37z_file . a37z_sf_names_rel dir

a37z_load_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
a37z_load_msg b0 vc = Common.au_loader [0] (a37z_sf_names Nothing vc) b0

a37z_load :: Buffer_Id -> Common.Voice_Name -> IO ()
a37z_load b0 = withSc3 . mapM_ async . a37z_load_msg b0

a37z_sfz :: Common.Voice_Name -> [Sfz_Section]
a37z_sfz vc =
  Common.sfz_grp_rT 0.05
    : Common.sfz_region_1_seq (a37z_sf_names_rel Nothing vc) a37z_gamut Nothing

a37z_sfz_wr :: Common.Voice_Name -> IO ()
a37z_sfz_wr vc = sfz_write_sections False (a37z_dir </> vc <.> "sfz") (a37z_sfz vc)

-- * ZC

a37z_zc_load_msg :: Buffer_Id -> Common.Voice_Name -> [Message]
a37z_zc_load_msg b0 vc = Common.au_loader [0] (a37z_sf_names (Just "zc") vc) b0

a37z_zc_load :: Buffer_Id -> Common.Voice_Name -> IO ()
a37z_zc_load b0 = withSc3 . mapM_ async . a37z_zc_load_msg b0

a37z_zc_ix :: [(Common.Voice_Name, [Common.LoopPoints])]
a37z_zc_ix = zip a37z_vc (map Common.sf_from_raw_du A37z.Data.a37z_zc_ix_raw)

a37z_zc_lp :: [(Common.Voice_Name, [Common.LoopPoints])]
a37z_zc_lp = map (\(vc, ix) -> (vc, map (\(_st, du) -> (0, du - 1)) ix)) a37z_zc_ix

a37z_zc_sfz :: Common.Voice_Name -> [Sfz_Section]
a37z_zc_sfz vc =
  Common.sfz_grp_rT 0.05
    : Common.sfz_region_1_seq (a37z_sf_names_rel (Just "zc") vc) a37z_gamut (lookup vc a37z_zc_lp)

a37z_zc_sfz_wr :: Common.Voice_Name -> IO ()
a37z_zc_sfz_wr vc = sfz_write_sections False (a37z_dir </> (vc ++ "-zc") <.> "sfz") (a37z_zc_sfz vc)
