-- | Auditor common
module Sound.Sc3.Auditor.Common where

import Data.Bifunctor {- base -}
import Data.Int {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import System.FilePath {- filepath -}
import System.IO {- base -}
import Text.Printf {- base -}

import qualified Data.List.Split as Split {- split -}
import qualified System.Process as Process {- process -}

import qualified Music.Theory.List as List {- hmt-base -}

import qualified Music.Theory.Pitch as Pitch {- hmt -}
import qualified Music.Theory.Tuning as Tuning {- hmt -}
import qualified Music.Theory.Tuning.Midi as Tuning.Midi {- hmt -}

import qualified Sound.Midi.Type as Midi {- midi-osc -}

import qualified Sound.Osc.Core as Osc {- hosc -}
import qualified Sound.Sc3 as Sc3 {- hsc3 -}

import qualified Sound.Sc3.Server.Command.MemCpy as MemCpy {- sc3-rdu -}

import qualified Sound.File.Riff as Riff {- hsc3-sf -}
{- hsc3-sf -}

import qualified Sound.File.Vector as Vector {- hsc3-sf -}
import qualified Sound.File.Wave as Wave

import qualified Sound.File.HSndFile as Sf {- hsc3-sf-hsndfile -}

import qualified Sound.Sc3.Data.Sfz as Sfz {- hsc3-data -}

-- * Param

-- | Scan Opt for indicated keys and parse to Param
param_opt_scan :: [String] -> [(String, String)] -> Sc3.Param
param_opt_scan nm = mapMaybe (\(k, v) -> if k `elem` nm then Just (k, read v) else Nothing)

-- * Channels

-- | Channel modes, allows direct (non-panning) mono.
data Ch_Mode
  = -- | Stereo pan of mono signal
    Ch_Pan_1
  | -- | Azimth pan of mono signal accross /n/ channels
    Ch_PanAz_1 Int
  | -- | Direct send of /n/ channels to "bus" parameter
    Ch_Direct Int
  deriving (Eq, Show)

-- | Input channel count.
ch_mode_nc :: Ch_Mode -> Int
ch_mode_nc md =
  case md of
    Ch_Pan_1 -> 1
    Ch_PanAz_1 _ -> 1
    Ch_Direct nc -> nc

-- | Ordinary derivation
nc_to_ch_mode :: Int -> Ch_Mode
nc_to_ch_mode nc =
  case nc of
    1 -> Ch_Pan_1
    2 -> Ch_Direct nc
    _ -> error "nc_to_ch_mode?"

{- | All Ch_Mode for given /nc/.

>>> ch_mode_univ 2
[Ch_Pan_1,Ch_Direct 1,Ch_Direct 2]
-}
ch_mode_univ :: Int -> [Ch_Mode]
ch_mode_univ nc = Ch_Pan_1 : map Ch_Direct [1 .. nc]

-- | 0-indexed list of sound-file channels.
type Sf_Chan = [Int]

-- | nc to channel-list.
nc_to_sf_chan :: Int -> Sf_Chan
nc_to_sf_chan nc = take nc [0 ..]

-- * Gate-Duration-Loop

-- | Loop mode: none = no loop, buf = entire buffer, seg = indicated segment
data Lp_Mode = Lp_None | Lp_Buf | Lp_Seg
  deriving (Eq, Show, Read)

-- | End mode: dur = indicated duration, gate = user trigger, buf = end of buffer
data En_Mode = En_Dur | En_Gate | En_Buf
  deriving (Eq, Show, Read)

-- * Envelope

-- | env_aT = attack time, env_rT = release time, env_aC = attack curve, env_rC = release curve
type Ar_Env = (Double, Double, Double, Double)

-- * Math

-- | Real
type R = Double

{- | Ranges are /inclusive/.

>>> range_to_degree (0,5)
6
-}
range_to_degree :: Num t => (t, t) -> t
range_to_degree (p, q) = q - p + 1

{- | Ranges are /inclusive/.

>>> range_to_gamut (0,5)
[0,1,2,3,4,5]
-}
range_to_gamut :: Enum t => (t, t) -> [t]
range_to_gamut (p, q) = [p .. q]

-- | First and last elements of ascending list.
seq_to_range :: [x] -> (x, x)
seq_to_range l = (List.head_err l, List.last_err l)

{- | Is x in (inclusive) range.

>>> map (in_range (8,9)) [7 .. 10]
[False,True,True,False]
-}
in_range :: Ord a => (a, a) -> a -> Bool
in_range (l, r) x = x >= l && x <= r

-- * Stored

{- | Midi key numbers that map to each stored tone.

>>> stored_to_rng [60,62,64,65,67]
[(60,61),(62,63),(64,64),(65,66),(67,67)]
-}
stored_to_rng :: [Midi.Key] -> [(Midi.Key, Midi.Key)]
stored_to_rng x = zip x (map (subtract 1) (List.tail_err x)) ++ [(List.last_err x, List.last_err x)]

-- * Midi

{- | If a note is not in range, shift until it is in range and give octave shift.

>>> map (fold_midi_oct (59,87)) (zip (repeat 0) [50.5,58.5,60.5,90.5])
[(-1.0,62.5),(-1.0,70.5),(0.0,60.5),(1.0,78.5)]
-}
fold_midi_oct :: (Ord n, Num n, Fractional r) => (n, n) -> (r, n) -> (r, n)
fold_midi_oct (l, r) (o, mn) =
  if mn < l && mn + 12 <= r
    then fold_midi_oct (l, r) (o - 1, mn + 12)
    else
      if mn > r && mn - 12 >= l
        then fold_midi_oct (l, r) (o + 1, mn - 12)
        else (o, mn)

-- * Line

-- | Line generator, ie. 'line' for linear, 'xLine' for exponential.
type Line_f = Sc3.Rate -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.DoneAction Sc3.Ugen -> Sc3.Ugen

-- | kr controls /nm0/ and /nm1/ and a connecting /gen_f/ Sc3.Ugen of /dur/ seconds.
k_pair_gen :: Line_f -> Sc3.Ugen -> String -> R -> Sc3.Ugen
k_pair_gen gen_f dur nm def =
  let start = Sc3.control Sc3.kr (nm ++ "0") def
      end = Sc3.control Sc3.kr (nm ++ "1") def
  in gen_f Sc3.ar start end dur Sc3.DoNothing

-- | 'k_pair_gen' of 'line'
k_pair_lin :: Sc3.Ugen -> String -> R -> Sc3.Ugen
k_pair_lin = k_pair_gen Sc3.line

-- | 'k_pair_gen' of 'xLine'
k_pair_exp :: Sc3.Ugen -> String -> R -> Sc3.Ugen
k_pair_exp = k_pair_gen Sc3.xLine

-- * Sound-File

{- | sr = sample-rate, st = start time (sec.), du = duration (sec.)

>>> sf_sample_loc_frames 100 1 2
(100,200)
-}
sf_sample_loc_frames :: Integral i => Double -> Double -> Double -> (i, i)
sf_sample_loc_frames sr st du = (round (st * sr), round (du * sr))

{- | Sample indices for each tone in sequence.

>>> sf_sample_bounds 100 1 3 (0.1,0.2)
[(110,80),(210,80),(310,80)]
-}
sf_sample_bounds :: (Integral k, Integral i) => Double -> Double -> k -> (Double, Double) -> [(i, i)]
sf_sample_bounds sr du k (st_d, en_d) =
  let st = map (+ st_d) [du, du * 2 ..]
  in genericTake k (zipWith (sf_sample_loc_frames sr) st (repeat (du - en_d)))

{- | Transform list of zero-crossings [st,en,st,en...] into (st,en) pairs.

>>> sf_from_raw_en "abcdef"
[('a','b'),('c','d'),('e','f')]
-}
sf_from_raw_en :: [t] -> [(t, t)]
sf_from_raw_en =
  let f l = case l of
        [p, q] -> (p, q)
        _ -> error "sf_from_raw?"
  in map f . Split.chunksOf 2

lp_to_du :: Num b => (b, b) -> (b, b)
lp_to_du (st, en) = (st, en - st + 1)

sf_from_raw_du :: Num n => [n] -> [(n, n)]
sf_from_raw_du = map lp_to_du . sf_from_raw_en

-- * Zc/Raw Data

-- | For each index give the location of the next zero crossing.
sf_zero_crossings :: FilePath -> [Int] -> IO [Int]
sf_zero_crossings fn ix = do
  (_hdr, vec) <- Sf.read_vec_f32 fn
  return (map (fromJust . Vector.vec_next_zc vec) ix)

-- | For each index give the location of the next n zero crossings.
sf_zero_crossings_n :: Int -> FilePath -> [Int] -> IO [[Int]]
sf_zero_crossings_n n fn ix = do
  (_hdr, vec) <- Sf.read_vec_f32 fn
  return (map (Vector.vec_next_zc_seq n vec) ix)

-- * Io

print_stderr :: Show s => s -> IO ()
print_stderr = hPutStrLn stderr . show

-- * Load

type LoopPoints = (Int, Int)

-- | Message to allocate buffer and read file.
ld_buf_msg :: Sf_Chan -> (Sc3.Buffer_Id, FilePath, Maybe LoopPoints) -> Osc.Message
ld_buf_msg ch (ix, fn, lp) =
  let (st, en) = maybe (0, 0) lp_to_du lp
  in Sc3.b_allocReadChannel ix fn st en ch

ld_or_send_buf_msg :: Sf_Chan -> (Sc3.Buffer_Id, FilePath, Maybe LoopPoints) -> IO Osc.Message
ld_or_send_buf_msg ch (ix, fn, lp) = do
  let (st, en) = maybe (0, 0) lp_to_du lp
  MemCpy.b_allocReadOrSendChannel ix fn st en ch

-- | Given channel list and an ordered sequence of filenames make sample loader.
au_loader :: Sf_Chan -> [FilePath] -> Sc3.Buffer_Id -> [Osc.Message]
au_loader ch nm_seq b0 = map (ld_buf_msg ch) (zip3 [b0 ..] nm_seq (repeat Nothing))

au_loader_or_sender :: Sf_Chan -> [FilePath] -> Sc3.Buffer_Id -> IO [Osc.Message]
au_loader_or_sender ch nm_seq b0 = mapM (ld_or_send_buf_msg ch) (zip3 [b0 ..] nm_seq (repeat Nothing))

au_loader_lp :: Sf_Chan -> [FilePath] -> Sc3.Buffer_Id -> [Maybe LoopPoints] -> [Osc.Message]
au_loader_lp ch nm_seq b0 lp = map (ld_buf_msg ch) (zip3 [b0 ..] nm_seq lp)

au_loader_or_sender_lp :: Sf_Chan -> [FilePath] -> Sc3.Buffer_Id -> [Maybe LoopPoints] -> IO [Osc.Message]
au_loader_or_sender_lp ch nm_seq b0 lp = mapM (ld_or_send_buf_msg ch) (zip3 [b0 ..] nm_seq lp)

-- | Variant where all files are in the same directory, so given as directory and file list.
au_loader_dir :: Sf_Chan -> FilePath -> [FilePath] -> Sc3.Buffer_Id -> [Osc.Message]
au_loader_dir ch dir nm = au_loader ch (map (dir </>) nm)

-- * Pitch

{- | Mnn name

>>> map mnn_to_iso_sharp [36,86]
["C2","D6"]
-}
mnn_to_iso_sharp :: Pitch.Midi -> String
mnn_to_iso_sharp = Pitch.pitch_pp_iso . Pitch.midi_to_pitch (Pitch.pc_spell_sharp :: Pitch.Spelling Int)

range_octpc_to_midi :: (Pitch.OctPc, Pitch.OctPc) -> (Pitch.Midi, Pitch.Midi)
range_octpc_to_midi = let f = Pitch.octpc_to_midi in bimap f f

pc_names :: [String]
pc_names = words "C C# D D# E F F# G G# A A# B"

octpc_to_name :: Pitch.OctPc -> String
octpc_to_name (oct, pc) = (pc_names !! pc) ++ show oct

midi_to_name :: Pitch.Midi -> String
midi_to_name = octpc_to_name . Pitch.midi_to_octpc

is_gamut :: (Eq t, Enum t) => [t] -> Bool
is_gamut l = [List.head_err l .. List.last_err l] == l

gamut_to_range :: (Eq t, Enum t) => [t] -> (t, t)
gamut_to_range l =
  let n1 = List.head_err l
      n2 = List.last_err l
  in if [n1 .. n2] == l
      then (n1, n2)
      else error "gamut_to_range?"

gamut_pc_names :: [Pitch.Midi] -> [String]
gamut_pc_names = map midi_to_name

range_pc_names :: (Pitch.Midi, Pitch.Midi) -> [String]
range_pc_names = gamut_pc_names . range_to_gamut

-- * Voice

type Voice_Name = String

-- * Dyn

type Dynamic_Name = String

-- * Loop-Pt

{- | Read @smpl@ chunk of WAV file and return (nc,ws,st,en).
nc=number-of-channels, ws=word-size, st=start (bytes), en=end (bytes).

>>> fn = "/home/rohan/data/audio/instr/soni-musicae/Orgue-de-Salon/B-C1.wav"
>>> wave_read_lp fn
(2,2,43075521,43139599)
-}
wave_read_lp :: FilePath -> IO (Int, Int, Int, Int)
wave_read_lp fn = do
  ch <- Wave.wave_load_ch fn
  let hdr = Wave.wave_fmt_16_parse (Riff.find_chunk_err "fmt " ch)
      nc = Wave.numChannels hdr
      ws = Wave.bitsPerSample hdr `div` 8 -- word-size
  case Wave.wave_smpl_parse (Riff.find_chunk_err "smpl" ch) of
    (_, [[_, _, start, end, _, _]], _) -> return (fromIntegral nc, fromIntegral ws, fromIntegral start, fromIntegral end)
    _ -> error "wave_read_lp"

-- * Akt

{- | Function from (Ch,Mnn,Vel) to Maybe (Buffer-Id,Cents-Tuning)

This type does not specify if the Buffer-Id is:
relative, ie. zero-indexed for the current Voice, or
absolute, ie. an Sc3-Id
-}
type Akt_f = (Midi.Channel, Midi.Key, Midi.Velocity) -> Maybe (Sc3.Buffer_Id, Tuning.Cents)

{- | Given (Mnn,t) table lookup /k/ and return nearest t & cents-difference.

>>> map (\n -> akt_f_simple [(60,0),(62,1),(64,2),(65,3)] (0,n,127)) [59 .. 66]
[Just (0,-100.0),Just (0,0.0),Just (0,100.0),Just (1,0.0),Just (1,100.0),Just (2,0.0),Just (3,0.0),Just (3,100.0)]
-}
akt_f_simple :: [(Pitch.Midi, Sc3.Buffer_Id)] -> Akt_f
akt_f_simple tbl (_, k, _) =
  let (n, b) = List.find_nearest_by fst True tbl k
  in Just (b, (fromIntegral k - fromIntegral n) * 100)

-- | Table from Mnn to (Buffer-Id,Cents-Tuning)
type Akt_Tbl = [(Pitch.Midi, (Sc3.Buffer_Id, Tuning.Cents))]

{- | Atk f c-diff

>>> map (\n -> akt_f_cdiff [(60,(0,-10)),(62,(1,-5)),(64,(2,5)),(65,(3,10))] (0,n,127)) [59 .. 66]
[Just (0,-90.0),Just (0,10.0),Just (0,110.0),Just (1,5.0),Just (1,105.0),Just (2,-5.0),Just (3,-10.0),Just (3,90.0)]
-}
akt_f_cdiff :: Akt_Tbl -> Akt_f
akt_f_cdiff tbl (_, k, _) =
  let (n, (b, c)) = List.find_nearest_by fst True tbl k
  in Just (b, ((fromIntegral k - fromIntegral n) * 100) - c)

-- | Range
akt_f_rng :: (Midi.Key, Midi.Key) -> Akt_f
akt_f_rng (k0, k1) (_, n, _) = if n >= k0 && n <= k1 then Just (n - k0, 0) else Nothing

-- | Re-write akt_f with a pre-folding stage
akt_f_rw_fold :: (Midi.Key, Midi.Key) -> Akt_f -> Akt_f
akt_f_rw_fold rng akt_f (ch, mnn, vel) =
  let (oct, mnn') = fold_midi_oct rng (0, mnn)
      f (b, dt) = (b, dt + (oct * 1200))
  in fmap f (akt_f (ch, mnn', vel))

{- | Range/Fold

This rule doesn't select the nearest buffer,
notes outside the range do not all resample the same recording.
-}
akt_f_rng_fold :: (Pitch.Midi, Pitch.Midi) -> Akt_f
akt_f_rng_fold rng = akt_f_rw_fold rng (akt_f_rng rng)

akt_f_rw_b0 :: Sc3.Buffer_Id -> Akt_f -> Akt_f
akt_f_rw_b0 b0 f = let rw (b, dt) = (b0 + b, dt) in fmap rw . f

-- * Akt-F/Calc

{- | Calculate @bufnum@ and @rate@ parameters.
rt_mul = rate multiplier,
akt_f = derivation function,
ch = channel,
fmnn = fractional midi note number,
fvel = fractional velocity.
-}
smplr_calc_rate :: Double -> Akt_f -> (Midi.Channel, Double, Double) -> Sc3.Param
smplr_calc_rate rt_mul akt_f (ch, fmnn, fvel) =
  let mnn = round fmnn
      (b, dt) = fromJust (akt_f (ch, mnn, floor fvel))
  in if b < 0 -- should this enforce dt limit?
      then error "smplr_calc_rate?"
      else
        [ ("bufnum", fromIntegral b)
        , ("rate", Sc3.midi_to_ratio ((dt / 100) + (fmnn - fromIntegral mnn)) * rt_mul)
        ]

-- * Sfz

sfz_ctl_dir :: FilePath -> Sfz.Sfz_Section
sfz_ctl_dir dir = ("<control>", [("default_path", dir)])

sfz_grp_lm :: String -> Sfz.Sfz_Section
sfz_grp_lm md = ("<group>", [("loop_mode", md)])

sfz_int_pp :: Int -> String
sfz_int_pp = show

sfz_i8_pp :: Int8 -> String
sfz_i8_pp = show

sfz_f64_pp :: Double -> String
sfz_f64_pp = Sc3.double_pp 4

sfz_grp_rT :: Double -> Sfz.Sfz_Section
sfz_grp_rT rT = ("<group>", [("ampeg_release", sfz_f64_pp rT)])

sfz_sec_add :: Sfz.Sfz_Section -> [Sfz.Sfz_Opcode] -> Sfz.Sfz_Section
sfz_sec_add (h, o2) o1 = (h, o1 ++ o2)

sfz_grp_rT_vol :: (Double, Double) -> Sfz.Sfz_Section
sfz_grp_rT_vol (rT, vol) = sfz_sec_add (sfz_grp_rT rT) [("volume", sfz_f64_pp vol)]

{- | <https://sfzformat.com/opcodes/amp_velcurve_N>
"Set amp_velcurve_N=1 with N being equal to the hivel value for each layer"
-}
sfz_grp_rT_vel :: (Double, (Midi.Velocity, Midi.Velocity)) -> Sfz.Sfz_Section
sfz_grp_rT_vel (rT, (v1, v2)) =
  let vel = [("lovel", sfz_int_pp v1), ("hivel", sfz_int_pp v2), (printf "amp_velcurve_%d" v2, "1")]
  in sfz_sec_add (sfz_grp_rT rT) vel

sfz_region_1 :: (FilePath, Midi.Key, Maybe LoopPoints) -> Sfz.Sfz_Section
sfz_region_1 (nm, mnn, lp) =
  let lp_op (st, en) = [("loop_start", sfz_int_pp st), ("loop_end", sfz_int_pp en)]
  in ("<region>", [("sample", nm), ("key", sfz_int_pp mnn)] ++ maybe [] lp_op lp)

sfz_region_1_seq :: [FilePath] -> [Midi.Key] -> Maybe [LoopPoints] -> [Sfz.Sfz_Section]
sfz_region_1_seq nm mnn lp = map sfz_region_1 (zip3 nm mnn (maybe (repeat Nothing) (map Just) lp))

sfz_region_rng :: (FilePath, (Midi.Key, Midi.Key)) -> Sfz.Sfz_Section
sfz_region_rng (nm, (k1, k2)) =
  ( "<region>"
  ,
    [ ("sample", nm)
    , ("pitch_keycenter", sfz_int_pp k1)
    , ("lokey", sfz_int_pp k1)
    , ("hikey", sfz_int_pp k2)
    ]
  )

sfz_region_prt_seq :: [FilePath] -> [Midi.Key] -> [Sfz.Sfz_Section]
sfz_region_prt_seq nm mnn = map sfz_region_rng (zip nm (stored_to_rng mnn))

sfz_region_prt_tun_seq :: [FilePath] -> [(Midi.Key, Int8)] -> [Sfz.Sfz_Section]
sfz_region_prt_tun_seq nm_seq sq =
  let f nm ((k1, k2), t) =
        let (hdr, opc) = sfz_region_rng (nm, (k1, k2))
        in (hdr, ("tune", sfz_i8_pp t) : opc)
      dat = zip (stored_to_rng (map fst sq)) (map snd sq)
  in zipWith f nm_seq dat

-- * Cmd

type Cmd = (String, [String])

run_cmd :: Cmd -> IO ()
run_cmd = uncurry Process.callProcess

-- * Extract

-- | Requires /dir/ exist.
sf_extract_tone_cmd :: Int -> Maybe FilePath -> FilePath -> (Midi.Key, (Int, Int)) -> Cmd
sf_extract_tone_cmd nc subdir fn (mnn, (st, du)) =
  let en = st + du - 1
      (dir, typ) = splitExtension fn
      out_fn = dir </> fromMaybe "" subdir </> show mnn <.> typ
  in ("hsc3-sf-extract", [fn, show nc, show st, show en, show du, out_fn])

sf_extract_tone_set_cmd :: Int -> Maybe FilePath -> FilePath -> [(Midi.Key, (Int, Int))] -> [Cmd]
sf_extract_tone_set_cmd nc subdir fn dat =
  ("mkdir", ["-p", dropExtension fn </> fromMaybe "" subdir])
    : map (sf_extract_tone_cmd nc subdir fn) dat

-- * Tun

-- | Monadic version of fmap
fmap_m :: Monad m => (t -> m u) -> Maybe t -> m (Maybe u)
fmap_m f = maybe (return Nothing) (fmap Just . f)

tun_tbl_load :: Maybe FilePath -> IO (Maybe Tuning.Midi.Mnn_Fmnn_Table)
tun_tbl_load = fmap_m Tuning.Midi.mnn_fmnn_table_load_csv

tun_tbl_load_null :: FilePath -> IO (Maybe Tuning.Midi.Mnn_Fmnn_Table)
tun_tbl_load_null fn = tun_tbl_load (if null fn then Nothing else Just fn)
