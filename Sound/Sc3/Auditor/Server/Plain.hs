{- | Plain

Minimal state and decoding.
-}
module Sound.Sc3.Auditor.Server.Plain where

import Control.Concurrent {- base -}
import Control.Exception {- base -}
import Control.Monad {- base -}

import qualified Data.Map as Map {- containers -}

import Sound.Osc.Fd {- hosc -}
import Sound.Sc3.Fd {- hsc3 -}

import Sound.Sc3.Data.Math.Types {- hsc3-data -}

import Sound.Midi.Common {- midi-osc -}

import qualified Sound.Midi.Csv as Csv {- midi-osc -}
import qualified Sound.Midi.Ky as Ky {- midi-osc -}
{- midi-osc -}

import qualified Sound.Midi.Osc.Server as Server {- midi-osc -}
import qualified Sound.Midi.Type as M

linlin_u8 :: (F64, F64) -> U8 -> F64
linlin_u8 r x = linlin_hs (0, 127) r (u8_to_f64 x)

pw_f64 :: (U8, U8) -> F64
pw_f64 = u16_to_f64 . bits_7_join_le

-- | Given user (state, midi-note-number) calculate (state,fractional-midi-note-number).
type Tun_F usr = (usr, U8) -> (usr, F64)

-- | Function to calculate s_new and any associated messages.
type Msg_F usr = (U4, F64, F64) -> (String, Synth_Id, Group_Id, Param, usr) -> (Synth_Id, usr, [Message])

{- | State

cc = control-change (CVM-0xB)
  bnk = bank-select = 0x00
  mw = modulation-wheel = 0x01
  vol = volume = 0x07
  pan = 0x0A
  exp = expression-controller = 0x0B
  sus = sustain-pedal = 0x40
  rT = release-time = 0x48
  aT = attack-time = 0x49
  pwD = detune-depth = 0x5E
prg = program-change (CVM-0xC)
at = channel-aftertouch (CVM-0xD)
pw = pitch-wheel (CVM-0xE)


st_cc & sus_id:
st_cc 0x40 indicates the state of the sustain pedal.
If a note is released while the sustain pedal is down,
the note is removed from the key-map and the ID is added to sus_id.
When the sustain pedal is released all nodes sus_id are released.
-}
data St t = St
  { st_syn :: String -- synthdef name
  , st_grp :: Group_Id -- Sc3 server group index
  , st_ky :: Ky.Ky (M.Channel, U8) -- event -> id
  , st_at :: U8 -- channel-aftertouch (0xD)
  , st_prg :: U8 -- program-change (0xC)
  , st_pw :: (U8, U8) -- pitch-wheel (0xE)
  , st_cc :: Map.Map U8 U8 -- CC
  , st_sus_id :: [Node_Id] -- nodes to be released with sustain pedal
  , st_vel_rng :: (F64, F64) -- key-velocity range
  , st_at_rng :: (F64, F64) -- channel-aftertouch range
  , st_pw_rng :: (F64, F64) -- pitch-wheel range
  , st_vol_rng :: (F64, F64) -- volume range
  , st_mw_rng :: (F64, F64) -- mod-wheel range
  , st_pan_rng :: (F64, F64) -- pan range
  , st_exp_rng :: (F64, F64) -- exp range
  , st_t0 :: Time -- t0 = time-stamp epoch
  , st_tun :: Tun_F t -- tuning-function
  , st_msg :: Msg_F t -- s_new (&etc.) message function
  , st_usr :: t -- user-state
  }

def_tun_f :: Tun_F t
def_tun_f (usr, x) = (usr, fromIntegral x)

tun_f_from_tbl :: [(U8, F64)] -> Tun_F t
tun_f_from_tbl tbl (st, mnn) =
  let m = Map.fromList tbl
  in (st, Map.findWithDefault (fromIntegral mnn) mnn m)

def_msg_f :: Msg_F t
def_msg_f _ (syn, nid, grp, param, usr) = (nid, usr, [s_new syn nid AddToTail grp param])

st_pw_u16 :: St t -> U16
st_pw_u16 = bits_7_join_le . st_pw

st_pw_in_rng :: St t -> F64
st_pw_in_rng st = linlin_hs (0, 0x3FFF) (st_pw_rng st) (pw_f64 (st_pw st))

st_at_in_rng :: St t -> F64
st_at_in_rng st = linlin_u8 (st_at_rng st) (st_at st)

st_cc_get :: U8 -> St t -> U8
st_cc_get k st = Map.findWithDefault 0 k (st_cc st)

-- | CC-01 = mod-wheel
st_cc_mw_u8 :: St t -> U8
st_cc_mw_u8 st = st_cc_get 0x01 st

st_cc_mw_in_rng :: St t -> F64
st_cc_mw_in_rng st = linlin_u8 (st_mw_rng st) (st_cc_mw_u8 st)

-- | CC-07 = volume
st_cc_vol_u8 :: St t -> U8
st_cc_vol_u8 st = st_cc_get 0x07 st

st_cc_vol_in_rng :: St t -> F64
st_cc_vol_in_rng st = linlin_u8 (st_vol_rng st) (st_cc_vol_u8 st)

-- | CC-0A = pan
st_cc_pan_u8 :: St t -> U8
st_cc_pan_u8 st = st_cc_get 0x0A st

st_cc_pan_in_rng :: St t -> F64
st_cc_pan_in_rng st = linlin_u8 (st_pan_rng st) (st_cc_pan_u8 st)

-- | CC-0B = exp = expression-controller
st_cc_exp_u8 :: St t -> U8
st_cc_exp_u8 st = st_cc_get 0x0B st

st_cc_exp_in_rng :: St t -> F64
st_cc_exp_in_rng st = linlin_u8 (st_exp_rng st) (st_cc_exp_u8 st)

-- | CC-40 = sustain pedal
st_cc_sus_u8 :: St t -> U8
st_cc_sus_u8 = st_cc_get 0x40

-- | (synthdef-name,group-id,node-id,usr)
type St_Opt t = (String, Group_Id, Node_Id, t)

-- | The rng values are set to MIDI values, ie. (0,127).
st_init_midi :: St_Opt t -> St t
st_init_midi (syn, grp, n0, usr) =
  let ky = Ky.ky_init n0
      cc = Map.fromList [(0x01, 0x00), (0x07, 0x7F), (0x0A, 0x40), (0x0B, 0x7F)]
      pw = (0, 0x40) -- ie. MID-POINT, ie. ZERO
  in St
      syn
      grp
      ky
      0
      0
      pw
      cc
      []
      (0, 0x7F)
      (0, 0x7F)
      (0, 0x3FFF)
      (0, 0x7F)
      (0, 0x7F)
      (0, 0x7F)
      (0, 0x7F) -- vel at pw vol mw pan exp
      (-1)
      def_tun_f
      def_msg_f
      usr

-- | As above but the rng values are set to floating-point values, ie. (0,1) or (-1,1).
st_init_fp :: St_Opt t -> St t
st_init_fp opt =
  (st_init_midi opt)
    { st_vel_rng = (0, 1)
    , st_at_rng = (0, 1)
    , st_pw_rng = (-1, 1)
    , st_vol_rng = (0, 1)
    , st_mw_rng = (0, 1)
    , st_pan_rng = (-1, 1)
    , st_exp_rng = (0, 1)
    }

n_set_sq :: [(String, F64)] -> [Int] -> [Message]
n_set_sq m = map (\i -> n_set i m)

send_msgs :: Transport t => t -> [Message] -> IO ()
send_msgs fd m = when (not (null m)) (sendBundle fd (bundle immediately m))

print_csv_mnd :: Time -> M.Channel_Voice_Message U8 -> IO ()
print_csv_mnd t0 m = do
  t <- time
  maybe (return ()) putStrLn (Csv.cvm_to_csv_mnd (t - t0) m)

recv_f :: Server.Midi_Recv_f (MVar (St t)) U8
recv_f fd mvar_st midi_msg = do
  st <- readMVar mvar_st
  let edit_st x = swapMVar mvar_st x >> return mvar_st
      off_f ch mnn = do
        let (ky, z) = Ky.ky_free1_err (st_ky st) (ch, mnn)
            sus_id = (if st_cc_sus_u8 st >= 64 then (z :) else id) (st_sus_id st)
        when (st_cc_sus_u8 st < 64) (sendMessage fd (n_set1 z "gate" 0))
        edit_st (st {st_ky = ky, st_sus_id = sus_id})
      on_f ch mnn vel = do
        let (ky0, z0) = Ky.ky_alloc1 (st_ky st) (ch, mnn)
            usr0 = st_usr st
            (usr1, fmnn) = (st_tun st) (usr0, mnn)
            param =
              [ ("amp", linlin_u8 (st_vel_rng st) vel) -- scaled key-velocity
              , ("at", st_at_in_rng st) -- channel-aftertouch (0xD)
              , ("freq", midi_to_cps fmnn) -- cps
              , ("gate", 1)
              , ("mnn", fmnn) -- note-on (0x9) {fractional}
              , ("mw", st_cc_mw_in_rng st) -- cc-mw (mod-wheel 0x01)
              , ("pan", st_cc_pan_in_rng st) -- cc-pan (0x0A)
              , ("exp", st_cc_exp_in_rng st) -- cc-exp (expression-controller 0x0B)
              , ("bnk", u8_to_f64 (st_cc_get 0x00 st)) -- cc-bank-select (0x00)
              , ("prg", u8_to_f64 (st_prg st)) -- program-change (0xC)
              , ("pw", st_pw_in_rng st) -- pitch-wheel (0xE)
              , ("vel", u8_to_f64 vel) -- note-on (0x9)
              , ("vol", st_cc_vol_in_rng st) -- cc-vol (0x07)
              ]
            (z1, usr2, msg) = (st_msg st) (ch, fmnn, u8_to_f64 vel) (st_syn st, z0, st_grp st, param, usr1)
            ky1 = if z0 == z1 then ky0 else Ky.ky_reassign1 ky0 (ch, mnn) z1
        sendBundle fd (Bundle immediately msg)
        edit_st (st {st_ky = ky1, st_usr = usr2})
  print_csv_mnd (st_t0 st) midi_msg
  case midi_msg of
    M.Note_Off ch mnn _ -> off_f ch mnn -- 0x8=
    M.Note_On ch mnn vel -> on_f ch mnn vel -- 0x9
    M.Control_Change _ 0x01 d2 -> do
      -- MW = 0xB-0x01
      let mw = linlin_u8 (st_mw_rng st) d2
      sendMessage fd (n_set1 (st_grp st) "mw" mw)
      edit_st (st {st_cc = Map.insert 0x01 d2 (st_cc st)})
    M.Control_Change _ 0x07 d2 -> do
      -- VOL = 0xB-0x07
      let vol = linlin_u8 (st_vol_rng st) d2
      sendMessage fd (n_set1 (st_grp st) "vol" vol)
      edit_st (st {st_cc = Map.insert 0x07 d2 (st_cc st)})
    M.Control_Change _ 0x0A d2 -> do
      -- PAN = 0xB-0x0A
      let pan = linlin_u8 (st_pan_rng st) d2
      sendMessage fd (n_set1 (st_grp st) "pan" pan)
      edit_st (st {st_cc = Map.insert 0x0A d2 (st_cc st)})
    M.Control_Change _ 0x0B d2 -> do
      -- EXP = 0xB-0x0B
      let xpr = linlin_u8 (st_exp_rng st) d2
      sendMessage fd (n_set1 (st_grp st) "exp" xpr)
      edit_st (st {st_cc = Map.insert 0x0B d2 (st_cc st)})
    M.Control_Change _ 0x40 d2 -> do
      -- SUS = 0xB-0x40
      let sus_rel = d2 < 64 && st_cc_sus_u8 st >= 64
      when sus_rel (send_msgs fd (n_set_sq [("gate", 0)] (st_sus_id st)))
      edit_st
        ( st
            { st_cc = Map.insert 0x40 d2 (st_cc st)
            , st_sus_id = if sus_rel then [] else st_sus_id st
            }
        )
    -- Sostenuto_On_Off_T -- ^ 0x42
    -- Soft_Pedal_On_Off_T -- ^ 0x43
    M.Control_Change _ d1 d2 -> do
      -- CC-GENERA (BNK = 0x00)
      edit_st (st {st_cc = Map.insert d1 d2 (st_cc st)})
    M.Program_Change _ d1 -> edit_st (st {st_prg = d1}) -- PRG = 0xC
    M.Channel_Aftertouch _ d1 -> edit_st (st {st_at = d1}) -- AT = 0xD
    M.Pitch_Bend _ d1 d2 -> do
      -- PW = 0xE
      let d = M.pitch_bend (st_pw_rng st) (d1, d2)
      sendMessage fd (n_set1 (st_grp st) "pw" d)
      edit_st (st {st_pw = (d1, d2)})
    _ -> return mvar_st

-- | group-id must exist.  node-id must not exist.
rt_midi_osc :: St t -> IO (IO (), MVar (St t))
rt_midi_osc st = do
  t0 <- time
  st_m <- newMVar (st {st_t0 = t0})
  let act = finally (Server.run_midi (\_ -> return st_m) recv_f) (putStrLn "rt_midi_osc: end")
  return (act, st_m)

-- | Run rt_midi_osc process using forkIO and return ThreadId
rt_midi_osc_th :: St t -> IO (ThreadId, MVar (St t))
rt_midi_osc_th st = do
  (act, st_m) <- rt_midi_osc st
  th <- forkIO act
  return (th, st_m)
