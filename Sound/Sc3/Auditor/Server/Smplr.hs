-- | historical - Fcd/Stp
module Sound.Sc3.Auditor.Server.Smplr where

import qualified Control.Concurrent as Concurrent {- base -}
import Control.Exception {- base -}
import Control.Monad {- base -}
import Data.List {- base -}
import qualified Data.Map as Map {- containers -}
import Data.Maybe {- base -}
import System.Environment {- base -}
import System.Random {- random -}

import qualified Music.Theory.List as T {- hmt -}
import qualified Music.Theory.Tuning as T {- hmt -}
import qualified Music.Theory.Tuning.Load as T {- hmt -}
import qualified Music.Theory.Tuning.Midi as T {- hmt -}

import qualified Sound.Osc.Fd as Osc {- hosc -}
import qualified Sound.Osc.Transport.Fd.Udp as Osc {- hosc -}
import qualified Sound.Sc3.Fd as Sc3 {- hsc3 -}

import qualified Sound.Osc.Datum.Unpack as U {- hosc-util -}

{- midi-osc -}
import qualified Sound.Midi.Csv as Csv {- midi-osc -}
{- midi-osc -}

import qualified Sound.Midi.Ky as Ky {- midi-osc -}
import qualified Sound.Midi.Osc.Server as Server {- midi-osc -}
import Sound.Midi.Type
import qualified Sound.Midi.Type as M

import qualified Sound.Sc3.Auditor.Common as Common {- hsc3-auditor -}
import qualified Sound.Sc3.Auditor.Smplr as Smplr {- hsc3-auditor -}

-- * Smplr

{- | Sampler options

syn = maybe synthdef name, else derived
rng = range of stored tones (midi-note-numbers)
akt_f = function to allow incomplete sample-sets
ch = channel assignment mode (ch | pn)
en = end mode
nid = node id
b0 = buffer zero
env = (attack time,attack curve,release time,release curve)
bus = output bus
grp = group to allocate node at
p2 = further synthesis parameters
lp = loop
-}
type Smplr_Opt =
  ( Maybe String
  , (M.Key, M.Key)
  , Common.Akt_f
  , Common.Ch_Mode
  , Common.En_Mode
  , Sc3.Node_Id
  , Sc3.Buffer_Id
  , Common.Ar_Env
  , Sc3.Bus_Id
  , Sc3.Group_Id
  , Sc3.Param
  , Common.Lp_Mode
  )

{-
smplr_opt_to_param :: Smplr_Opt -> Param
smplr_opt_to_param (_rng,_akt,_ch,_nid,_b0,env,bus,_grp,p2,lp) =
    let lp' = fmap (\x -> ("loop",u_constant_err (from_loop x))) lp
        env = (env_aT,env_rT,env_aC,env_rC)
        p1 = [("attackTime",env_aT)
             ,("attackCurve",env_aC)
             ,("releaseTime",env_rT)
             ,("releaseCurve",env_rC)
             ,("bus",fromIntegral bus)
             ]
    in param_merge (mcons lp' p1) p2
-}

-- | opt ((ch,mnn,vel),dt) maybe_dur (amp,vol,exp)
type Smplr_Fn = Smplr_Opt -> ((Channel, Key, Velocity), Double) -> Maybe Double -> (Double, Double, Double) -> Osc.Message

-- | Stop identifier...
type Stop_Id = Int

{- | Calculate buffer index and read rate.  Once the requested mnn is
folded into the stored range, find the distance (possibly zero) in
semitones of the nearest (left-most) stored tone.  /b0/ is buffer
zero of the /current/ stop.
-}
smplr_calc :: (Key, Key) -> Common.Akt_f -> Sc3.Buffer_Id -> ((Channel, Key, Velocity), Double) -> (Sc3.Buffer_Id, Double)
smplr_calc rng akt_f b0 ((ch, mnn, vel), dt) =
  let (o, mnn') = Common.fold_midi_oct rng (0, mnn)
      (mnn'', dt') = fromJust (akt_f (ch, mnn', vel))
  in (b0 + mnn'', (2 ** o) * T.cents_to_fratio (dt + dt'))

int_to_double :: Int -> Double
int_to_double = fromIntegral

{- | Make @smplr@ control 'Message'.

mnn = midi note number, dt = detune (cents), du = duration, g = gain (velocity), v = volume

When parameters are merged the calculated parameters are prioritised over the Smplr_Opt ones.
-}
smplr_msg :: Smplr_Fn
smplr_msg (syn, rng, akt_f, ch_mode, en, nid, b0, env, bus, grp, p2, lp) ((ch, mnn, vel), dt) du (amp, vol, expr) =
  let (b, rt) = smplr_calc rng akt_f b0 ((ch, mnn, vel), dt)
      sus = fmap (\du' -> ("sustain", realToFrac du')) du
      lp_ctl = case lp of
        Common.Lp_None -> Nothing
        _ -> Just ("loop", 1)
      (aT, rT, aC, rC) = env
      p1 =
        [ ("bufnum", int_to_double b)
        , ("rate", rt)
        , ("amp", amp)
        , ("vol", vol)
        , ("exp", expr)
        , ("aT", aT)
        , ("aC", aC)
        , ("rT", rT)
        , ("rC", rC)
        , ("bus", int_to_double bus)
        , ("startpos", 0) -- frames
        ]
      nm = maybe (Smplr.smplr_nm (ch_mode, en, lp)) id syn
  in Sc3.s_new nm nid Sc3.AddToHead grp (T.mcons lp_ctl (T.mcons sus (Sc3.param_merge_r p2 p1)))

-- | Environment variables.
type Env = [(String, String)]

-- | The parameter names that will be scanned for in the environment.
smplr_env_param :: [String]
smplr_env_param =
  [ "bus"
  , "tun"
  , "vol"
  , "exp" -- cc
  , "lfo_rate"
  , "lfo_depth" -- lfo
  , "aT"
  , "aC"
  , "rT"
  , "rC" -- envelope-generator
  , "r_amp"
  , "r_bus"
  , "r_delay"
  , "r_detune"
  , "r_pan" -- randomness
  ]

-- | Convert 'Env' to 'Param'.
smplr_env_to_param :: Env -> Sc3.Param
smplr_env_to_param =
  let f (k, v) = if k `elem` smplr_env_param then Just (k, read v) else Nothing
  in mapMaybe f

-- | 'smplr_env_to_param' of 'getEnvironment'.
smplr_read_env :: IO Sc3.Param
smplr_read_env = fmap smplr_env_to_param getEnvironment

-- * Server

{- | State

cc_sus & sus_id: cc_sus indicates the state of the sustain pedal.  If
a note is released while the sustain pedal is down, the note is
removed from the key-map and the ID is added to the sustain ID set.
When the sustain pedal is released all notes in the sustain ID set are
released.

pw = pitch-wheel, mw = modulation-wheel
-}
data St = St
  { st_key_map :: Ky.Ky (M.Channel, Key) -- event -> id
  , st_start_time :: Osc.Time -- t0 = time-stamp epoch
  , st_stops :: [Stop_Id] -- active stops
  , st_cc_vol :: Double -- CC-07 = volume (0,1)
  , st_amp_stop :: Map.Map Stop_Id Double -- voice/stop amplitude
  , st_cc_sus :: Bool -- CC-40 = sustain pedal (OFF/ON)
  , st_sus_id :: [Sc3.Node_Id] -- nodes to be released at pedal off
  , st_rgen :: StdGen -- random generator state
  , st_param :: Sc3.Param -- parameter state
  , st_tuning_ix :: Int -- tuning function index
  , st_tuning_coarse :: Double -- fractional midi-note-number
  , st_pw :: Double -- pitch-wheel (fractional midi-note-number)
  , st_cc_mw :: Double -- CC-01 = mod-wheel (0-1)
  , st_amp_tbl :: Map.Map Key Double -- per note amplitude table
  }
  deriving (Show)

type Init_Data = (Osc.Time, Sc3.Node_Id, StdGen, Sc3.Param, [Stop_Id])

st_init :: Init_Data -> St
st_init (t0, n0, g0, p, stp0) = St (Ky.ky_init n0) t0 stp0 1.0 Map.empty False [] g0 p 0 0.0 0.0 0.0 Map.empty

init_f :: Sc3.Group_Id -> Concurrent.MVar St -> Server.Midi_Init_f (Concurrent.MVar St)
init_f au_grp mvar_st sc3_fd = do
  mapM_ (Sc3.async sc3_fd) Smplr.smplr_recv_all_msg
  _ <- Concurrent.forkIO (osc_server (au_grp, sc3_fd) mvar_st)
  return mvar_st

-- | User options.
type Recv_Opt =
  ( Maybe String -- synthdef name
  , Sc3.Buffer_Id -- b0 = buffer zero
  , Sc3.Group_Id -- au_grp = auditor group (stop groups follow)
  , (Double, Double) -- ampl = amplitude range
  , Common.Ar_Env -- (aT,rT,aC,rC) = attack & release times & curves
  , Double -- pwD = pitch-wheel-depth (-pwD,+pwD)
  , Double -- mwD = mod-wheel-depth (0,mwD)
  , [Stop_Id] -- allowed stops (for program-change)
  , Stop_Id -> (Key, Key) -- rng = midi note number range
  , Stop_Id -> Common.Akt_f -- akt = stored sample midi note numbers
  , Common.Ch_Mode -- ch_mode = channel
  , Common.En_Mode -- en_mode = end
  , Common.Lp_Mode -- lp_mode = loop
  , Stop_Id -> Smplr_Fn -- smplr_f
  )

-- | If the stop is present, remove it, else add it.
edit_stops :: Stop_Id -> [Stop_Id] -> [Stop_Id]
edit_stops x l = if x `elem` l then delete x l else x : l

st_toggle_stops :: St -> Stop_Id -> St
st_toggle_stops st k = st {st_stops = edit_stops k (st_stops st)}

st_set_stops :: St -> [Stop_Id] -> St
st_set_stops st k = st {st_stops = k}

n_set_sq :: [(String, Double)] -> [Int] -> [Osc.Message]
n_set_sq m = map (\i -> Sc3.n_set i m)

send_msgs :: Osc.Transport t => t -> [Osc.Message] -> IO ()
send_msgs sc3_fd m = when (not (null m)) (Osc.sendBundle sc3_fd (Osc.bundle Osc.immediately m))

print_csv_mnd :: Osc.Time -> M.Channel_Voice_Message Int -> IO ()
print_csv_mnd t0 m = do
  t <- Osc.time
  maybe (return ()) putStrLn (Csv.cvm_to_csv_mnd (t - t0) m)

type Recv_f = Recv_Opt -> [T.Sparse_Midi_Tuning_St_f StdGen] -> Server.Midi_Recv_f (Concurrent.MVar St) Int

ch_to_int :: M.Channel -> Int
ch_to_int = fromIntegral

recv_core_f :: Recv_f
recv_core_f opt tn_f sc3_fd mvar_st midi_msg = do
  st <- Concurrent.readMVar mvar_st
  let edit_mvar_st x = Concurrent.swapMVar mvar_st x >> return mvar_st
      (syn, b0, au_grp, ampl, env, pwD, _mwD, _stp_l, rng, akt_f, ch_mode, en_mode, lp_mode, smplr_f) = opt
      St ky t0 stp cc_vol amp_stp cc_sus sus_id rgen p2 tn_ix tn_crs pw cc_mw amp_tbl = st
      mk_a = Sc3.linlin_hs (0, 1) ampl . (/ 127) . fromIntegral
      rmv ch mnn = do
        let (ky', z) = Ky.ky_free_nil ky (ch, mnn)
            sus_id' = if cc_sus then z ++ sus_id else sus_id
        when (not cc_sus) (send_msgs sc3_fd (n_set_sq [("gate", 0)] z))
        edit_mvar_st (st {st_key_map = ky', st_sus_id = sus_id'})
      gen ch mnn vel =
        case (cycle tn_f !! tn_ix) rgen mnn of
          (_, Nothing) -> return mvar_st
          (rgen', Just (mnn', dt)) -> do
            let (ky', z) = Ky.ky_alloc_nil ky (ch, mnn) (length stp)
                p2' =
                  [ ("pw", pw)
                  , ("tun", tn_crs)
                  , ("mw", cc_mw)
                  ]
                    ++ p2
                smplr_opt z' stp_x =
                  let grp = au_grp + 1 + stp_x
                  in (syn, rng stp_x, akt_f stp_x, ch_mode, en_mode, z', b0, env, 0, grp, p2', lp_mode)
                gen_msg (z', stp_x) =
                  let amp = mk_a vel * Map.findWithDefault 1.0 mnn amp_tbl
                      v = (amp, cc_vol, Map.findWithDefault 1.0 stp_x amp_stp)
                  in smplr_f stp_x (smplr_opt z' stp_x) ((ch, mnn', vel), dt) Nothing v
            send_msgs sc3_fd (map gen_msg (zip z stp))
            edit_mvar_st (st {st_key_map = ky', st_rgen = rgen'})
  print_csv_mnd t0 midi_msg
  case midi_msg of
    M.Note_Off ch mnn _ -> rmv ch mnn
    M.Note_On ch mnn vel -> gen ch mnn vel
    M.Pitch_Bend _ d1 d2 ->
      let d = M.pitch_bend (-pwD, pwD) (d1, d2)
      in Osc.sendMessage sc3_fd (Sc3.n_set1 au_grp "pw" d)
          >> edit_mvar_st (st {st_pw = d})
    _ -> return mvar_st

recv_program_change_f :: Recv_f
recv_program_change_f opt _tn_f _sc3_fd mvar_st midi_msg = do
  let (_, _, _, _, _, _, _, stp_l, _, _, _, _, _, _) = opt
  case midi_msg of
    M.Program_Change _ k ->
      if k `elem` stp_l
        then Concurrent.modifyMVar_ mvar_st (\st -> return (st_set_stops st [k])) -- st_toggle_stops
        else return ()
    _ -> return ()
  return mvar_st

recv_std_cc_f :: Recv_Opt -> [T.Sparse_Midi_Tuning_St_f StdGen] -> Server.Midi_Recv_f (Concurrent.MVar St) Int
recv_std_cc_f opt _tn_f sc3_fd mvar_st midi_msg = do
  st <- Concurrent.readMVar mvar_st
  let (_, _, au_grp, _, _, _, mwD, _, _, _, _, _, _, _) = opt
      edit_mvar_st x = Concurrent.swapMVar mvar_st x >> return mvar_st
  case midi_msg of
    M.Control_Change _ 0x01 k ->
      -- Modulation Wheel
      let mw = (fromIntegral k / 127.0) * mwD
      in Osc.sendMessage sc3_fd (Sc3.n_set1 au_grp "mw" mw)
          >> edit_mvar_st (st {st_cc_mw = mw})
    M.Control_Change _ 0x07 d2 ->
      -- Volume
      let vol = fromIntegral d2 / 127.0
      in Osc.sendMessage sc3_fd (Sc3.n_set1 au_grp "vol" vol)
          >> edit_mvar_st (st {st_cc_vol = vol})
    M.Control_Change _ 0x40 k ->
      -- Sustain; 0 to 63 = Off, 64 to 127 = On
      if k < 64 && st_cc_sus st
        then
          send_msgs sc3_fd (n_set_sq [("gate", 0)] (st_sus_id st))
            >> edit_mvar_st (st {st_cc_sus = False, st_sus_id = []})
        else edit_mvar_st (st {st_cc_sus = True}) -- sus_id == []
    _ -> return mvar_st

chain_f :: [Recv_f] -> Recv_f
chain_f f_seq opt tn_f fd st midi_msg =
  case f_seq of
    [] -> return st
    f : f_seq' -> do
      st' <- f opt tn_f fd st midi_msg
      chain_f f_seq' opt tn_f fd st' midi_msg

recv_f :: Recv_f
recv_f = chain_f [recv_core_f, recv_program_change_f, recv_std_cc_f]

{- | (syn-name,
      tuning-type,
      buffer-zero,(p-group,au-group),node-zero,
      [tuning-name],tuning-param-1,tuning-param-2
      ampl-rng,envelope,pitch-wheel-depth,mod-wheel-depth,param,
      initial-stop-list,en-mode)
-}
type Server_Opt_A =
  ( Maybe String
  , String
  , Sc3.Buffer_Id
  , (Sc3.Group_Id, Sc3.Group_Id)
  , Sc3.Node_Id
  , [FilePath]
  , Double
  , Int
  , (Double, Double)
  , Common.Ar_Env
  , Double
  , Double
  , Sc3.Param
  , [Stop_Id]
  , Common.En_Mode
  )

-- | (stop-id-set,stop-range,akt,channel-mode,loop,smplr-fn)
type Server_Opt_B =
  ( [Stop_Id]
  , Stop_Id -> (Key, Key)
  , Stop_Id -> Common.Akt_f
  , Common.Ch_Mode
  , Common.Lp_Mode
  , Stop_Id -> Smplr_Fn
  )

type Server_Opt = (Server_Opt_A, Server_Opt_B, (Sc3.Group_Id, Sc3.Group_Id) -> Osc.Message)

rt_midi_osc_au :: Server_Opt -> IO ()
rt_midi_osc_au (opt_a, opt_b, grp_init) = do
  let (syn, ty, b0, (p_grp, au_grp), n0, tn_nm, ty_f, k, ampl, env, pwD, mwD, p, stp0, en_mode) = opt_a
      (stp_l, rng_f, akt_f, ch_mode, lp_mode, smplr_f) = opt_b
      recv_opt = (syn, b0, au_grp, ampl, env, pwD, mwD, stp_l, rng_f, akt_f, ch_mode, en_mode, lp_mode, smplr_f)
  Sc3.withSc3 (\sc3_fd -> Osc.sendMessage sc3_fd (grp_init (p_grp, au_grp)))
  t0 <- Osc.time
  tn_f <- mapM (\nm -> T.load_tuning_st_ty ty (nm, ty_f, k)) tn_nm
  g0 <- newStdGen
  mvar_st <- Concurrent.newMVar (st_init (t0, n0, g0, p, stp0))
  finally
    (Server.run_midi (init_f au_grp mvar_st) (recv_f recv_opt tn_f))
    ( putStrLn "rt_midi_osc: end"
        >> Sc3.withSc3 (\sc3_fd -> Osc.sendMessage sc3_fd (Sc3.n_free [au_grp]))
    )

-- * Osc (incoming control messages)

type Sc3_Fd = Osc.OscSocket
type Osc_Opt = (Sc3.Group_Id, Sc3_Fd)

to_param1 :: [Osc.Datum] -> Sc3.Param1
to_param1 = fromMaybe (error "to_param1") . U.unpackC_sf

to_amp_stop :: [Osc.Datum] -> (Stop_Id, Double)
to_amp_stop = fromMaybe (error "to_amp_stop") . U.unpackC_if

proc_amp_stop :: Osc_Opt -> St -> (Stop_Id, Double) -> IO St
proc_amp_stop (au_grp, sc3_fd) st (stop_n, stop_amp) = do
  Osc.sendMessage sc3_fd (Sc3.n_set1 (au_grp + 1 + stop_n) "amp_stop" stop_amp)
  return st {st_amp_stop = Map.insert stop_n stop_amp (st_amp_stop st)}

proc_amp_tbl_set1 :: St -> (Key, Double) -> IO St
proc_amp_tbl_set1 st (k, x) = return st {st_amp_tbl = Map.insert k x (st_amp_tbl st)}

proc_osc :: Osc_Opt -> Concurrent.MVar St -> Osc.Message -> IO ()
proc_osc (au_grp, sc3_fd) mvar_st m =
  let mod_st = Concurrent.modifyMVar_ mvar_st
  in case m of
      Osc.Message "/amp_stop" dat ->
        let amp_stop = to_amp_stop dat
        in mod_st (\st -> proc_amp_stop (au_grp, sc3_fd) st amp_stop)
      Osc.Message "/amp_tbl_clr" [] -> mod_st (\st -> return st {st_amp_tbl = Map.empty})
      Osc.Message "/amp_tbl_set1" dat ->
        case U.unpackC_if dat of
          Nothing -> error "proc_osc: /amp_tbl_set1: non-integral start index?"
          Just (k, x) -> mod_st (\st -> proc_amp_tbl_set1 st (k, x))
      Osc.Message "/amp_tbl_setn" (k : x) ->
        case Osc.datum_integral k of
          Nothing -> error "proc_osc: /amp_tbl_setn: non-integral start index?"
          Just k' ->
            let x' = map (fromMaybe 1.0 . Osc.datum_floating) x
                f st = Map.union (Map.fromList (zip [k' ..] x')) (st_amp_tbl st)
            in mod_st (\st -> return st {st_amp_tbl = f st})
      Osc.Message "/p_set" dat ->
        let f st = st {st_param = Sc3.param_insert (st_param st) (to_param1 dat)}
        in mod_st (return . f)
      Osc.Message "/stops" k ->
        let k' = mapMaybe Osc.datum_integral k
        in mod_st (\st -> return (st {st_stops = k'}))
      Osc.Message "/status" [] -> Concurrent.readMVar mvar_st >>= print
      Osc.Message "/toggle_stop" [k] ->
        case Osc.datum_integral k of
          Nothing -> error "proc_osc: /toggle_stop: non-integral value?"
          Just k' -> mod_st (\st -> return (st_toggle_stops st k'))
      Osc.Message "/tuning_coarse" [k] ->
        -- fractional midi-note-number (ie. 0.5 = quarter tone)
        case Osc.datum_floating k of
          Nothing -> error "proc_osc: /tuning_coarse: non-floating value?"
          Just k' ->
            Osc.sendMessage sc3_fd (Sc3.n_set1 au_grp "dt" k')
              >> mod_st (\st -> return st {st_tuning_coarse = k'})
      Osc.Message "/tuning_set" [ix] ->
        case Osc.datum_integral ix of
          Nothing -> error "proc_osc: /tuning_set: non-integral value?"
          Just ix' -> mod_st (\st -> return st {st_tuning_ix = ix'})
      _ -> error "proc_osc: illegal message?"

auditor_port :: Int
auditor_port = 57149

{- | Osc server listening on Udp at auditor_port

echo '["/p_set","rdetune",0]' | hosc-json-cat -h 127.0.0.1 -p 57149
-}
osc_server :: Osc_Opt -> Concurrent.MVar St -> IO ()
osc_server osc_opt mvar_st = do
  fd <- Osc.udpServer "127.0.0.1" auditor_port
  let recur = do
        (pkt, _adr) <- Osc.recvFrom fd
        let m = Osc.packetMessages pkt
        print pkt
        mapM_ (proc_osc osc_opt mvar_st) m
  finally (forever recur) (Osc.close fd)
