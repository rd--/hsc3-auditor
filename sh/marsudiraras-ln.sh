#!/bin/sh

cd ~/data/audio/marsudi-raras/gamelan/wav
find . -type l | xargs rm -f
ln -s KempulSl3.wav KempulSl3l.wav
ln -s KempulSl5.wav KempulSl5l.wav
ln -s KempulSl6.wav KempulSl6l.wav
ln -s KenongPl1.wav KenongPl1h.wav
ln -s KenongSl1.wav KenongSl1h.wav
ln -s KetukSl6.wav KetukSl6l.wav
