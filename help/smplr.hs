import Sound.Sc3 {- hsc3 -}

import Sound.Sc3.Lang.Control.Event {- hsc3-lang -}
import Sound.Sc3.Lang.Pattern.P {- hsc3-lang -}
import Sound.Sc3.Lang.Pattern.P.Event {- hsc3-lang -}

import Sound.Sc3.Auditor.Common {- hsc3-auditor -}
import Sound.Sc3.Auditor.Smplr {- hsc3-auditor -}

-- > import Sound.Sc3.Auditor.Crotales
-- > crotale_load 0
-- > paudition (p_00 13)
p_00 :: Int -> P Event
p_00 k =
  pbind [(K_instr,psynth (smplr (Ch_Pan_1,En_Dur,Lp_None)))
        ,(K_param "bufnum",pseries 0 1 (k - 1))
        ,(K_param "attackTime",0.25)
        ,(K_param "releaseTime",0.15)
        ,(K_dur,0.35)]

-- > paudition (p_01 13)
p_01 :: Int -> P Event
p_01 k =
  pbind [(K_instr,psynth (smplr (Ch_Pan_1,En_Dur,Lp_None)))
        ,(K_param "bufnum",pwhitei 'α' 0 (toF k - 1) inf)
        ,(K_param "startpos",0.15 * 48000)
        ,(K_param "attackTime",0.15)
        ,(K_amp,pwhite 'β' 0.15 0.65 inf)
        ,(K_param "pan",pwhite 'γ' (-1) 1 inf)
        ,(K_dur,pwhite 'δ' 0.15 1.65 inf)]
